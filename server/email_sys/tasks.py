from __future__ import absolute_import
from celery import shared_task

import datetime
from enum import Enum

from django.core.mail import send_mail

from accounts.models import User
from api_v1.scraping.models import Project
from api_v1.scraping.models import Execution

import html2text as ht

@shared_task
def send_email_celery(subject, body, sender, recipients):
	send_mail(subject=subject, message=ht.html2text(body), from_email=sender, recipient_list=recipients, html_message=body)
	return

@shared_task
def send_monthly_report():
	from email_sys.views import Email_type
	from email_sys.views import email_builder

	#today = datetime.date.today()
	today = datetime.date.today()
	today = today.replace(day=1)

	month_obj = today - datetime.timedelta(days=1)
	month, year = month_obj.month, month_obj.year

	users = User.objects.all()

	for user in users:
		#data = {'recipients': [user.email], 'user_obj': user, 'month': month, 'year': year}
		data = {'recipients': ['jefu@2mailnext.com'], 'user_obj': user, 'month': month, 'year': year}
		email_obj = email_builder.build(Email_type.MONTHLY_REPORT, data)
		email_obj.send_mail()