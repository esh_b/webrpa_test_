import os
from django.conf import settings

from django.dispatch import receiver
from django.db.models.signals import post_save

from accounts.models import User
from accounts.models import Profile
from accounts.models import Data_Order
from api_v1.scraping.models import Execution
from api_v1.scraping.models import Project

from accounts.forms import ProfileChangeForm

from email_sys.views import email_builder
from email_sys.views import Email_type

from internal.xpath import compare_files
from internal.xpath import keyword_match

@receiver(post_save, sender=User)
def user_details_changed(sender: User, **kwargs):
    user = kwargs['instance']
    email_type = ''
    data = {'recipients': [kwargs['instance'].email]}

    if(kwargs['created']):
        if(not(user.social_auth.exists()) and user.is_active == 0):
            email_type = Email_type.ACCOUNT_ACTIVATION
            data.update({"user_obj": user})
        else:
            return
    elif(not(kwargs['created']) and kwargs['update_fields']):
        if('password' in kwargs['update_fields']):              #Password updation
            email_type = Email_type.PASSWORD_CHANGED
        elif('is_active' in kwargs['update_fields'] and user.is_active == 1):         #Activation status
            if(kwargs['instance'].old_email):
                email_type = Email_type.EMAIL_CHANGED
                data['recipients'].append(kwargs['instance'].old_email)
            else:
                email_type = Email_type.REGISTRATION_COMPLETE
        elif('is_active' in kwargs['update_fields'] and user.is_active == 0):         #Activation status
            email_type = Email_type.ACCOUNT_ACTIVATION
            data.update({"user_obj": user})
        else:
            return
    else:
        return

    email_obj = email_builder.build(email_type, data)
    email_obj.send_mail()
    return

#Triggered when an user info is updated in the accounts_profile table
@receiver(post_save, sender=Profile)
def user_profile_details_changed(sender: Profile, **kwargs):
    #Ignore if the row is created (thats when users signs up) or if the update is done without specifying update_field (maybe columns other than basic info)
    if(not(kwargs['created']) and kwargs['update_fields']):
        #Send the BASIC_INFO_CHANGED email if the the fields defined in the ProfileChangeForm ONLY is updated
        if(set(kwargs['update_fields']) < set(ProfileChangeForm._meta.fields)):
            email_type = Email_type.ACCOUNT_INFO_CHANGED
            data = {'recipients' : [kwargs['instance'].user.email], 'update_fields': kwargs['update_fields']}
            email_obj = email_builder.build(email_type, data)
            email_obj.send_mail()
    return

#Triggered when the project execution status has changed (SUCCESS or FAILURE)
#The code incorporates checking and sending DATA_DIFF, KEYWORD and DATA_KEYWORD email types incase any.
@receiver(post_save, sender=Execution)
def project_status_changed(sender: Execution, **kwargs):
    if(kwargs['created']):
        email_type = ''
        exec_obj = kwargs['instance']
        project = exec_obj.project

        uid = Project.objects.get(id=exec_obj.project.id).user.id
        user_email = User.objects.get(id=uid).email

        data = {
            'recipients': [user_email],
            'user_id': uid,
            'exec_id': exec_obj.task_id,
        }

        if(exec_obj.status):
            #data_flag is FALSE when there is some data difference between currently scraped data and the data scraped during earlier execution.
            data_flag = None
            execution_count = Execution.objects.filter(project_id=exec_obj.project.id).count()
            
            # TODO - compare_files will give FALSE even if the order of the results
            # in the scraped data is different. See email_sys/Readme.md for more details.
            if(execution_count >= 2):
                execution = Execution.objects.filter(project_id=exec_obj.project.id, status=1).order_by('-date_executed')
                path1 = os.path.join(settings.SCRAPING_DATA_DIR, str(project.user.id), str(project.id),
                                     execution[0].task_id + '.json')
                path2 = os.path.join(settings.SCRAPING_DATA_DIR, str(project.user.id), str(project.id),
                                     execution[1].task_id + '.json')
                data_flag = compare_files(path1, path2)
            
            #keyword_flag is TRUE when there is a word in the scraped data that matches the given 'keyword'.
            keyword_flag = None
            if(project.keyword != ''):
                keyword = project.keyword
                execution = Execution.objects.get(task_id=exec_obj.task_id)
                path = os.path.join(settings.SCRAPING_DATA_DIR, str(project.user.id), str(project.id),
                                    execution.task_id + '.json')
                keyword_flag = keyword_match(keyword, path)

            #ALERT_DATA_KEYWORD email is sent when there is DATA_DIFF and also KEYWORD_HITTING
            #assert data_flag is not None and is False and keyword is True for ALERT_DATA_KEYWORD email
            if(project.data_alert and project.keyword_alert and data_flag is not None and not data_flag and keyword_flag):
                email_type = Email_type.ALERT_DATA_KEYWORD
            elif(project.data_alert and data_flag is not None and not data_flag):
                email_type = Email_type.ALERT_DATA_DIFFERENCE
            elif(project.keyword_alert and keyword_flag):
                email_type = Email_type.ALERT_KEYWORD_HITTING
            elif(project.mail_alert):
                email_type = Email_type.EXEC_STATUS_SUCCESS
            else:
                return
        else:
            if(project.mail_alert):
                email_type = Email_type.EXEC_STATUS_FAILURE
            else:
                return

        email_obj = email_builder.build(email_type, data)
        email_obj.send_mail()
    return

#Triggered when a new row is created in the accounts_data_order table
@receiver(post_save, sender=Data_Order)
def user_data_order_confirmed(sender: Data_Order, **kwargs):
    if(kwargs['created']):
        email_type = Email_type.DATA_ORDER_CONFIRMED
        data = {'recipients' : [kwargs['instance'].email], "data_order_obj": kwargs['instance']}

        email_obj = email_builder.build(email_type, data)
        email_obj.send_mail()
    return

"""
#Triggered when user_activation_email is sent for the first time
@receiver(post_save, sender=UserSubscription)
def new_user_details_added(sender: UserSubscription, **kwargs):
    if(kwargs['created']):
        email_type = Email_type.ACCOUNT_ACTIVATION
        data = {'recipients' : [kwargs['instance'].user.email], "user_obj": kwargs['instance'].user}

        email_obj = email_builder.build(email_type, data)
        email_obj.send_mail()
    else:
        if(kwargs['update_fields'] is None):
            return

        if('plan_id' in kwargs['update_fields']):
            email_type = Email_type.PAYMENT_PLAN_CHANGED
            data = {'recipients' : [kwargs['instance'].user.email], 'plan_id': kwargs['instance'].plan_id}

            email_obj = email_builder.build(email_type, data)
            email_obj.send_mail()
    return
"""