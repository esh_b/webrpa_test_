# Email notifications system

The email notifications system is implemented as a separate app in Django. There are various types of emails to be sent: Every email type is discussed below.

### ACCOUNT_ACTIVATION Email
- This email is sent during two occasions:
	- To verify the user email address when he signs up.
	- To verify the user's new email address when the user tries to change his email address.

- Technical specifications:
	- The first type of email (user signing up for the first time) is triggered when a new row is *CREATED* in the `accounts_user` table.
	- The second type of email (user changing email address) is triggered when the `is_active` field is *UPDATED* to `0` in the `accounts_user` table. **Note** that the `old_email` field in the `accounts_user` table is set to the user's existing email address while the `email` field and the `username` field is set to the user's new email address once before the activation email is actually sent here.

### REGISTRATION_COMPLETE Email
- This email is sent when the user has finished verifying his email address the first time he signs up.
- Technical specifications:
	- This email is triggered when the `is_active` field is *UPDATED* to `1` in the `accounts_user` table **AND** the `old_email` field is `NULL`.

### EMAIL_CHANGED Email
- This email is sent when the user has finished verifying his new email address.
- Technical specifications:
	- This email is triggered when the `is_active` field is *UPDATED* to `1` in the `accounts_user` table **AND** also `old_email` field is `NOT NULL`.

###  PASSWORD_CHANGED Email
- This email is sent when the user has successfully changed his password.
- Technical specifications:
	- This email is triggered when the `password` field is *UPDATED* in the `accounts_user` table.

### VERSION_UPDATE Email
- This email is sent when there is some `tag push` event in our Gitlab project. The assumption is that tag push is done only when there is some new version released.
- It is assumed that a push is made with the tagname starting with the letter `v`.
- Technical specifications:
	- This email is triggered when a request from the Gitlab webhook is received (that's when there is a `tag push` event in Gitlab) and the `tagname` starts with the letter `v`.
	- **Note** that the webhook has to be setup on the `tag push` event in the Gitlab project beforehand to trigger this email.
- Ambiguities:
	- The version_update email is expected to be sent when there is some new client version is released. But, in our case, the user will be updated only when the server-code is updated and pushed with the new version.
	- Workarounds: Have to store the latest_versions of Client in some Google Drive directory and setup webhooks for that directory ([link](https://developers.google.com/drive/api/v3/push)).

### EXEC_STATUS_SUCCESS Email
- The user can select whether he wished to receive notifications when the project execution is complete (SUCCESS or FAILURE). 
- This email is specifically sent when the user has subscribed to receive notifications about the project status and that the project execution is successful i.e. the status of the execution in Celery TaskResult table is `SUCCESS`. 
- `NOTE`: This email is not sent when the project is successful and there is some data difference or keyword matching in the current execution. In that case, `ALERT_DATA_DIFF`, `ALERT_KEYWORD_HITTING` or `ALERT_DATA_KEYWORD` emails are sent accordingly.
-Technical specifications:
	- This email is triggered when the `status` or `error` fields are *UPDATED* in the `scraping_execution` table **AND** `status` field is *UPDATED* to `1` and `error` is *UPDATED* to `0` (meaning project execution is SUCCESSFUL) **AND** `mail_alert` field for the executing project has been set to `1` in the `scraping_project` table **AND** other emails like `ALERT_DATA_DIFF`, `ALERT_KEYWORD_HITTING` or `ALERT_DATA_KEYWORD` emails are NOT sent.

### EXEC_STATUS_FAILURE Email
- As mentioned above, the user can select whether he wished to receive notifications when the project execution is complete (SUCCESS or FAILURE). 
- This email is specifically sent when the user has subscribed to receive notifications about the project status and that the project execution has failed i.e. the status of the execution in Celery TaskResult table is `FAILURE`. 
-Technical specifications:
	- - This email is triggered when the `status` or `error` fields are *UPDATED* in the `scraping_execution` table **AND** `status` field is *UPDATED* to `0` and `error` is *UPDATED* to `1` (meaning project execution has FAILED) **AND** `mail_alert` field for the executing project has been set to `1` in the `scraping_project` table.

### ACCOUNT_INFO_CHANGED Email
- This email is sent when the user has updated any of his basic account information (like name, company, phone number etc).
- Technical specifications:
	- This email is sent when one or more of the fields used in the `ProfileChangeForm` form is *UPDATED* in the `accounts_profile` table. The `ProfileChangeForm` form is shown to the user when he wants to edit his basic information. So, when something is *UPDATED* in the `accounts_profile` table, an email is sent only when the `update_fields` has one of the fields defined in the `ProfileChangeForm` form.

### MONTHLY_REPORT Email
- This email is sent to every user to update them about their last month Pig data usage (like number of projects executed last month and number of successful and failed executions for those projects).
- `django-celery-beat` module was used to schedule the monthly report cron job. This scheduling can be done in the admin panel. The link to the tutorial to schedule the cron job can be found [here](https://www.merixstudio.com/blog/django-celery-beat/).
- To start running the cron job, the scheduler has to be started separately as a `celery` beat type. The command to start the scheduler is: `celery -A <NAME_OF_THE_PROJECT> beat -l info --scheduler django_celery_beat.schedulers:DatabaseScheduler`.
- Once the cron job is scheduled to run on the first day of every month (at 12 am maybe) and the scheduler is started, the `send_monthly_report` task is executed and monthly reports are sent every month at the specified date and time.
- Note that in `send_monthly_report` celery task, first the Email is built using the `Build_email` class and then an instance of `Email` object is returned. Then, the `send_mail`  method of the `Email` class sends the email using a celery worker. So, it is important for a celery worker to be running when the scheduler is running.

### ALERT_DATA_KEYWORD Email
- This email is sent when there is some difference in the scraped data in the current execution compared to the earlier execution and ALSO there is a word in the scraped data that is matching the given keyword for a project.
- Note that the `EXEC_STATUS_SUCCESS` mail will not be sent when this email is getting sent.
- Technical specifications:
	- This email is triggered when the `status` or `error` fields are *UPDATED* in the `scraping_execution` table **AND** `status` field is `1` and `error` is `0` (meaning project execution is SUCCESSFUL) **AND** there is some difference in the scraped data in the current execution compared to the earlier execution **AND** there is a word in the scraped data that is matching the given keyword for a project.

### ALERT_DATA_DIFFERENCE Email
- This email is sent when there is some difference in the scraped data in the current execution compared to the earlier execution for a project.
- Note that the `EXEC_STATUS_SUCCESS` mail will not be sent when this email is getting sent.
- Technical specifications:
	- This email is triggered when the `status` or `error` fields are *UPDATED* in the `scraping_execution` table **AND** `status` field is `1` and `error` is `0` (meaning project execution is SUCCESSFUL) **AND** there is some difference in the scraped data in the current execution compared to the earlier execution for the project.

### ALERT_KEYWORD_HITTING Email
- This email is sent when there is a word in the scraped data that is matching the given keyword for a project.
- Note that the `EXEC_STATUS_SUCCESS` mail will not be sent when this email is getting sent.
- Technical specifications:
	- This email is triggered when the `status` or `error` fields are *UPDATED* in the `scraping_execution` table **AND** `status` field is `1` and `error` is `0` (meaning project execution is SUCCESSFUL) **AND** there is a word in the scraped data that is matching the given keyword for the project.

### PAYMENT_STATUS_SUCCESS Email
- This email is sent when the event `charge.succeeded` is triggered in the PAYJP server for our account. That event is triggered when the user payment was successful (maybe the first time payment or monthly deduction).
- Technical specifications:
	- A webhook is setup in the PAYJP server so as to receive requests in our server incase any event (like `charge.succeeded` or `charge.failure`) is triggered in the PAYJP server. We then send this email only when the triggered event is `charge.succeeded`. 
	- Any other kind of events (like `charge.captured`, `subscription.created` etc) other than `charge.succeeded` and `charge.failed` are simply **ignored**.
	- **Note** that the webhook has to be setup in the PAYJP server beforehand to trigger this email.

### PAYMENT_STATUS_FAILURE Email
- This email is sent when the event `charge.failure` is triggered in the PAYJP server for our account. That event is triggered when the user payment has failed due to some reasons (e.g. card declined).
- Technical specifications:
	- The same webhook used for the `PAYMENT_STATUS_SUCCESS` email is used here and this email is sent when the `charge.failed` event is triggered.
	- Any other kind of events (like `charge.captured`, `subscription.created` etc) other than `charge.succeeded` and `charge.failed` are simply **ignored**.
	- **Note** that the webhook has to be setup in the PAYJP server beforehand to trigger this email.

### DATA_ORDER_CONFIRMED Email
- This email is sent when the user has saved some data_order project.
- Technical specifications:
	- This email is triggered when a new row is *CREATED* in the `accounts_data_order` table.

### PAYMENT_PLAN_CHANGED Email
- This email is sent when the user has changed his payment plan from `FREE` to `BUSINESS` or vice versa.
- Technical specifications:
	- This email is triggered when the `plan_id` field is *UPDATED* in the `sales_usersubscription` table.

## TODO
- **IMPORTANT:** `ALERT_DATA_DIFF` email will be sent even if there is some change in the order of the results in the scraped data. So, the `compare_files` method has to be changed accordingly.
For example: for a text_extraction project, even if the extracted text is the same during the earlier and current execution but the order of the text results are not equal, even then the email is getting sent which MUST NOT be the case.
- The `ALERT_DATA_DIFF`, `ALERT_KEYWORD_HITTING` and `ALERT_DATA_KEYWORD` email templates have to be checked.

## INFO FOR DEVELOPERS
- Currently, the email module tracks the following tables:
	- `accounts_user`
	- `accounts_profile`
	- `accounts_data_order`
	- `scraping_execution`
- **NOTE:** Please be careful while trying to add or update fields in the above-mentioned tables.
