from django.conf.urls import url, include
from django.urls.conf import path
from . import views

app_name = 'email_sys'
urlpatterns = [
    path(r'get_version', views.get_version, name='get_version'),
    path(r'get_payment_details', views.get_payment_details, name='get_payment_details'),
]
