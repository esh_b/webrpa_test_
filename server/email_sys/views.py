"""Summary

Attributes:
    email_builder (TYPE): Python instance of Email_builder class - builds email from its components
    Email_type (TYPE): Enum of email types
    JSON_MAPPINGS_PATH (TYPE): Path to the mappings.json file
    module_dir (TYPE): The current file directory
"""
import json
import os
from enum import Enum

from django.template.loader import render_to_string
from django.http import HttpResponse, HttpRequest
from django.views.decorators.csrf import csrf_exempt

from .tokens import account_activation_token

from django.db import models
from accounts.models import User
from accounts.models import Profile
from api_v1.scraping.models import Project
from api_v1.scraping.models import Execution
from django_celery_results.models import TaskResult

from django.conf import settings

from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode

from .tasks import send_email_celery

module_dir = os.path.dirname(__file__)
JSON_MAPPINGS_PATH = os.path.join(module_dir, 'mappings.json')
Email_type = Enum('Email_type', ['ACCOUNT_ACTIVATION',
                                 'REGISTRATION_COMPLETE',
                                 'EMAIL_CHANGED',
                                 'PASSWORD_CHANGED',
                                 'VERSION_UPDATE',
                                 'EXEC_STATUS_SUCCESS',
                                 'EXEC_STATUS_FAILURE',
                                 'ACCOUNT_INFO_CHANGED',
                                 'MONTHLY_REPORT',
                                 'ALERT_DATA_KEYWORD',
                                 'ALERT_DATA_DIFFERENCE',
                                 'ALERT_KEYWORD_HITTING',
                                 'PAYMENT_STATUS_SUCCESS',
                                 'PAYMENT_STATUS_FAILURE',
                                 'DATA_ORDER_CONFIRMED',
                                 'PAYMENT_PLAN_CHANGED'
                                 ])


def send_email_func_call(email_type, **kwargs):
    """Method to get email objects using function calls

    Args:
        **kwargs: Description
    """
    pass


@csrf_exempt
def get_version(request: HttpRequest):
    """Method to get request from Gitlab incase of any tag push
    
    Args:
        request (HttpRequest): Description
    
    Returns:
        TYPE: Description
    """
    
    if(not(request.method == "POST") or not('HTTP_X_GITLAB_TOKEN' in request.META) or 
                                        not(request.META['HTTP_X_GITLAB_TOKEN'] == settings.GITLAB_TOKEN)):
        return HttpResponse(status=403)

    body = json.loads(request.body.decode('utf-8'))
    tagname = body['ref'].split("/")[-1]

    #Check tagname starts with 'v'
    if(tagname.startswith("v")):
        email_type = Email_type.VERSION_UPDATE
        data = {'version': tagname}
        email_obj = email_builder.build(email_type, data)
        #email_obj.send_mail()

    return HttpResponse('')

@csrf_exempt
def get_payment_details(request: HttpRequest):
    """Method to get request from PAYJP when a payment has been done
    
    Args:
        request (HttpRequest): Description
    
    Returns:
        TYPE: Description
    """
    if(not(request.method == "POST") or not('HTTP_X_PAYJP_WEBHOOK_TOKEN' in request.META) or 
                                        not(request.META['HTTP_X_PAYJP_WEBHOOK_TOKEN'] == settings.PAYJP_TOKEN)):
        return HttpResponse(status=403)

    body = json.loads(request.body.decode('utf-8'))

    if(body['type'] in ["charge.succeeded", "charge.failed"]):
        data = {}
        #data['user_id'] = body['data']['customer']
        data['user_id'] = 56
        data['payment_amount'] = body['data']['amount']

        if(body['type'] == "charge.succeeded"):
            email_type = Email_type.PAYMENT_STATUS_SUCCESS
        else:
            email_type = Email_type.PAYMENT_STATUS_FAILURE
            data['failure_code'] = body['data']['failure_code']
            data['failure_message'] = body['data']['failure_message']

        email_obj = email_builder.build(email_type, data)
        #email_obj.send_mail()

    return HttpResponse('')

class Build_email:
    """Email Builder class

    Attributes:
        ACCOUNT_PROFILE_LINK (str): User account profile URL
        CLIENT_TOOL_DOWNLOAD_LINK (str): Client tool download link
        mappings (dict): Dict mapping email_type to its subject and template name
    """

    def __init__(self, json_filepath: str):
        """Summary

        Args:
            json_filepath (str): Description
        """
        self.mappings = {}
        with open(json_filepath, encoding="utf-8") as f:
            self.mappings = json.load(f)

    def get_subject(self, email_type: Email_type):
        """Summary

        Args:
            email_type (Email_type): Description

        Returns:
            TYPE: Description
        """
        return self.mappings[email_type.name]['sub']

    def get_recipients(self, data: dict):
        """Get the email recipients

        Args:
            data (dict): Description

        Returns:
            TYPE: Description
        """
        if ('recipients' in data):
            return data['recipients']
        elif('user_id' in data):
            user_obj = User.objects.get(id=data['user_id'])
            return [user_obj.email]
        else:
            # Bulk email
            recipients = []
            user_objs = User.objects.only("email")
            for user in user_objs:
                recipients.append(user.email)
            return recipients

    def get_activation_email_params(self, data: dict):
        """
        Args:
            data (dict): Description

        Returns:
            TYPE: Description
        """

        user = data['user_obj']
        current_site_url = settings.SITE_URL

        activation_params = {
            'user': user,
            'site_url': current_site_url,
            'uid': force_text(urlsafe_base64_encode(force_bytes(user.pk))),
            'token': account_activation_token.make_token(user),
        }
        data.update(activation_params)
        return

    def get_registered_email_params(self, data: dict):
        """Method to add additional params for email_registered

        Args:
            data (dict): Description

        Returns:
            TYPE: Description
        """
        return data.update({'profile_link': settings.ACCOUNT_PROFILE_LINK})

    def get_email_changed_email_params(self, data: dict):
        """Method to add additional params for email_changed email

        Args:
            data (dict): Description

        Returns:
            TYPE: Description
        """
        return data.update({'profile_link': settings.ACCOUNT_PROFILE_LINK})

    def get_password_changed_email_params(self, data: dict):
        """Method to add additional params for password_changed email

        Args:
            data (dict): Description

        Returns:
            TYPE: Description
        """
        return data.update({'profile_link': settings.ACCOUNT_PROFILE_LINK})

    def get_account_info_changed_email_params(self, data: dict):
        """Method to add additional params for basic_info_changed email

        Args:
            data (dict): Description

        Returns:
            TYPE: Description
        """
        return data.update({'profile_link': settings.ACCOUNT_PROFILE_LINK})

    def get_dwnd_type_name(self, dwnd_type: int):
        """Method to map DOWNLOAD_TYPE to NAME

        Args:
            dwnd_type (int): Description

        Returns:
            TYPE: Description
        """
        if (dwnd_type == -1):
            return "NONE"
        elif (dwnd_type == 0):
            return "TEXT"
        elif (dwnd_type == 1):
            return "MEDIA"
        elif (dwnd_type == 2):
            return "TEXT, MEDIA"
        else:
            return None

    def get_executed_data_info(self, user_id: str, project_id: str, exec_id: str):
        """Method to get the execution's download data size and type

        Args:
            user_id (str): Description
            project_id (str): Description
            exec_id (str): Description

        Returns:
            TYPE: Description
        """
        filename = exec_id + ".json"
        fp = os.path.join(settings.SCRAPING_DATA_DIR, *[str(user_id), str(project_id), filename])
        data_size = os.path.getsize(fp)

        dwnd_type = ""
        with open(fp) as f:
            dwnd_type = json.loads(f.read())[2]
        dwnd_type_name = self.get_dwnd_type_name(dwnd_type)

        return data_size, dwnd_type_name

    def get_exec_status_email_params(self, data: dict, exec_done: bool):
        """Method to add additional project status params

        Args:
            data (dict): Description
            exec_done (bool): Description

        Returns:
            TYPE: Description
        """

        # uid = Project.objects.get(id=project.id).user.id
        user_email = User.objects.get(id=data['user_id']).email
        exec_obj = Execution.objects.get(task_id=data['exec_id'])
        project = exec_obj.project

        add_data = {
            'recipents': [user_email],
            'project': {
                'name': project.name,
                'category': project.category,
            },
            'execution': {
                'start_time': exec_obj.date_executed.strftime("%Y-%m-%d %H:%M:%S"),
                'completion_time': exec_obj.end_time.strftime("%Y-%m-%d %H:%M:%S"),
                'elapsed_time': exec_obj.exec_time.total_seconds(),
            }
        }

        if (exec_done):
            exec_data_size, exec_dwnd_type = self.get_executed_data_info(data['user_id'], project.id, data['exec_id'])

            add_data['execution'].update({
                'data_size': str(exec_data_size) + " bytes",
                'dwnd_type': exec_dwnd_type
            })
        return data.update(add_data)

    def get_version_up_email_params(self, data: dict):
        """Method to add the additional params required for version_up email

        Args:
            data (dict): Description

        Returns:
            TYPE: Description
        """
        return data.update({'download_link': settings.CLIENT_TOOL_DOWNLOAD_LINK})

    def get_monthly_project_info_params(self, proj: Project, month: int, year: int):
        """For a given project, get all the executions in the month and the year given

        Args:
            proj (Project): The Project Instance
            month (int): The month during which the number of executions for the project has to be returned
            year (int): The year during which the number of executions for the project has to be returned

        Returns:
            TYPE: Description
        """
        execs = Execution.objects.filter(project_id=proj.id, date_executed__month=month, date_executed__year=year)
        if (execs.count() == 0):
            return None

        execs_succ = execs.filter(status=1).count()
        execs_fail = execs.filter(status=0).count()

        proj = {
            'name': proj.name,
            'category': proj.category,
            'execs_succ': execs_succ,
            'execs_fail': execs_fail
        }
        return proj

    def get_monthly_email_params(self, data: dict):
        """Get the projects that were run in the last month along with their category and exec_success and exec_failure count

        Args:
            data (dict): Description

        Returns:
            TYPE: Description
        """
        user_projs = Project.objects.filter(user_id=data['user_obj'].id)

        projs_data = {}
        for proj in user_projs:
            proj_data = self.get_monthly_project_info_params(proj, data['month'], data['year'])
            if (proj_data):
                projs_data[proj.id] = proj_data

        return data.update({'projects': projs_data})

    def get_data_difference_email_params(self, data: dict):
        """Get the project information for the execution during which there was difference in the data scraped.
        
        Args:
            data (dict): Description
        
        Returns:
            TYPE: Description
        """
        project = Execution.objects.get(task_id=data['exec_id']).project

        # uid = Project.objects.get(id=project.id).user.id
        user_email = User.objects.get(id=data['user_id']).email

        add_data = {
            'project': {
                'name': project.name,
                'category': project.category,
            },
        }
        return data.update(add_data)

    def get_keyword_email_params(self, data: dict):
        """Get the project information for the execution during which the given keyword for the project has matched
        
        Args:
            data (dict): Description
        
        Returns:
            TYPE: Description
        """
        project = Execution.objects.get(task_id=data['exec_id']).project

        # uid = Project.objects.get(id=project.id).user.id
        user_email = User.objects.get(id=data['user_id']).email

        add_data = {
            'project': {
                'name': project.name,
                'category': project.category,
                'id': project.id,
                'keyword': project.keyword
            },
        }
        return data.update(add_data)

    def get_data_keyword_email_params(self, data: dict):
        """Get the project information for the execution during which there was both keyword match and data difference.
        
        Args:
            data (dict): Description
        
        Returns:
            TYPE: Description
        """
        project = Execution.objects.get(task_id=data['exec_id']).project

        # uid = Project.objects.get(id=project.id).user.id
        user_email = User.objects.get(id=data['user_id']).email

        add_data = {
            'project': {
                'name': project.name,
                'category': project.category,
                'id': project.id,
                'keyword': project.keyword
            },

        }
        return data.update(add_data)

    def get_payment_success_email_params(self, data: dict):
        """Method to get the additional params required for PAYMENT_STATUS_SUCCESS email
        
        Args:
            data (dict): Description
        
        Returns:
            TYPE: Description
        """

        user_profile = Profile.objects.get(user_id=data['user_id'])
        user_name = (user_profile.last_name_kana + " " + user_profile.first_name_kana).strip()
        user_company = user_profile.company_name_kana

        add_data = {
            'user_name': user_name,
            'user_company': user_company,
            'payment_info_link': settings.ACCOUNT_PAYMENT_INFO_LINK
        }
        return data.update(add_data)

    def get_body(self, email_type: Email_type, data: dict):
        """Method to build the email body

        Args:
            email_type (Email_type): Description
            data (dict): Description

        Returns:
            TYPE: Description
        """
        template_name = self.mappings[email_type.name]['template']

        if (email_type == Email_type.EMAIL_CHANGED):
            self.get_email_changed_email_params(data)
        elif (email_type == Email_type.PASSWORD_CHANGED):
            self.get_password_changed_email_params(data)
        if (email_type == Email_type.ACCOUNT_INFO_CHANGED):
            self.get_account_info_changed_email_params(data)
        elif (email_type == Email_type.ACCOUNT_ACTIVATION):
            self.get_activation_email_params(data)
        elif (email_type == Email_type.REGISTRATION_COMPLETE):
            self.get_registered_email_params(data)
        elif (email_type == Email_type.VERSION_UPDATE):
            self.get_version_up_email_params(data)
        elif (email_type == Email_type.EXEC_STATUS_SUCCESS):
            self.get_exec_status_email_params(data, exec_done=True)
        elif (email_type == Email_type.ALERT_DATA_KEYWORD):
            self.get_data_keyword_email_params(data)
        elif (email_type == Email_type.ALERT_DATA_DIFFERENCE):
            self.get_data_difference_email_params(data)
        elif (email_type == Email_type.ALERT_KEYWORD_HITTING):
            self.get_keyword_email_params(data)
        elif (email_type == Email_type.EXEC_STATUS_FAILURE):
            self.get_exec_status_email_params(data, exec_done=False)
        elif (email_type == Email_type.MONTHLY_REPORT):
            self.get_monthly_email_params(data)
        elif (email_type == Email_type.PAYMENT_STATUS_SUCCESS):
            self.get_payment_success_email_params(data)

        return render_to_string(template_name, data)

    def build(self, email_type: Email_type, data: dict):
        """Method to build the email from email_type and data

        Args:
            email_type (Email_type): Description
            data (dict): Description

        Returns:
            TYPE: Description
        """
        recipients = self.get_recipients(data)
        subject = self.get_subject(email_type)
        template = self.get_body(email_type, data)

        return Email(recipients, subject, template)


class Email:
    """Email class to get email params and send emails

    Attributes:
        body (str): Email body
        recipients (list): List of recipients
        sender (str): Email sender
        subject (str): Email subject
    """

    def __init__(self, recipients: list, subject: str, body: str):
        """init() method

        Args:
            recipients (list): List of recipients
            subject (str): Email subject
            body (str): Email body
        """
        self.subject = subject
        self.recipients = recipients
        self.body = body
        self.sender = 'no-reply@sms-datatech.co.jp'

    def send_mail(self):
        """Method to send tasks to celery
        """
        for recipient in self.recipients:
            send_email_celery.delay(self.subject, self.body, self.sender, [recipient])


email_builder = Build_email(JSON_MAPPINGS_PATH)
