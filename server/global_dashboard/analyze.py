from django.http.request import HttpRequest
from django.shortcuts import render
import os
from django.core.files.storage import FileSystemStorage
from django.conf import settings
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import os
import math
import seaborn as sns
from datetime import datetime,timedelta
from pandas.plotting  import lag_plot
from pandas.plotting import autocorrelation_plot
from random import randint
import time
from statistics import median
from django.views.decorators.csrf import csrf_exempt
import gc
from pylab import rcParams
from sklearn.manifold import TSNE
from sklearn.preprocessing import StandardScaler, MinMaxScaler
from sklearn.decomposition import PCA

upload_folder = settings.SCRAPING_UPLOAD_DIR

@csrf_exempt
def analyze(request: HttpRequest):
    if request.method == 'POST' and request.POST.get("col1"):
        col1 = request.POST.get('col1')
        col2 = request.POST.get('col2')
        print('col1:' + col1 , 'col2:' + col2 )

    if request.method == 'POST' and request.FILES['myfile']:
        myfile = request.FILES['myfile']

        folder = os.path.join(upload_folder, str(request.user.id))
        fs = FileSystemStorage(location=folder)
        filename = fs.save(myfile.name, myfile)
        uploaded_file_url = fs.url(filename)
        uploaded_file_url = os.path.join(folder, uploaded_file_url)

        if getFileType(uploaded_file_url) == 'csv' or 'xls' or 'xlxs':

            data_frame = pd.read_csv(uploaded_file_url)
        else:
            error = 'File type not supported'
            return render(request, 'analyze.html', {'activate':'analyze_data', 'error': error})




        print(data_frame.iat[0,2])
        # with open(uploaded_file_url, 'r+') as data_file:
        #     data = csv.DictReader(data_file)
        #     for row in data:
        #         print(row)
        cols = data_frame.columns
        # print(cols.to_dict())
        # rows = data_frame.shape[0]
        cols_count = data_frame.shape[1]
        # print(data_frame.head().to_dict())
        return render(request, 'analyze.html', {
            'uploaded_file_url': uploaded_file_url, 'activate':'analyze_data', 'df': data_frame.head().to_html(), 'rows': data_frame.head(), 'object': data_frame.head().to_dict(), 'cols':cols, 'cols_count': cols_count })
    return render(request, 'analyze.html', {'activate':'analyze_data'})


def getFileType(filename):
    if filename[-3:] == 'csv':
        return "csv"
    elif filename[-3:] == 'xls':
        return "xls"
    elif filename[-4:] == 'xlsx':
        return "xlsx"
    else:
        return "invalid"

def getDataFrame(filename):
    df = pd.DataFrame()
    ext = getFileType(filename)
    if ext == "xls" or ext == "xlsx":
        temp =  pd.ExcelFile(filename)
        df = pd.read_excel(temp,1)
        return df
    else:
        df = pd.read_csv(filename)
        return df
    return df


def getColumns(dataframe):
    columns = [col for col in dataframe.columns]
    return columns, len(columns)


def getColumnTypes(dataframe):
    column_types = {}
    for k,v in dataframe.infer_objects().dtypes.items():
        column_types[k] = v
    rev_col_type = {}
    for k, v in sorted(column_types.items()):
        rev_col_type.setdefault(v, []).append(k)
    return rev_col_type


def getCategoricalVariables(dataframe):
    cat_vars = []
    cols, numa = getColumns(dataframe)
    for col in cols:
        if len(dataframe[col].value_counts()) <=15:
            cat_vars.append(col)
    return cat_vars


def getNumericalVariables(dataframe):
    s = getColumnTypes(dataframe)
    cols = []
    for k,v in s.items():
        if k == 'float64' or k == 'int64':
            cols.extend(v)
    cat_cols = getCategoricalVariables(dataframe)
    num_cols = [x for x in cols if x not in cat_cols]
    return num_cols

def getCrossTab(dataframe, cat_col_1, cat_col_2):
    df = pd.DataFrame()
    cat_cols = getCategoricalVariables(dataframe)
    if cat_col_1 not in cat_cols or cat_col_2 not in cat_cols:
        return df
    df = pd.crosstab(dataframe[cat_col_1], dataframe[cat_col_2])
    return df

def getCatVarCountPlots(dataframe):
    s = getCategoricalVariables(dataframe)
    nRows = len(s) // 4
    nCols = 4
    _, axes = plt.subplots(nrows=math.ceil(len(s)/4), ncols=nCols, figsize=(18, 6),squeeze=False)
    for i in range(len(s)):
        nr = i//4
        nc = i%4
        sns.countplot(x=s[i], data=dataframe, ax=axes[nr][nc]);
    plt.savefig("categorical.png")

def getNumVarBoxPlots(dataframe):
    s = getNumericalVariables(dataframe)
    nRows = len(s) // 4
    nCols = 4
    _, axes = plt.subplots(nrows=math.ceil(len(s)/4), ncols=nCols, figsize=(18, 9),squeeze=False)
    for i in range(len(s)):
        nr = i//4
        nc = i%4
        sns.boxplot(x=s[i], data=dataframe, ax=axes[nr][nc],orient = 'v');
    plt.savefig("numerical.png")

def getDataFrameInfo(dataframe, info_cols):
    info = dataframe.describe()
    return info[info_cols]

