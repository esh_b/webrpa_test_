import os
import ast
import json
import xlwt
import uuid
import csv
import xlrd
import logging
import shutil
import zipfile
import feedparser
from datetime import datetime, timedelta
from time import sleep
from io import BytesIO
from dashboard.convert import produce_excel, data_to_excel
from accounts.models import Profile, Data_Order
from accounts.forms import UserChangeForm, ProfileChangeForm
#from api_v1.scheduling.models import Schedule
from api_v1.scraping.models import BehindLogin
from api_v1.scraping.models import Project, Execution
from api_v1.scraping.tasks import *
from collections import namedtuple, OrderedDict
from django.conf import settings
from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm, PasswordChangeForm
from django.contrib.auth.views import LoginView, LogoutView, PasswordChangeView, PasswordChangeDoneView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from accounts.models import User
from django.core import serializers
from django.db import connection
from django.http import JsonResponse, Http404
from django.http.request import HttpRequest
from django.http.response import HttpResponse, JsonResponse, HttpResponseRedirect
from django.shortcuts import render, redirect, reverse
from django.template.loader import render_to_string
from django.utils import timezone
from django.urls import reverse_lazy
from django.core.files.storage import FileSystemStorage
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.cache import cache_control
from django import forms
import pandas as pd
from api_v1.feedback.models import Feedback

from schedule.views import update_or_create_schedule
from schedule.views import get_next_run_cron

data_folder = settings.SCRAPING_DATA_DIR
project_folder = settings.SCRAPING_PROJECT_DIR

upload_folder = settings.SCRAPING_UPLOAD_DIR
USER_DASHBD_MAX_EXECS = settings.USER_DASHBD_MAX_EXECS

@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required
def global_home(request):
    projects = Project.objects.filter(user=request.user).order_by("-date_created")[:5]
    num_proj = Project.objects.filter(user=request.user).count()

    totalprojects = num_proj
    totalexecutions = Execution.objects.filter()

    total_size = 0
    prj = Project.objects.filter(user=request.user)
    for project in prj:
        total_size = total_size + int(project.project_size or 0)

    g = 1024 * 1024 * 1024
    m = 1024 * 1024
    k = 1024

    show_size = round(total_size / m, 1)  # MB size
    unit = "MB"

    total_size_limit = 200;  # 200M
    percent = round(show_size / total_size_limit * 100, 3)

    num_proj_limit = 100;  # temp 100 project limit
    storage_ratio = round(totalprojects / num_proj_limit, 1) * 10;
    box_num = range(1, 11)
    blog_num = 1
    max_blog_num = 5
    blog_info = [['お得に買い物をするために！主要ECサイトを一気に比較', 'https://services.sms-datatech.co.jp/pig-data/2018/11/29/try11/',
                  '2018/11/29'], ['インバウンド対策とは？ 厳選インバウンドアプリの事例5選',
                                  'https://services.sms-datatech.co.jp/pig-data/2018/11/22/inboundapp/',
                                  '2018/11/22'],
                 ['企業ゆるキャラランキング2018徹底調査！', 'https://services.sms-datatech.co.jp/pig-data/2018/11/20/try10/',
                  '2018/11/20'], ['無料RPAツール【PigData】 ver.1.0.2リリース',
                                  'https://services.sms-datatech.co.jp/pig-data/2018/11/07/update1/', '2018/11/07'],
                 ['3分でわかる！RPAとAIの違いとは？', 'https://services.sms-datatech.co.jp/pig-data/2018/11/06/rpanai/',
                  '2018/11/06']]

    return render(request, 'global-dashboard.html',
                  {'user': request.user, 'totalprojects': totalprojects, 'unit': unit, 'projects': projects,
                   'num_proj': num_proj, 'percent': percent, 'storage_ratio': storage_ratio, 'box_num': box_num,
                   'total_size_limit': total_size_limit, 'show_size': show_size, 'blog_info': blog_info, 'activate': 'home'})

@login_required()
def edit_project(request: HttpRequest):
    pid = request.GET.get('pid', None)
    pname = request.GET.get('pname', None)
    project = Project.objects.get(id=pid)
    project.name = pname
    project.save()
    response = HttpResponse(json.dumps({'changed': 1, 'err': 'some custom error message'}),
                            content_type='application/json')
    return response

@login_required()
def edit_keyword(request: HttpRequest):
    pid = request.GET.get('pid', None)
    kwname = request.GET.get('kwname', None)
    project = Project.objects.get(id=pid)
    project.keyword = kwname
    if len(kwname)!=0:
        project.keyword_alert = 1
    else:
        project.keyword_alert = 0
    project.save()
    response = HttpResponse(json.dumps({'changed': 1, 'err': 'some custom error message'}),
                            content_type='application/json')
    return response

@csrf_exempt
def profile(request: HttpRequest):
    return render(request, 'profile.html')

@csrf_exempt
def analyze(request: HttpRequest):
    if request.method == 'POST' and request.POST.get("col1"):
        col1 = request.POST.get('col1')
        col2 = request.POST.get('col2')
        print('col1:' + col1 , 'col2:' + col2 )

    if request.method == 'POST' and request.FILES['myfile']:
        myfile = request.FILES['myfile']

        folder = os.path.join(upload_folder, str(request.user.id))
        fs = FileSystemStorage(location=folder)
        print(folder)
        filename = fs.save(myfile.name, myfile)
        uploaded_file_url = fs.url(filename)
        uploaded_file_url = os.path.join(folder, uploaded_file_url)

        data_frame = pd.read_csv(uploaded_file_url)
        print(data_frame.iat[0,2])
        # with open(uploaded_file_url, 'r+') as data_file:
        #     data = csv.DictReader(data_file)
        #     for row in data:
        #         print(row)
        cols = data_frame.columns
        # print(cols.to_dict())
        # rows = data_frame.shape[0]
        cols_count = data_frame.shape[1]
        # print(data_frame.head().to_dict())
        return render(request, 'analyze.html', {
            'uploaded_file_url': uploaded_file_url, 'activate':'analyze_data', 'df': data_frame.head().to_html(), 'rows': data_frame.head(), 'object': data_frame.head().to_dict(), 'cols':cols, 'cols_count': cols_count })
    return render(request, 'analyze.html', {'activate':'analyze_data'})


@login_required()
@csrf_exempt
def settings(request: HttpRequest):
    return render(request, 'settings.html', {'activate':'settings'})

@csrf_exempt
def activity(request: HttpRequest):
    return render(request, 'activity.html')

class PasswordChangeCustomForm(PasswordChangeForm):
    def save(self, commit=True):
        self.user.set_password(self.cleaned_data['new_password1'])
        if commit:
            self.user.save(update_fields=['password'])


class PasswordChange(LoginRequiredMixin, PasswordChangeView):
    form_class = PasswordChangeCustomForm
    success_url = reverse_lazy('global_dashboard:password_change_finish')
    template_name = 'password_change.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["form_name"] = "password_change"
        return context

def password_change_finish(request: HttpRequest):
    return render(request, 'password_change_finish.html', {'activate':'settings'})

@login_required
def new_project(request):
    return render(request, 'new_project.html', {'activate':'new_project'})

def get_project_schedule_params(periodic_task):
    """Function to return the schedule params (required to print in templates)
    
    Args:
        periodic_task (TYPE): Description
    
    Returns:
        TYPE: Description
    """
    params = {}
    if(periodic_task is not None):
        params['next_dt'] = get_next_run_cron(periodic_task.crontab)

        if(periodic_task.interval is not None):
            params['sched_type'] = '1'
            params['every'] = periodic_task.interval.every
        elif(periodic_task.crontab is not None):
            if(periodic_task.crontab.day_of_month == '*'):
                params['sched_type'] = '2'
            else:
                params['sched_type'] = '3'
    else:
        params['sched_type'] = '0'
    return params

@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required
def global_project_list(request):
    #Update project schedule
    try:
        if request.POST.get("scheduleType"):
            pid = request.POST.get("pid")
            sched_type = request.POST.get("scheduleType")
            if request.POST.get("appt"):
                hour, minute = request.POST.get("appt").split(":")
            time_to_run = timezone.now() + timedelta(minutes=-2)
            sched_value = request.POST.get('day', None) or request.POST.get('day-name', None) or request.POST.get('month', None)
            sched_time = request.POST.get("appt", None)

            update_or_create_schedule(request.user.id, pid, sched_type, sched_value, sched_time)
        else:
            print("ELSE:", )
    except Exception as e:
        print("EXCEPTION:", e)
        return HttpResponse(status=500)

    # Project
    num_proj = Project.objects.filter(user=request.user).count()
    projects = OrderedDict()
    if request.POST.get("sort"):
        all_proj = Project.objects.filter(user=request.user).order_by(request.POST.get("sort"))
    else:
        all_proj = Project.objects.filter(user=request.user).order_by("-date_created")

    for project in all_proj:
        projects[project.id] = project
        project.executions = []
        # project.schedule = Schedule.objects.filter(project=project)
        delta = timezone.now() - project.date_created
        project.new_flag = 0
        if delta.seconds < 5*60: # 5minutes
            project.new_flag = 1

        #Project schedule string in human-readable format
        project.sched_params = get_project_schedule_params(project.periodic_task)

        for execution in Execution.objects.filter(project=project).order_by('-date_executed')[:USER_DASHBD_MAX_EXECS]:
            project.executions.append(execution)

    # notification
    if request.POST.get("notification"):
        user = User.objects.get(pk=request.user.id)
        user.profile.notification = request.POST.get("notification")
        notification_list = request.POST.getlist("notification")
        pid = request.POST.get("pid")
        project = Project.objects.get(id=pid)
        if request.POST.get("notification"):
            # seleted option
            for noti_no in notification_list:
                noti_no = int(noti_no)
                if noti_no == 1:
                    project.mail_alert = 1
                elif noti_no == 2:
                    project.data_alert = 1
                elif noti_no == 3:
                    project.keyword_alert = 1
            # unseleted some options
            unseleted_list = [1, 2, 3]
            for noti_no in notification_list:
                unseleted_list.remove(int(noti_no))
            if len(unseleted_list) > 0:
                for noti_no in unseleted_list:
                    if noti_no == 1:
                        project.mail_alert = 0
                    elif noti_no == 2:
                        project.data_alert = 0
                    elif noti_no == 3:
                        project.keyword_alert = 0
        # seleted no options
        elif notification_list == []:
            project.mail_alert = 0
            project.data_alert = 0
            project.keyword_alert = 0
        project.save()
        return HttpResponseRedirect('/global/projects/')

    return render(request, 'download_data.html',
                  {'user': request.user, 'num_proj': num_proj, 'projects': projects, 'all_projects': all_proj,
                   'activate':'download_data'})

@login_required()
@csrf_exempt
def product_type(request: HttpRequest, uid, pid, eid, ft):
    # filestr = request.POST['filetype']
    # uid, pid, eid, ft = filestr.split("#")
    # print(ft)
    print(request.body)
    if ft == '1':
        res = produce_excel(uid, pid, eid)
        project = Project.objects.get(id=pid)
        execution = Execution.objects.get(task_id=eid)
        name = str(project.name) + '_' + str(execution.date_executed).replace(':', '-').replace(' ', '_') + '.xls'
        content_str = 'attachment; filename=' + name
        response = HttpResponse(content_type='application/vnd.ms-excel')
        response['Content-Disposition'] = content_str
        res[0].save(response)
        return response
    elif ft == '2':
        project = Project.objects.get(id=pid)
        execution = Execution.objects.get(task_id=eid)
        name = str(project.name) + '_' + str(execution.date_executed).replace(':', '-').replace(' ', '_') + '.csv'
        content_str = 'attachment;filename=' + name

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = content_str
        writer = csv.writer(response)

        res = produce_excel(uid, pid, eid)
        wb = xlrd.open_workbook(res[1], formatting_info=True)
        for s in wb.sheets():
            sh = wb.sheet_by_name(s.name)
            for rownum in range(sh.nrows):
                # print(sh.row_values(rownum))
                writer.writerow(sh.row_values(rownum))
                # print([x for x in sh.row_values(rownum)])
                # writer.writerow([x for x in sh.row_values(rownum)])
        return response
    elif ft == '3':
        project = Project.objects.get(id=pid)
        execution = Execution.objects.get(task_id=eid)
        name = str(project.name) + '_' + str(execution.date_executed).replace(':', '-').replace(' ', '_') + '.txt'
        content_str = 'attachment;filename=' + name

        response = HttpResponse(content_type='text/plain')
        response['Content-Disposition'] = content_str

        project = Project.objects.get(id=pid)
        execution = Execution.objects.get(task_id=eid)
        filepath = data_folder + uid + '/' + pid + '/' + eid + '.json'
        data = json.loads(open(filepath).read())
        response.content = str(data)
        return response
    elif ft == '4':
        # folder_path = os.path.join('/home/nitish/Desktop/pigdata-white/web-rpa/server/data', uid, pid, eid, '')
        folder_path = os.path.join(data_folder, uid, pid, eid, '')
        # folder_path = folder_path + '/'
        folder_name = pid + '_' + eid
        print("folder_path is: ", folder_path)
        s = BytesIO()
        zf = zipfile.ZipFile(s, "w")
        if os.path.exists(folder_path + folder_name):

            for root, dirs, files in os.walk(folder_path + folder_name):
                for file in files:
                    zf.write(os.path.join(root, file), '/'.join(os.path.join(root, file).split('/')[11:]))
            zf.close()
        else:
            print("Error: File not found!!")

        name = folder_name + '.zip'
        content_str = 'attachment;filename=' + name
        response = HttpResponse(s.getvalue(), content_type="application/x-zip-compressed")
        response['Content-Disposition'] = content_str

        total_size = 0
        start_path = data_folder + uid + '/' + pid + '/'
        total_size = 0
        for dirpath, dirnames, filenames in os.walk(start_path):
            for f in filenames:
                fp = os.path.join(dirpath, f)
                total_size += os.path.getsize(fp)
        print("-------")
        print(total_size)
        prjct = Project.objects.get(id=pid)
        prjct.project_size = total_size
        prjct.save()

        return response
    else:
        print("Type not recognized")
        return HttpResponseRedirect('/global/home/', [pid])

    return HttpResponseRedirect('/global/home/', [pid])

@login_required()
def global_delete_project(request: HttpRequest, pid):
    project = Project.objects.filter(id=pid)
    rows = Execution.objects.filter(project=project[0])
    for r in rows:
        r.delete()

    #Schedule.objects.filter(project=project[0]).delete()
    BehindLogin.objects.filter(project_id=pid).delete()
    Project.objects.filter(id=pid).delete()

    dirname = str(request.user.id)
    filename = str(pid)
    shutil.rmtree(os.path.join(data_folder, dirname, filename))
    os.remove(os.path.join(project_folder, dirname, filename + '.json'))
    return redirect('global_dashboard:all_projects')

@login_required()
def run_now(request: HttpRequest, pid=None):
    task_scrape.delay(request.user.id, pid)
    return HttpResponseRedirect('/global/projects/')

@csrf_exempt
def post_feedback(request: HttpRequest):
    print('\n==========feedback=====\n')
    try:
        start_url = request.POST.get('url_start')
        browser = request.POST.get('web')
        comment = request.POST.get('messag_send')

        try:
            new_feedback = Feedback(user=request.user, start_url=start_url, browser=browser,
                                    comment=comment, timestamp=datetime.now())
            new_feedback.save()
            status_code = 200
        except:
            status_code = 500

        return JsonResponse(status_code, safe=False)
    except HTTPException as ex:
        return ex.as_response()
    except Exception:
        logger = logging.getLogger(__name__)
        logger.exception('Internal server error')
        return HttpResponse(status=500)

@csrf_exempt
def instructions(request: HttpRequest):
    return render(request, 'instructions.html')

@csrf_exempt
def profile(request: HttpRequest):
    return render(request, 'profile.html')

@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required
@csrf_exempt
def setting(request: HttpRequest):
    user = User.objects.get(pk=request.user.id)
    profile = Profile.objects.get(user=request.user.id)

    #user_form = UserChangeForm(instance=user)
    profile_form = ProfileChangeForm(instance=profile)
    if request.method == 'POST':
        #user_form = UserChangeForm(request.POST, instance=user)
        profile_form = ProfileChangeForm(request.POST, instance=profile)
        if profile_form.is_valid():
            try:
                #user_form.save()
                profile_form.save(update_fields=profile_form.changed_data)
                return render(request, 'profile_change_finish.html')
            except:
                return render(request, 'system_error.html', {'back': '../profile_change'})
        else:
            #print(user_form.errors)
            print(profile_form.errors)
            return render(request, 'profile_change.html', {'user': user, 'profile_form': profile_form})

    return render(request, 'setting.html', {'user': user, 'profile_form': profile_form})

@csrf_exempt
def activity_log(request: HttpRequest):
    projects = Project.objects.filter(user=request.user).order_by("-date_created")[:10]

    return render(request, 'activity_log.html', {'projects': projects })

@csrf_exempt
def email_change(request):
    if (request.method == 'POST'):
        new_email = request.POST.get('email')

        user = User.objects.get(pk=request.user.id)
        user.old_email = user.email
        user.email     = new_email
        user.username  = new_email
        user.is_active = 0
        user.save(update_fields=['username', 'old_email', 'email', 'is_active'])

    return account_setting_basic(request)


@csrf_exempt
class DataOrderForm(forms.ModelForm):
    MONEY_CHOICES = (
    ('0~50,000', '〜50,000'), ('50,000~100,000', '50,000〜100,000'), ('100,000~500,000', '100,000〜500,000'),
    ('500,000~1,000,000', '500,000〜1,000,000'), ('1,000,000~5,000,000', '1,000,000〜5,000,000'),
    ('5,000,000', '5,000,000〜'))
    UPDATE_CHOICES = ((1, 'はい'), (0, 'いいえ'))

    url_list = forms.CharField(label="URL", max_length=100, widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'https://', 'pattern': 'https?://.+'}))
    data_details = forms.CharField(label="Data Details",
                                   widget=forms.Textarea(attrs={'class': '', 'placeholder': "", 'cols': 53, 'rows': 6}))
    due_date = forms.DateField(label="Due Date", widget=forms.DateInput(
        attrs={'class': 'form-control', 'placeholder': timezone.now().strftime("%Y/%m/%d"), 'id': 'datepicker'},
        format='%Y/%m/%d'), input_formats=('%Y/%m/%d',))
    purpose = forms.CharField(label="Acquisition Purpose",
                              widget=forms.Textarea(attrs={'class': '', 'placeholder': "", 'cols': 53, 'rows': 6}))
    company = forms.CharField(max_length=100,
                              widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': "会社名"}))
    department = forms.CharField(max_length=100,
                                 widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': "部署名"}))
    budget = forms.ChoiceField(label="", choices=MONEY_CHOICES, widget=forms.Select(attrs={'class': 'dropdown'}))
    email = forms.CharField(label="Contact Email", max_length=100, widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': "pigdata@example.com",
               'pattern': '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$'}))
    phone_number = forms.CharField(label="Phone Number", max_length=100, widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': "0312345678", 'pattern': '\d{2,4}\d{3,4}\d{3,4}'}))
    regular_order = forms.ChoiceField(widget=forms.RadioSelect, choices=UPDATE_CHOICES, required=False)

    class Meta:
        model = Data_Order
        fields = (
        'url_list', 'data_details', 'due_date', 'purpose', 'company', 'department', 'budget', 'email', 'phone_number',
        'regular_order')

@csrf_exempt
def data_order(request: HttpRequest):
    form = DataOrderForm()
    return render(request, 'data_order.html', {'user': request.user, 'form': form, 'activate':'data_order'})

@csrf_exempt
def data_order_form(request: HttpRequest):
    if request.method == 'POST':
        if 'form' in request.POST:
            # after filling in
            form = DataOrderForm(request.POST)
            if form.is_valid():
                return render(request, 'data_order_confirm.html', {'form': form})
            else:
                return render(request, 'data_order_form.html', {'form': form})
        else:
            # before filling in
            url_list = request.POST.get("url_list")
            form = DataOrderForm(initial={'url_list': url_list, 'regular_order': 0})
            return render(request, 'data_order_form.html', {'user': request.user, 'form': form})

    return redirect('global_dashboard:data_order')

@csrf_exempt
def data_order_confirm(request: HttpRequest):
    if request.method == 'POST':
        form = DataOrderForm(request.POST)
        # after sending final form
        if 'done' in request.POST:
            if form.is_valid():
                result = form.save(commit=False)
                user = User.objects.get(pk=request.user.id) if request.user.id is not None else None
                result.user = user
                result.created_at = timezone.now()
                result.save()

                return render(request, 'data_order_done.html', {'form': form, 'email': result.email})
            else:
                return render(request, 'data_order_form.html', {'form': form})
        # after clicking back button
        elif 'back' in request.POST:
            return render(request, 'data_order_form.html', {'form': form})
        # error
        else:
            return render(request, 'system_error.html', {'back': '../data_order'})

    return redirect('global_dashboard:data_order')

@csrf_exempt
def data_order_done(request: HttpRequest):
    if request.method == 'POST':
        return render(request, 'data_order_done.html')

    return redirect('global_dashboard:data_order')

# Password Setting
class PasswordChangeCustomForm(PasswordChangeForm):
    def save(self, commit=True):
        self.user.set_password(self.cleaned_data['new_password1'])
        if commit:
            self.user.save(update_fields=['password'])


class PasswordChange(LoginRequiredMixin, PasswordChangeView):
    form_class = PasswordChangeCustomForm
    success_url = reverse_lazy('global_dashboard:password_change_finish')
    template_name = 'password_change.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["form_name"] = "password_change"
        return context

@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required
def password_change_finish(request: HttpRequest):
    return render(request, 'password_change_finish.html')

# cancel
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required
def cancel(request: HttpRequest):
    if request.method == "POST":
        user = User.objects.get(pk=request.user.id)
        user.is_active = 0
        user.save()
        return render(request, 'cancel_finish.html')

    return render(request, 'cancel.html')

@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required
def cancel_finish(request: HttpRequest):
    return render(request, 'cancel_finish.html')

