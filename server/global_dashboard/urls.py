from . import views
from . import  analyze as analyze_views
from django.conf.urls import url, include
from django.contrib.auth import views as auth_views
from django.urls import path

app_name = 'global_dashboard'

urlpatterns = [
    url(r'global/home/$', views.global_home, name='index'),
    url(r'global/new/project$', views.new_project, name='new_project'),
    url(r'global/projects/$', views.global_project_list, name='all_projects'),
    url(r'ajax/(?P<uid>[^/]+)/(?P<pid>[^/]+)/(?P<eid>[^/]+)/(?P<ft>[^/]+)$', views.product_type, name='product_type'),
    url(r'running/(?P<pid>[^/]+)$', views.run_now, name='run_now'),
    path(r'global/delete/(?p<pid>[^/]+)$', views.global_delete_project, name='global_delete_project'),
    url(r'^project/list/i18n/', include('django.conf.urls.i18n')),
    url(r'global/profile/$', views.profile, name='profile'),
    url(r'global/setting/$', views.setting, name='setting'),
    url(r'global/activity_log/$', views.activity_log, name='activity_log'),
    url(r'global/feedback/$', views.post_feedback, name='feedback'),
    url(r'global/instructions/$', views.instructions, name='instructions'),
    path(r'ajax_project/$', views.edit_project, name='edit_project'),
    path(r'ajax_keyword/$', views.edit_keyword, name='edit_keyword'),

    url(r'global/profile/$', views.profile, name='profile'),
    url(r'global/analyze/$', analyze_views.analyze, name='analyze'),
    url(r'global/activity/$', views.activity, name='activity'),
    url(r'global/settings/$', views.settings, name='settings'),

    url(r'global/data_order/', views.data_order, name='data_order'),
    url(r'global/data_order_form/', views.data_order_form, name='data_order_form'),
    url(r'global/data_order_confirm/', views.data_order_confirm, name='data_order_confirm'),
    url(r'global/data_order_done/', views.data_order_done, name='data_order_done'),
    url(r'global/password_change/$', views.PasswordChange.as_view(), name='password_change'),
    url(r'global/password_change_finish/$', views.password_change_finish, name='password_change_finish'),
    url(r'global/email_change/$', views.email_change, name='email_change'),
    url(r'global/cancel/$', views.cancel, name='cancel'),
    url(r'global/cancel_finish/$', views.cancel_finish, name='cancel_finish'),
]
