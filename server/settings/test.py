from .base import *  # noqa

SITE_URL = "localhost:8000"

#Link to user account profile page
ACCOUNT_PROFILE_LINK = os.path.join(SITE_URL, "account_setting_basic/")

#Link to user payment information page
ACCOUNT_PAYMENT_INFO_LINK = os.path.join(SITE_URL, "account_payment/")

# Database
# https://docs.djangoproject.com/en/2.0/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'sample_db',
        'HOST': 'mysql',
        'USER': 'root',
        'PASSWORD': 'root',
        'OPTIONS': {
            'init_command': "SET sql_mode='STRICT_TRANS_TABLES'",
        },
    }
}
