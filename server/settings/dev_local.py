from .base import *  # noqa

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

SITE_URL = "http://localhost:8000"

#Link to user account profile page
ACCOUNT_PROFILE_LINK = os.path.join(SITE_URL, "account_setting_basic/")

#Link to user payment information page
ACCOUNT_PAYMENT_INFO_LINK = os.path.join(SITE_URL, "account_payment/")

# Database
# https://docs.djangoproject.com/en/2.0/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'pigdata_apr_new',
        'HOST': 'localhost',
        'USER': 'root',
        'PASSWORD': 'root',
        'OPTIONS': {
            'init_command': "SET sql_mode='STRICT_TRANS_TABLES'",
        },
    }
}

# TIME_ZONE = 'UTC'

# CELERY_ENABLE_UTC = True