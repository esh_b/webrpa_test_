from .base import *  # noqa

SITE_URL = "https://pig-data.sms-datatech.co.jp"

#Link to user account profile page
ACCOUNT_PROFILE_LINK = os.path.join(SITE_URL, "account_setting_basic/")

#Link to user payment information page
ACCOUNT_PAYMENT_INFO_LINK = os.path.join(SITE_URL, "account_payment/")

# Database
# https://docs.djangoproject.com/en/2.0/ref/settings/#databases
# DATABASES = {
#     'default': {
#         'ENGINE': 'django.db.backends.mysql',
#         'NAME': 'pigdata_saas_developing',
#         'HOST': '192.168.20.152',
#         'USER': 'nitish',
#         'PASSWORD': 'nitish123',
#         'OPTIONS': {
#             'init_command': "SET sql_mode='STRICT_TRANS_TABLES'",
#         },
#     }
# }
