import os
import ast
import json
import xlwt
import uuid
import csv
import xlrd
import logging
import shutil
import zipfile
import payjp
import feedparser
from datetime import datetime, timedelta
from time import sleep
from io import BytesIO
from dashboard.convert import produce_excel, data_to_excel
from accounts.models import Profile, Data_Order
from accounts.forms import UserChangeForm, ProfileChangeForm
#from api_v1.scheduling.models import Schedule
from api_v1.scraping.models import BehindLogin
from api_v1.scraping.models import Project, Execution
from api_v1.scraping.tasks import *
from collections import namedtuple, OrderedDict
from django.conf import settings
from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm, PasswordChangeForm
from django.contrib.auth.views import LoginView, LogoutView, PasswordChangeView, PasswordChangeDoneView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.decorators import login_required
from accounts.models import User
from django.core.mail import send_mail
from django.core import serializers
from django.db import connection
from django.http import JsonResponse, Http404
from django.http.request import HttpRequest
from django.http.response import HttpResponse, JsonResponse, HttpResponseRedirect
from django.shortcuts import render, redirect, reverse
from django.template.loader import render_to_string
from django.utils import timezone
from django.urls import reverse_lazy
from django.views.static import serve
from django.views.generic import View
from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.cache import cache_control
from django import forms

from django_celery_results.models import TaskResult

from schedule.views import update_or_create_schedule

from cron_descriptor import get_description

data_folder = settings.SCRAPING_DATA_DIR
project_folder = settings.SCRAPING_PROJECT_DIR


class PasswordChangeCustomForm(PasswordChangeForm):
    def save(self, commit=True):
        self.user.set_password(self.cleaned_data['new_password1'])
        if commit:
            self.user.save(update_fields=['password'])


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required
def index(request: HttpRequest):
    projects = Project.objects.filter(user=request.user).order_by("-date_created")[:5]
    num_proj = Project.objects.filter(user=request.user).count()

    totalprojects = num_proj
    totalexecutions = Execution.objects.filter()

    total_size = 0
    prj = Project.objects.filter(user=request.user)
    for project in prj:
        total_size = total_size + int(project.project_size or 0)

    g = 1024 * 1024 * 1024
    m = 1024 * 1024
    k = 1024

    show_size = round(total_size / m, 1)  # MB size
    unit = "MB"

    total_size_limit = 200;  # 200M
    percent = round(show_size / total_size_limit * 100, 3)

    num_proj_limit = 100;  # temp 100 project limit
    storage_ratio = round(totalprojects / num_proj_limit, 1) * 10;
    box_num = range(1, 11)
    blog_num = 1
    max_blog_num = 5
    blog_info = [['お得に買い物をするために！主要ECサイトを一気に比較', 'https://services.sms-datatech.co.jp/pig-data/2018/11/29/try11/', '2018/11/29'], ['インバウンド対策とは？ 厳選インバウンドアプリの事例5選', 'https://services.sms-datatech.co.jp/pig-data/2018/11/22/inboundapp/', '2018/11/22'], ['企業ゆるキャラランキング2018徹底調査！', 'https://services.sms-datatech.co.jp/pig-data/2018/11/20/try10/', '2018/11/20'], ['無料RPAツール【PigData】 ver.1.0.2リリース', 'https://services.sms-datatech.co.jp/pig-data/2018/11/07/update1/', '2018/11/07'], ['3分でわかる！RPAとAIの違いとは？', 'https://services.sms-datatech.co.jp/pig-data/2018/11/06/rpanai/', '2018/11/06']]


    return render(request, 'index.html',
                  {'user': request.user, 'totalprojects': totalprojects, 'unit': unit, 'projects': projects,
                   'num_proj': num_proj, 'percent': percent, 'storage_ratio': storage_ratio, 'box_num': box_num,
                   'total_size_limit': total_size_limit, 'show_size': show_size, 'blog_info': blog_info})

@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required
def project_list(request: HttpRequest):
    #request.META['LANGUAGE'] = 'en_IN'

    # Rename Categoronfirm safe time
    if request.POST.get("category-rename-target"):
        category_rename_origin = request.POST.get("category-rename-origin")
        category_rename_target = request.POST.get("category-rename-target")

        projects = Project.objects.filter(user=request.user, category=category_rename_origin)
        for project in projects:
            project.category = category_rename_target
            project.save()

    # Create category
    elif request.POST.get("category-create"):
        new_category = request.POST.get("category-create")
        pid = request.POST.get("projectId")

        project = Project.objects.get(id=pid)
        project.category = new_category
        project.save()

    # Move project into defferent category
    elif request.POST.get("category-move-target"):
        pid_origin = request.POST.get("projectId")
        project_move_origin = Project.objects.get(id=pid_origin)

        category_move_target = request.POST.get("category-move-target") 

        project_move_origin.category = category_move_target
        project_move_origin.save()

    # display mode
    if request.POST.get("mode") == "projectMode":
        currentMode = "projectMode"
    else:
        currentMode = "categoryMode"

    # Project
    num_proj = Project.objects.filter(user=request.user).count()
    projects = OrderedDict()

    if request.POST.get("sort"):
        all_proj = Project.objects.filter(user=request.user).order_by(request.POST.get("sort"))
    else:
        all_proj = Project.objects.filter(user=request.user).order_by("-date_created")

    for project in all_proj:
        projects[project.id] = project
        project.executions = []
        #project.schedule = Schedule.objects.filter(project=project)

        #Project schedule string in human-readable format
        if(project.periodic_task is not None):
            if(project.periodic_task.crontab is not None):
                crontab_str_chunks = str(project.periodic_task.crontab).split(" ")
                crontab_str = ' '.join([crontab_str_chunks[0], crontab_str_chunks[1], crontab_str_chunks[3], crontab_str_chunks[4], crontab_str_chunks[2]])
                project.schedule_str = get_description(crontab_str)
            elif(project.periodic_task.interval is not None):
                project.schedule_str = "Every " + str(project.periodic_task.interval.every) + " days"
        else:
            project.schedule_str = "None"

    for execution in Execution.objects.filter(project__in=projects).order_by('-date_executed'):
        project = projects[execution.project.id]
        project.executions.append(execution)

    category_list = ["No Category"]
    category_list.extend(
        Project.objects.filter(user=request.user).exclude(category="No category").values_list("category",
                                                                                              flat=True).order_by("category").distinct())
    if currentMode == "categoryMode":
        categorys = category_list
    elif currentMode == "projectMode":
        categorys = [""]

    #Update project schedule
    try:
        if request.POST.get("scheduleType"):
            pid = request.POST.get("projectId")
            time_to_run = timezone.now() + timedelta(minutes=-2)
            sched_type = request.POST.get("scheduleType")
            sched_value = request.POST.get('day', None) or request.POST.get('day-name', None) or request.POST.get('month', None)

            update_or_create_schedule(request.user.id, pid, sched_type, sched_value, time_to_run)
    except Exception as e:
        print("EXCEPTION:", e)
        return HttpResponse(status=500)

    # notification
    user = User.objects.get(pk=request.user.id)
    if request.POST.get("notification"):
        user.profile.notification = request.POST.get("notification")
        notification_list = request.POST.getlist("notification")
        pid = request.POST.get("projectId")
        project = Project.objects.get(id = pid)
        if len(notification_list) == 3:
            project.mail_alert = 1
            project.data_alert = 1   
        elif len(notification_list) == 1:
            project.mail_alert = 0
            project.data_alert = 0         
        elif notification_list[0]=='1':
            project.mail_alert = 1
            project.data_alert = 0
        else:
            project.mail_alert = 0
            project.data_alert = 1
        project.save()
        return HttpResponseRedirect(reverse("dashboard:project_list"))

    return render(request, 'project_list.html',
                  {'user': request.user, 'num_proj': num_proj, 'projects': projects, 'all_projects': all_proj,
                   'categorys': categorys, 'category_list': category_list, 'currentMode': currentMode})


@login_required
def get_project(request: HttpRequest):
    pid = request.GET['pid']
    project = Project.objects.get(id=pid)

    executions = {
        "id": [],
        "date": [],
        "status": []
    }

    for execution in Execution.objects.filter(project=project).order_by('-date_executed'):
        executions["id"].append(execution.id)
        executions["date"].append(execution.date_executed)
        executions["status"].append(execution.status)

    return JsonResponse(executions)


@login_required
def auto_refresh(request: HttpRequest):
    if request.method == "GET":
        eid_list = request.GET.getlist('eids[]')
        executions = OrderedDict()
        executions = {
            "id": [],
            "status": [],
            "error": [],
            #"scrape_time": []
        }

        for execution in Execution.objects.filter(id__in=eid_list).order_by('-date_executed'):
            executions["id"].append(execution.id)
            executions["status"].append(execution.status)
                #executions["scrape_time"].append(str(datetime.now() - execution.date_executed))
        if executions["id"] != []:
            return JsonResponse(executions, safe=False)
    return JsonResponse({'status': 'false'})

"""
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required
def project_schedule(request: HttpRequest):
    project_obj = Project.objects.filter(user=request.user).order_by('-date_created')

    d = {}
    for obj in project_obj:
        sched = Schedule.objects.filter(project=Project.objects.get(id=obj.id))
        d[obj.id] = sched

    with connection.cursor() as cursor:
        cursor.execute(
            "select * from scheduling_schedule right join scraping_project on scraping_project.id=scheduling_schedule.project_id where scraping_project.user_id=%s order by scraping_project.date_created desc",
            [request.user.id])
        project_obj = cursor.fetchall()

    return render(request, 'schedule.html', {'project_schedule': project_obj, 'sched': d})

@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required
def schedule_form(request: HttpRequest):
    if request.method == "POST":
        st = request.POST.get("scheduleType")
        pid = request.POST.get("pid")

        scheduled = Schedule.objects.filter(project=Project.objects.get(id=pid)).update(schedule_type=st,
                                                                                        last_date=timezone.now(),
                                                                                        next_date=timezone.now())
        if not scheduled:
            p = Schedule(schedule_type=st, project=Project.objects.get(id=pid), last_date=timezone.now(),
                         next_date=timezone.now())
            p.save()

    with connection.cursor() as cursor:
        cursor.execute(
            "select * from scheduling_schedule right join scraping_project on scraping_project.id=scheduling_schedule.project_id where scraping_project.user_id=%s order by scraping_project.date_created desc",
            [request.user.id])
        project_obj = cursor.fetchall()

    return render(request, 'schedule.html', {'project_schedule': project_obj})
"""

@login_required()
def managefiletype(request: HttpRequest):
    uid = request.GET.get('uid', None)
    pid = request.GET.get('pid', None)
    eid = request.GET.get('eid', None)
    file_path = data_folder + uid + '/' + pid + '/' + eid + '.json'
    data = []
    print('\n******************\n')
    print(file_path)
    with open(file_path) as f:
        data = json.load(f)
    response = HttpResponse(json.dumps({'mediatype': data[2], 'err': 'some custom error message'}),
                            content_type='application/json')
    return response


@login_required()
def managefiletype1(request: HttpRequest):
    uid = request.GET.get('uid', None)
    pid = request.GET.get('pid', None)
    eid = request.GET.get('eid', None)
    start_path = data_folder + uid + '/' + pid + '/'
    total_size = 0
    for dirpath, dirnames, filenames in os.walk(start_path):
        for f in filenames:
            fp = os.path.join(dirpath, f)
            total_size += os.path.getsize(fp)
    print("-------")
    print(total_size)
    project = Project.objects.get(id=pid)
    project.project_size = total_size
    project.save()
    response = HttpResponse(json.dumps({'mediatype': 1, 'err': 'some custom error message'}),
                            content_type='application/json')
    return response


@login_required()
@csrf_exempt
def product_type(request: HttpRequest):
    filestr = request.POST['filetype']
    uid, pid, eid, ft = filestr.split("#")
    print(ft)
    if ft == '1':
        res = produce_excel(uid, pid, eid)
        project = Project.objects.get(id=pid)
        execution = Execution.objects.get(id=eid)
        name = str(project.name) + '_' + str(execution.date_executed).replace(':', '-').replace(' ', '_') + '.xlsx'
        content_str = 'attachment; filename=' + name
        response = HttpResponse(content_type='application/vnd.ms-excel')
        response['Content-Disposition'] = content_str
        res[0].save(response)
        return response
    elif ft == '2':
        project = Project.objects.get(id=pid)
        execution = Execution.objects.get(id=eid)
        name = str(project.name) + '_' + str(execution.date_executed).replace(':', '-').replace(' ', '_') + '.csv'
        content_str = 'attachment;filename=' + name

        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = content_str
        writer = csv.writer(response)

        res = produce_excel(uid, pid, eid)
        wb = xlrd.open_workbook(res[1], formatting_info=True)
        for s in wb.sheets():
            sh = wb.sheet_by_name(s.name)
            for rownum in range(sh.nrows):
                writer.writerow(sh.row_values(rownum))
        return response
    elif ft == '3':
        project = Project.objects.get(id=pid)
        execution = Execution.objects.get(id=eid)
        name = str(project.name) + '_' + str(execution.date_executed).replace(':', '-').replace(' ', '_') + '.txt'
        content_str = 'attachment;filename=' + name

        response = HttpResponse(content_type='text/plain')
        response['Content-Disposition'] = content_str

        project = Project.objects.get(id=pid)
        execution = Execution.objects.get(id=eid)
        filepath = data_folder + uid + '/' + pid + '/' + eid + '.json'
        data = json.loads(open(filepath).read())
        response.content = str(data)
        return response
    elif ft == '4':
        # folder_path = os.path.join('/home/nitish/Desktop/pigdata-white/web-rpa/server/data', uid, pid, eid, '')
        folder_path = os.path.join(data_folder, uid, pid, eid, '')
        # folder_path = folder_path + '/'
        folder_name = pid + '_' + eid
        print("folder_path is: ", folder_path)
        s = BytesIO()
        zf = zipfile.ZipFile(s, "w")
        if os.path.exists(folder_path + folder_name):

            for root, dirs, files in os.walk(folder_path + folder_name):
                for file in files:
                    zf.write(os.path.join(root, file), '/'.join(os.path.join(root, file).split('/')[11:]))
            zf.close()
        else:
            print("Error: File not found!!")

        name = folder_name + '.zip'
        content_str = 'attachment;filename=' + name
        response = HttpResponse(s.getvalue(), content_type="application/x-zip-compressed")
        response['Content-Disposition'] = content_str

        total_size = 0
        start_path = data_folder + uid + '/' + pid + '/'
        total_size = 0
        for dirpath, dirnames, filenames in os.walk(start_path):
            for f in filenames:
                fp = os.path.join(dirpath, f)
                total_size += os.path.getsize(fp)
        print("-------")
        print(total_size)
        prjct = Project.objects.get(id=pid)
        prjct.project_size = total_size
        prjct.save()

        return response
    else:
        print("Type not recognized")
        return HttpResponseRedirect('/home/', [pid])

    return HttpResponseRedirect('/home/', [pid])


@login_required()
def run(request: HttpRequest, pid=None):
    print('----inside run----')
    try:
        #: :type project: Project
        project = Project.objects.get(
            id=pid, user=request.user)
    except Project.DoesNotExist:
        raise HTTPException(404)

    res = task_scrape.delay(request.user.id, project.id)
    return HttpResponseRedirect('/project/list/', [pid])


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required()
def upgrade(request: HttpRequest):
    return render(request, 'upgrade.html')


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required()
def settings(request: HttpRequest):
    return render(request, 'setting.html')


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required()
def delete_account(request: HttpRequest):
    return render(request, 'project_details.html')


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required()
def account_setting_basic(request: HttpRequest):
    user = User.objects.get(pk=request.user.id)
    profile = Profile.objects.get(user=request.user.id)

    total_size = 0
    start_path = os.path.join(data_folder, str(request.user.id))
    for dirpath, dirnames, filenames in os.walk(start_path):
        for f in filenames:
            fp = os.path.join(dirpath, f)
            total_size += os.path.getsize(fp)

    g = 1024 * 1024 * 1024
    m = 1024 * 1024
    k = 1024

    show_size = round(total_size / m, 1)  # MB size
    unit = "MB"

    # if total_size > g:
    #    show_size = round(total_size / g, 1)
    #    unit = "GB"
    # elif total_size > m:
    #    show_size = round(total_size / m, 1)
    #    unit = "MB"
    # elif total_size > k:
    #    show_size = round(total_size / k, 1)
    #    unit = "KB"
    # else:
    #    show_size = round(total_size, 1)
    #    unit = "B"

    total_size_limit = 200;  # 200M
    percent = show_size / total_size_limit * 100

    proj_limit = 100
    num_proj = Project.objects.filter(user=request.user).count()
    remain_proj = proj_limit - num_proj

    return render(request, 'account_setting_basic.html',
                  {'user': user, 'profile': profile, 'plan': 1, 'payment': '',
                   'show_size': show_size, 'total_size_limit': total_size_limit, 'percent': percent,
                   'remain_proj': remain_proj})


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required()
def agreement_payment(request: HttpRequest):
    return render(request, 'agreement_payment.html')


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required()
def profile_change(request: HttpRequest):
    user = User.objects.get(pk=request.user.id)
    profile = Profile.objects.get(user=request.user.id)

    #user_form = UserChangeForm(instance=user)
    profile_form = ProfileChangeForm(instance=profile)
    messages = False
    if request.method == 'POST':
        #user_form = UserChangeForm(request.POST, instance=user)
        profile_form = ProfileChangeForm(request.POST, instance=profile)
        if profile_form.is_valid():
            try:
                #user_form.save()
                profile_form.save(update_fields=profile_form.changed_data)
                messages = True
                return render(request, 'profile_change_finish.html')
            except:
                return render(request, 'system_error.html', {'back': '../profile_change'})
        else:
            #print(user_form.errors)
            print(profile_form.errors)
            return render(request, 'profile_change.html',
                          {'user': user, 'profile_form': profile_form})

    return render(request, 'profile_change.html', {'user': user, 'profile_form': profile_form})


class PasswordChange(LoginRequiredMixin, PasswordChangeView):
    form_class = PasswordChangeCustomForm
    success_url = reverse_lazy('dashboard:password_change_finish')
    template_name = 'password_change.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["form_name"] = "password_change"
        return context


@csrf_exempt
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required()
def email_change(request):
    if (request.method == 'POST'):
        uid = request.POST.get('uid')
        new_email = request.POST.get('email')

        user = User.objects.get(pk=uid)
        user.old_email = user.email
        user.email = new_email
        user.username = new_email
        user.is_active = 0
        user.save(update_fields=['username', 'old_email', 'email', 'is_active'])

    return account_setting_basic(request)


# return JsonResponse({'status':'success'}, status=200)


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required()
def notification_setting(request: HttpRequest):
    user = User.objects.get(pk=request.user.id)
    if user.profile.notification == True:
        status = 'Yes'
    else:
        status = 'No'

    class NotificationForm(forms.Form):
        user = forms.IntegerField(initial=request.user.id)
        company = forms.CharField(max_length=100, widget=forms.TextInput(
            attrs={'class': 'form-control', 'placeholder': "Ltd. SMS DataTech"}))
        company_kana = forms.CharField(max_length=100, widget=forms.TextInput(
            attrs={'class': 'form-control', 'placeholder': "株式会社エスエムエスデータテック"}))
        department = forms.CharField(max_length=100,
                                     widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': "システム部"}))
        position = forms.CharField(max_length=100,
                                   widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': "主任"}))
        phone_number = forms.CharField(max_length=100, widget=forms.TextInput(
            attrs={'class': 'form-control', 'placeholder': "03-1234-5678", 'pattern': '\d{2,4}-\d{3,4}-\d{3,4}'}))
        address = forms.CharField(max_length=100, widget=forms.TextInput(
            attrs={'class': 'form-control', 'placeholder': "東京都中央区湊3-5-10 VORT新富町 3F"}))

    form = NotificationForm()

    return render(request, 'notifications.html', {'status': status, 'form': form})


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required()
def notification_change(request: HttpRequest):
    if request.method == "POST":
        notification_status = request.POST.get("email_notification")

    user = User.objects.get(pk=request.user.id)

    if notification_status == '1':
        user.profile.notification = True
        status = 'Yes'
        user.save()
        notification_message = 'Your mail notification settings has been updated.'
    elif notification_status == '0':
        user.profile.notification = False
        status = 'No'
        user.save()
        notification_message = 'Your mail notification settings has been updated. you will not receive anymore notifications.'
    else:
        notification_message = 'Please select an option.'

    return render(request, 'notifications.html', {'notification_status': notification_message, 'status': status})


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required()
def update_profile(request: HttpRequest):
    user = User.objects.get(id=request.user.id)
    profile = Profile.objects.get(user=request.user.id)
    if request.method == 'POST':
        class ReceiveNotificationForm(forms.ModelForm):
            class Meta:
                model = Profile
                fields = ['user', 'company', 'company_kana', 'department', 'position', 'phone_number', 'address']

        form = ReceiveNotificationForm(request.POST, instance=profile)
        if form.is_valid():
            form.save()

            # email report
            subject = 'Changing account information Complete'
            message = "You finish changing account information."
            from_email = 'no-reply@sms-datatech.co.jp'
            user = User.objects.get(id=request.user.id)
            send_mail(subject, message, from_email, [user.email])
        else:
            print(form.errors)
            return redirect('dashboard:notification_setting')

    return render(request, 'notifications.html', {'form': form})


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required()
def change_password(request: HttpRequest):
    if request.method == 'POST':
        # form = PasswordChangeForm(request.user, request.POST)
        form = PasswordChangeCustomForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, 'Your password was successfully updated!')
            return redirect('dashboard:change_password')
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        form = PasswordChangeCustomForm(request.user)
    return render(request, 'change_password.html', {
        'form': form
    })


@login_required()
def zip_data(request: HttpRequest, pid):
    project = Project.objects.filter(id=pid)
    rows = Execution.objects.filter(project=project[0])

    directory = os.path.join(data_folder, str(request.user.id), str(pid), 'zip')

    if not os.path.exists(directory):
        os.makedirs(directory)

    s = BytesIO()
    zf = zipfile.ZipFile(s, "w")
    for r in rows:
        filepath = os.path.join(data_folder, str(request.user.id), pid, str(r.id) + '.json')
        op = os.path.join(directory, str(r.id) + '.xls')
        data_to_excel(filepath, op)
        name = str(project[0].name) + '_' + str(r.date_executed).replace(':', '-').replace(' ', '_') + '.xls'
        zf.write(op, name)

    zf.close()

    name = str(project[0].name) + '.zip'
    content_str = 'attachment;filename=' + name
    response = HttpResponse(s.getvalue(), content_type="application/x-zip-compressed")
    response['Content-Disposition'] = content_str

    return response


@login_required()
def delete_category(request: HttpRequest, cname):
    projects = Project.objects.filter(user=request.user, category=cname)

    for project in projects:
        pid = project.id
        rows = Execution.objects.filter(project=pid)
        for r in rows:
            r.delete()

        #Schedule.objects.filter(project=pid).delete()
        Project.objects.filter(id=pid).delete()

        dirname = str(request.user.id)
        filename = str(pid)
        shutil.rmtree(os.path.join(data_folder, dirname, filename))
        os.remove(os.path.join(project_folder, dirname, filename + '.json'))
    return redirect('dashboard:project_list')


@login_required()
def delete_project(request: HttpRequest, pid):
    project = Project.objects.filter(id=pid)
    print(project)

    rows = Execution.objects.filter(project=project[0])
    for r in rows:
        r.delete()

    #Schedule.objects.filter(project=project[0]).delete()
    BehindLogin.objects.filter(project_id=pid).delete()
    Project.objects.filter(id=pid).delete()

    dirname = str(request.user.id)
    filename = str(pid)
    shutil.rmtree(os.path.join(data_folder, dirname, filename))
    os.remove(os.path.join(project_folder, dirname, filename + '.json'))
    return redirect('dashboard:project_list')


@login_required()
def edit_project(request: HttpRequest):
    pid = request.GET.get('pid', None)
    pname = request.GET.get('pname', None)
    project = Project.objects.get(id=pid)
    project.name = pname
    project.save()
    response = HttpResponse(json.dumps({'changed': 1, 'err': 'some custom error message'}),
                            content_type='application/json')
    return response

@login_required()
def edit_keyword(request: HttpRequest):
    pid = request.GET.get('pid', None)
    kwname = request.GET.get('kwname', None)
    project = Project.objects.get(id=pid)
    project.keyword = kwname
    if len(kwname)!=0:
        project.keyword_alert = 1
    else:
        project.keyword_alert = 0
    project.save()
    response = HttpResponse(json.dumps({'changed': 1, 'err': 'some custom error message'}),
                            content_type='application/json')
    return response


@login_required()
def delete_execution(request: HttpRequest, pid, eid):
    execution = Execution.objects.get(id=eid)
    if execution.status == 1:
        user_dir = str(request.user.id)
        proj_dir = str(pid)
        file_path = os.path.join(project_folder, user_dir, proj_dir, eid + '.json')
        if os.path.isfile(file_path):
            os.remove(file_path)

    Execution.objects.filter(id=eid).delete()
    return redirect('dashboard:project_data')


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required()
def project_details(request: HttpRequest, pid):
    # return render(request, 'details.html', {'pid': pid, 'pname': project[0].name})
    return render(request, 'details.html', {'user': str(request.user.id), 'pid': pid})


class DataOrderForm(forms.ModelForm):
    MONEY_CHOICES = (
    ('0~50,000', '〜50,000'), ('50,000~100,000', '50,000〜100,000'), ('100,000~500,000', '100,000〜500,000'),
    ('500,000~1,000,000', '500,000〜1,000,000'), ('1,000,000~5,000,000', '1,000,000〜5,000,000'),
    ('5,000,000', '5,000,000〜'))
    UPDATE_CHOICES = ((1, 'はい'), (0, 'いいえ'))

    url_list = forms.CharField(label="URL", max_length=100, widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': 'https://', 'pattern': 'https?://.+'}))
    data_details = forms.CharField(label="Data Details",
                                   widget=forms.Textarea(attrs={'class': '', 'placeholder': "", 'cols': 53, 'rows': 6}))
    due_date = forms.DateField(label="Due Date", widget=forms.DateInput(
        attrs={'class': 'form-control', 'placeholder': timezone.now().strftime("%Y/%m/%d"), 'id': 'datepicker'},
        format='%Y/%m/%d'), input_formats=('%Y/%m/%d',))
    purpose = forms.CharField(label="Acquisition Purpose",
                              widget=forms.Textarea(attrs={'class': '', 'placeholder': "", 'cols': 53, 'rows': 6}))
    company = forms.CharField(max_length=100,
                              widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': "会社名"}))
    department = forms.CharField(max_length=100,
                                 widget=forms.TextInput(attrs={'class': 'form-control', 'placeholder': "部署名"}))
    budget = forms.ChoiceField(label="", choices=MONEY_CHOICES, widget=forms.Select(attrs={'class': 'dropdown'}))
    email = forms.CharField(label="Contact Email", max_length=100, widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': "pigdata@example.com",
               'pattern': '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$'}))
    phone_number = forms.CharField(label="Phone Number", max_length=100, widget=forms.TextInput(
        attrs={'class': 'form-control', 'placeholder': "0312345678", 'pattern': '\d{2,4}\d{3,4}\d{3,4}'}))
    regular_order = forms.ChoiceField(widget=forms.RadioSelect, choices=UPDATE_CHOICES, required=False)

    class Meta:
        model = Data_Order
        fields = (
        'url_list', 'data_details', 'due_date', 'purpose', 'company', 'department', 'budget', 'email', 'phone_number',
        'regular_order')


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required()
def data_order(request: HttpRequest):
    form = DataOrderForm()
    return render(request, 'data_order.html', {'user': request.user, 'form': form})


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required()
def data_order_form(request: HttpRequest):
    if request.method == 'POST':
        if 'form' in request.POST:
            # after filling in
            form = DataOrderForm(request.POST)
            if form.is_valid():
                return render(request, 'data_order_confirm.html', {'form': form})
            else:
                return render(request, 'data_order_form.html', {'form': form})
        else:
            # before filling in
            url_list = request.POST.get("url_list")
            form = DataOrderForm(initial={'url_list': url_list, 'regular_order': 0})
            return render(request, 'data_order_form.html', {'user': request.user, 'form': form})

    return redirect('dashboard:data_order')


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required()
def data_order_confirm(request: HttpRequest):
    if request.method == 'POST':
        form = DataOrderForm(request.POST)
        # after sending final form
        if 'done' in request.POST:
            form = DataOrderForm(request.POST)
            if form.is_valid():
                result = form.save(commit=False)
                user = User.objects.get(pk=request.user.id)
                result.user = user
                result.created_at = datetime.now()
                result.save()

                """
                # email report
                subject = '【PigData】データオーダー完了のお知らせ（株式会社 SMSデータテック）'
                message = render_to_string('data_order_email.html', {'user': user, 'form': form})
                from_email = 'no-reply@sms-datatech.co.jp'
                user = User.objects.get(pk=request.user.id)
                send_mail(subject, message, from_email, [form.data['email'], "support@sms-datatech.zendesk.com"])
                """
                return render(request, 'data_order_done.html', {'form': form, 'email': result.email})
            else:
                return render(request, 'data_order_form.html', {'form': form})
        # after clicking back button
        elif 'back' in request.POST:
            return render(request, 'data_order_form.html', {'form': form})
        # error
        else:
            return render(request, 'system_error.html', {'back': '../data_order'})

    return redirect('dashboard:data_order')


@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required()
def data_order_done(request: HttpRequest):
    if request.method == 'POST':
        return render(request, 'data_order_done.html')

    return redirect('dashboard:data_order')


## PAYMENT


class ShowCartView(View):
    def get(self, request, *args, **kwargs):
        return render(request, 'shop/cart.html', {
            'paypal_mode': settings.PAYPAL_MODE,
        })


@login_required()
def ExecutePaymentView(req):
    # def form_valid(self, form):
    print('TokenID: ' + req.POST['payjpToken'] + '   Price: ' + req.POST['price'])
    # return super(Criar, self).form_valid(form)
    pay_token_id = req.POST['payjpToken']
    price = req.POST['price']

    # this is seacret key, so shis should be hide.
    payjp.api_key = 'sk_test_8ba665ee99743d9928387cc0'
    charge = payjp.Charge.create(
        amount=price,
        card=pay_token_id,
        currency='jpy',
    )

    is_success = charge.paid
    print('Paymnt is successed? [' + str(is_success) + ']')

    # email report
    subject = 'Payment Procedure Complete'
    message = "You finished registering payment."
    from_email = 'no-reply@sms-datatech.co.jp'
    user = User.objects.get(id=req.user.id)
    send_mail(subject, message, from_email, [user.email])

    # store the data (token)

    return redirect('dashboard:upgrade')


def password_change_finish(request: HttpRequest):
    return render(request, 'password_change_finish.html')


def payment_change_bank(request: HttpRequest):
    return render(request, 'payment_change_bank.html')


def payment_change_credit(request: HttpRequest):
    return render(request, 'payment_change_credit.html')


def payment_change_finish(request: HttpRequest):
    return render(request, 'payment_change_finish.html')


def product_download(request: HttpRequest):
    return render(request, 'product_download.html')


def profile_change_finish(request: HttpRequest):
    return render(request, 'profile_change_finish.html')


def system_error(request: HttpRequest):
    return render(request, 'system_error.html')


# cancel
def cancel(request: HttpRequest):
    if request.method == "POST":
        user = User.objects.get(pk=request.user.id)
        user.is_active = 0
        user.save()
        return render(request, 'cancel_finish.html')

    return render(request, 'cancel.html')


def cancel_finish(request: HttpRequest):
    return render(request, 'cancel_finish.html')
