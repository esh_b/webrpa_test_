from django.contrib.auth.decorators import login_required
#from api_v1.scheduling.models import Schedule
from api_v1.scraping.models import Project, Execution
from django.http.response import HttpResponse, JsonResponse, HttpResponseRedirect
from django.utils import timezone
from collections import namedtuple, OrderedDict
from django.views.static import serve
import xlwt, uuid
from api_v1.scraping.tasks import *
from django.contrib import messages
from django.contrib.auth import update_session_auth_hash
from django.contrib.auth.forms import PasswordChangeForm
from django.shortcuts import render, redirect
import shutil, zipfile
from django.db import connection
import logging
import xlrd
from django.contrib.auth.models import User
import pandas as pd
from django.core.mail import send_mail
import csv
from django.conf import settings
from django.contrib import messages
from django.http import JsonResponse, Http404
from django.shortcuts import render, reverse
from django.views.generic import View
import paypalrestsdk
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
import json, os, ast

SCRAPING_DATA_DIR = '/var/lib/pig-data/data/'
SCRAPING_PROJECT_DIR = '/var/lib/pig-data/projects/'
@login_required
def index(request: HttpRequest):
    # if not request.user.is_authenticated:
    #     raise HTTPException(403)
    # else:
    #     total_size = 0
    #     ts = 0
    #     start_path = os.path.join(settings.SCRAPING_DATA_DIR, str(request.user.id))
    #     for dirpath, dirnames, filenames in os.walk(start_path):
    #         for f in filenames:
    #             fp = os.path.join(dirpath, f)
    #             total_size += os.path.getsize(fp)
    #             if total_size > 1024*1024:
    #                 total_size = total_size/(1024*1024)
    #                 ts = str(total_size)
    #             else:
    #                 ts = '< 1'
    ts = 0
    totalprojects = Project.objects.filter(user=request.user).count()
    totalexecutions = Execution.objects.filter()
    return render (request, 'index.html', {'total_size': ts, 'user': request.user, 'totalprojects': totalprojects} )


@login_required
def project_data(request: HttpRequest):
    if not request.user.is_authenticated:
        raise HTTPException(403)
    else:
        all_proj = Project.objects.filter(user=request.user).order_by('-date_created')

        num_proj = Project.objects.filter(user=request.user).count()
        projects = OrderedDict()

        for project in Project.objects.filter(user=request.user).order_by('-date_created'):
            projects[project.id] = project
            project.executions = []

        for execution in Execution.objects.filter(project__in=projects).order_by('-date_executed'):
            project = projects[execution.project.id]
            project.executions.append(execution)

    return render(request, 'data1.html', {'user': request.user, 'num_proj': num_proj, 'projects': projects, 'all_projects': all_proj})


@login_required
def get_project(request: HttpRequest):
    pid = request.GET['pid']
    project = Project.objects.get(id=pid)

    executions = {
        "id": [],
        "date": [],
        "status": []
    }

    for execution in Execution.objects.filter(project=project).order_by('-date_executed'):
        executions["id"].append(execution.id)
        executions["date"].append(execution.date_executed)
        executions["status"].append(execution.status)

    return JsonResponse(executions)

"""
@login_required
def project_schedule(request: HttpRequest):
    project_obj = Project.objects.filter(user=request.user).order_by('-date_created')

    d = {}
    for obj in project_obj:
        sched = Schedule.objects.filter(project = Project.objects.get(id=obj.id))
        d[obj.id] = sched

    with connection.cursor() as cursor:
        cursor.execute("select * from scheduling_schedule right join scraping_project on scraping_project.id=scheduling_schedule.project_id where scraping_project.user_id=%s order by scraping_project.date_created desc", [request.user.id])
        project_obj = cursor.fetchall()

    return render(request, 'schedule.html', {'project_schedule': project_obj, 'sched': d })


@login_required
def schedule_form(request: HttpRequest):
    if request.method == "POST":
        st = request.POST.get("scheduleType")
        pid = request.POST.get("pid")

        scheduled = Schedule.objects.filter(project = Project.objects.get(id=pid)).update(schedule_type = st, last_date = timezone.now(), next_date = timezone.now())
        if not scheduled:
            p = Schedule(schedule_type = st, project = Project.objects.get(id=pid), last_date = timezone.now(), next_date=timezone.now())
            p.save()

    with connection.cursor() as cursor:
        cursor.execute("select * from scheduling_schedule right join scraping_project on scraping_project.id=scheduling_schedule.project_id where scraping_project.user_id=%s order by scraping_project.date_created desc", [request.user.id])
        project_obj = cursor.fetchall()

    return render(request, 'schedule.html', {'project_schedule': project_obj})
"""

@login_required()
@csrf_exempt
def product_type(request: HttpRequest):
    if not request.user.is_authenticated:
        raise HTTPException(403)
    else:
        filestr = request.POST['filetype']
        uid,pid,eid,ft = filestr.split("#")
        print(ft)
        if ft == '1':
            op = produce_excel(uid, pid, eid)
            print("OP is: ", op)
            return serve(request, os.path.basename(op), os.path.dirname(op))
        elif ft=='2':
            op = csv_from_excel(uid, pid, eid)
            return serve(request, os.path.basename(op), os.path.dirname(op))
        elif ft =='3':
            op = json_to_txt(uid, pid, eid)
            return serve(request, os.path.basename(op), os.path.dirname(op))
        else:
            print("type not recognized")
            return HttpResponseRedirect('/project/data/', [pid])
    return HttpResponseRedirect('/project/data/', [pid])


@login_required()
def run(request: HttpRequest, pid=None):
    try:
        #: :type project: Project

        project = Project.objects.get(
            id=pid, user=request.user)
    except Project.DoesNotExist:
        raise HTTPException(404)

    execution = Execution()
    execution.id = str(uuid.uuid4())
    execution.project = project
    execution.date_executed = datetime.now()
    execution.status = 0
    execution.save()

    res = task_scrape.delay(request.user.id, project.id, execution.id)

    #if res.status == 'SUCCESS':
    #    subject = 'PigData:Download Ready'
    #    message = 'Your extractor of project' + project.name + 'created on' + execution.date_executed + 'has been finished successfully.'
    # user.email_user(subject, message)
    #    send_mail(subject, message, 'no-reply@sms-datatech.co.jp', [request.user.email])

    return HttpResponseRedirect('/project/data/', [pid])


@login_required()
def upgrade(request: HttpRequest):
    return render(request, 'upgrade.html')

@login_required()
def settings(request: HttpRequest):
    return render(request, 'setting.html')


@login_required()
def project_details(request: HttpRequest, pid):
    return render(request, 'project_details.html')


@login_required()
def delete_account(request: HttpRequest):
    return render(request, 'project_details.html')


@login_required()
def notification_setting(request: HttpRequest):
    user = User.objects.get(pk=request.user.id)
    if user.profile.notification == True:
        status = 'Yes'
    else:
        status = 'No'

    return render(request, 'notifications.html', {'status': status})


@login_required()
def notification_change(request: HttpRequest):
    if request.method == "POST":
        notification_status = request.POST.get("email_notification")

    user = User.objects.get(pk=request.user.id)

    if notification_status == '1':
        user.profile.notification = True
        status = 'Yes'
        user.save()
        notification_message = 'Your mail notification settings has been updated.'
    elif notification_status == '0':
        user.profile.notification = False
        status = 'No'
        user.save()
        notification_message = 'Your mail notification settings has been updated. you will not receive anymore notifications.'
    else:
        notification_message = 'Please select an option.'

    return render(request, 'notifications.html', {'notification_status': notification_message, 'status': status})


@login_required()
def update_profile(request: HttpRequest):
    return render(request, 'notifications.html')


@login_required()
def change_password(request: HttpRequest):
    if request.method == 'POST':
        form = PasswordChangeForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()
            update_session_auth_hash(request, user)  # Important!
            messages.success(request, 'Your password was successfully updated!')
            return redirect('dashboard:change_password')
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        form = PasswordChangeForm(request.user)
    return render(request, 'change_password.html', {
        'form': form
    })


@login_required()
def zip_data(request: HttpRequest, pid):
    project = Project.objects.filter(id=pid)
    rows = Execution.objects.filter(project=project[0])

    directory = os.path.join(SCRAPING_DATA_DIR, str(request.user.id), str(pid), 'zip')
    opzip = os.path.join(directory, str(project[0].name) + '.zip')

    if not os.path.exists(directory):
        os.makedirs(directory)

    if not os.path.exists(opzip):
        for r in rows:
            filepath = os.path.join(SCRAPING_DATA_DIR, str(request.user.id), pid, str(r.id) + '.json')
            op = os.path.join(directory, str(r.id) + '.xls')
            data = json.loads(open(filepath).read())
            book = xlwt.Workbook()
            ws = book.add_sheet("PySheet1")
            column = 0
            for page in range(0, len(data)):
                page_data = data[page]

                for action in range(0, len(page_data)):
                    action_data_dict = page_data[action]

                    for key in action_data_dict:
                        action_data = action_data_dict[key]
                        print(action_data)
                        ws.write(0, column, key)

                    for i in range(0, len(action_data)):
                        ws.write(i + 1, column, action_data[i])

                column += 1

            book.save(op)

        shutil.move(shutil.make_archive(str(project[0].name), 'zip', directory), directory + '/')
        return serve(request, os.path.basename(opzip), os.path.dirname(opzip))
    else:
        return serve(request, os.path.basename(opzip), os.path.dirname(opzip))




@login_required()
def delete_project(request: HttpRequest, pid):
    project = Project.objects.filter(id=pid)
    print(project)

    rows = Execution.objects.filter(project=project[0])
    for r in rows:
        r.delete()

    Schedule.objects.filter(project=project[0]).delete()
    Project.objects.filter(id=pid).delete()

    dirname = str(request.user.id)
    filename = str(pid)
    shutil.rmtree(os.path.join(SCRAPING_DATA_DIR, dirname, filename))
    os.remove(os.path.join(SCRAPING_PROJECT_DIR, dirname, filename + '.json'))
    return redirect('dashboard:project_data')



#------------Payment-----------------------

class ShowCartView(View):
    def get(self, request, *args, **kwargs):
        return render(request, 'shop/cart.html', {
            'paypal_mode': settings.PAYPAL_MODE,
        })


class CreatePaymentView(View):
    def post(self, request, *args, **kwargs):
        paypalrestsdk.configure({
            'mode': settings.PAYPAL_MODE,
            'client_id': settings.PAYPAL_CLIENT_ID,
            'client_secret': settings.PAYPAL_CLIENT_SECRET,
        })

        print('settings.PAYPAL_MODE' + settings.PAYPAL_MODE)



        payment = paypalrestsdk.Payment({
            'intent': 'sale',

            # Payer
            'payer': {
                'payment_method': 'paypal',
            },

            # Redirect URLs
            'redirect_urls': {
                'return_url': request.build_absolute_uri(reverse('dashboard:execute-payment')),
                'cancel_url': request.build_absolute_uri(reverse('dashboard:cart')),
            },

            # Transaction
            # Note: This is dummy. If production, transaction should be created with reference to cart items.
            'transactions': [{
                # Item List
                'item_list': {
                    'items': [{
                        'name': 'item',
                        'sku': 'item',
                        'price': '5.00',
                        'currency': 'USD',
                        'quantity': 1,
                    }]
                },
                # Amount
                'amount': {
                    'total': '5.00',
                    'currency': 'USD',
                },
                'description': 'This is the payment transaction description.',
            }]
        })

        # Create Payment
        if payment.create():
            logger.info("Payment[{}] created successfully.".format(payment.id))
            return JsonResponse({'success': True, 'paymentId': payment.id})
        else:
            logger.error("Payment failed to create. {}".format(payment.error))
            return JsonResponse({'success': False, 'error': "Error occurred while creating your payment."}, status=500)


class ExecutePaymentView(View):
    def get(self, request, *args, **kwargs):
        # Query strings are always in request.GET
        payment_id = request.GET.get('paymentId', None)
        payer_id = request.GET.get('PayerID', None)

        try:
            payment = paypalrestsdk.Payment.find(payment_id)
        except paypalrestsdk.ResourceNotFound as err:
            logger.error("Payment[{}] was not found.".format(payment_id))
            return Http404

        # Execute Payment
        if payment.execute({'payer_id': payer_id}):
            logger.info("Payment[{}] executed successfully.".format(payment.id))
            messages.info(request, "Your payment has been completed successfully.")
            return render(request, 'shop/complete.html', {
                'payment': payment,
            })
        else:
            logger.error("Payment[{}] failed to execute.".format(payment.id))
            messages.error(request, "Error occurred while executing your payment.")
            return render(request, 'error.html')


def csv_from_excel(uid, pid, eid):
    excel_file = produce_excel(uid, pid, eid)
    data_xls = pd.read_excel(excel_file, 'PySheet1', index_col=None)
    csv_file = 'op.csv'
    data_xls.to_csv(csv_file, sep=',', encoding='utf-8', index=False)

    return csv_file

def json_to_txt(uid, pid, eid):
    project = Project.objects.get(id=pid)
    execution = Execution.objects.get(id=eid)

    filepath = os.path.join(SCRAPING_DATA_DIR, uid, pid, eid + '.json')
    op = os.path.join(SCRAPING_DATA_DIR, str(uid), str(pid), str(project.name) + '_' + str(execution.date_executed) + '.txt')
    data = json.loads(open(filepath).read())
    textFile = open(op, 'w')
    textFile.write(str(data))
    csv_file = 'op.csv'
    data_xls.to_string(csv_file, encoding='utf-8', index=False)
    # textFile.save(op)
    return op


def produce_excel(uid, pid, eid):
    print("-------PIGDATA-ITWEEK--------------")	
    project = Project.objects.get(id=pid)
    execution = Execution.objects.get(id=eid)

    file_path = os.path.join('/var/lib/pig-data/data/' + uid, pid, eid + '.json')
    op = os.path.join('/var/lib/pig-data/data/' + str(uid), str(pid), 'excel', eid + '.xls')
    print(file_path) 
    print(op)
    data = json.loads(open(file_path).read())
    print("----i m here----")
    try:
        if data[0] == 1 or data[0] == 2:
            data = data[1]
            print(data)
            book = xlwt.Workbook()
            ws = book.add_sheet("PySheet1")
            column = 0
            for page in range(0, len(data)):
                page_data = data[page]
                for action in range(0, len(page_data)):
                    action_data_dict = page_data[action]
                    for key in action_data_dict:
                        action_data = action_data_dict[key]
                        print(action_data)
                        ws.write(0, column, key)
                        if key == '':
                            for i in range(0, len(action_data)):
                                for j in range(0, len(action_data[i])):
                                    if action_data[i][j] != None:
                                        #                            print(action_data[i])
                                        ws.write(j + 1, column, action_data[i][j])
                                    else:
                                        ws.write(j + 1, column, 'None')
                                column += 1
                        else:
                            for i in range(0, len(action_data)):
                                ws.write(i + 1, column, action_data[i])
                    column += 1
                column += 1

        elif data[0] == 3:
            file = ToExcel()
            data = open(filepath).read()
            data = ast.literal_eval(data)
            data = data[1]
            book = xlwt.Workbook()
            ws = book.add_sheet("PySheet1")
            # ws = book.add_sheet("PySheet1", cell_overwrite_ok=True)
            ws = file.headline(ws, data, 0)
            ws = file.making(ws, data)
            book.save(op)

        elif data[0] == 4 or data[0] == 5:
            data = data[1]
            book = xlwt.Workbook()
            ws = book.add_sheet("PySheet1")
            row = 0
            product_row = 0
            for product in range(0, len(data[1])):
                page_list = data[1][product]
                name = data[0][product]
                product_url = data[2][product]
                for page in range(0, len(page_list)):
                    if product == 0 and page == 0:
                        [similar_flag, ws] = process(page_list[page], ws, 0)
                    else:
                        [similar_flag, ws] = process(page_list[page], ws, 1)
                    print('similar_flag:', similar_flag)
                    for action in range(0, len(page_list[page])):
                        for key in page_list[page][action]:
                            action_data = page_list[page][action][key]
                            if len(action_data) == 1:
                                for i in range(0, similar_flag):
                                    ws.write(i + row + product_row + 1, action + 2, action_data[0])
                            elif len(action_data) == 0:
                                for i in range(0, similar_flag):
                                    ws.write(i + row + product_row + 1, action + 2, '')
                            else:
                                for i in range(0, similar_flag):
                                    ws.write(i + row + product_row + 1, action + 2, action_data[i])

                    for i in range(0, similar_flag):
                        ws.write(i + row + product_row + 1, 1, product_url[page])
                    row += similar_flag
                for i in range(0, row):
                    ws.write(i + product_row + 1, 0, name)
                product_row = row
                row = 0


        book.save(op)
        print("OP is: ", op)

        return op
    except FileNotFoundError:
        print('File Not Found...!!')

def process(page, ws, flag):
    length = 1
    print(page)
    for action in range(0, len(page)):
        for key in page[action]:
            if flag == 0:
                ws.write(0, action + 2, key)
            if len(page[action][key]) > 1:
                length = len(page[action][key])

    return length, ws



class ToExcel():
    def __init__(self):
        self.row = 1
        self.column = 0
        self.arr = []

    def making(self, ws, data):
        if len(data) == 1:
            maxi = 1
            action_list = data[0]
            # print(action_list)
            for action in action_list:
                for key, value in action.items():
                    if maxi < len(value):
                        maxi = len(value)
            x = self.row
            y = self.column
            # print("Array is: ", arr)
            for k in range(0, maxi):
                for i in range(0, len(self.arr)):
                    # print(x, y-len(self.arr)+i, self.arr[i])
                    ws.write(x, y - len(self.arr) + i, self.arr[i])
                x += 1

            # y = self.column
            for action in action_list:
                x = self.row
                for key, value in action.items():
                    if len(value) == 1:
                        for k in range(0, maxi):
                            print(x, y, value[0])
                            ws.write(x, y, value[0])
                            x += 1
                    elif len(value) == 0:
                        for k in range(0, maxi):
                            print(x, y, [])
                            ws.write(x, y, [])
                            x += 1
                    else:
                        for val in value:
                            print(x, y, val)
                            ws.write(x, y, val)
                            x += 1
                y += 1
            self.row += maxi

        # print("Ending arr is: ", arr)
        else:

            dim = []
            for action in data[0]:
                for key in action:
                    dim.append(action[key])
            self.column += 2
            for i in range(0, len(data[1])):
                self.arr.append(dim[0][i])
                self.arr.append(dim[1][i])
                # print("Array is: ", self.arr)
                ws = self.making(ws, data[1][i])
                self.arr = self.arr[:-1]
                self.arr = self.arr[:-1]
            self.column -= 2
        # print("Ending Row, Column is: ", row, column)
        return ws

    def headline(self, ws, data, y):
        action_list = data[0]
        for action in action_list:
            for key, value in action.items():
                ws.write(0, y, key)
                y += 1
        if len(data) != 1:
            ws = self.headline(ws, data[1][0], y)
        return ws

