from django.dispatch import receiver
from django.db.models.signals import pre_save
from django.db.models.signals import post_save

from api_v1.scraping.models import Execution
from api_v1.scraping.models import Project
from django_celery_results.models import TaskResult

from django.utils import timezone

from celery.signals import task_prerun, task_postrun
import celery.states as celery_state

from api_v1.scraping.tasks import task_scrape

@task_prerun.connect
def pre_run(sender, task_id, task, *args, **kwargs):
    task.__task_starttime = timezone.now()
    return

@task_postrun.connect
def post_run(sender, task_id, task, retval, state,  *args, **kwargs):
    end_time = timezone.now()
    start_time = getattr(task, "__task_starttime", None)

    #check
    #print(isinstance(sender, type(task_scrape)))
    #if(sender == task_scrape):
    if(not(start_time is None)):
        if(sender.name == task_scrape.name):
            #task_scrape task
            try:
                exec_time = end_time - start_time
                task_project = Project.objects.get(id=kwargs['args'][1])
                task_result = TaskResult.objects.get(task_id=task_id)
                #task_state is 0 for every completed state (FAILED, REVOKED) other than success
                task_state = True if state == celery_state.SUCCESS else False
                
                res = Execution.objects.create(task=task_result, status=task_state, project=task_project, 
                                            date_executed=start_time, end_time=end_time, exec_time=exec_time)
            except Exception as e:
                print("task_postrun exception:", e)
                import traceback
                print(traceback.print_tb(e.__traceback__))
        else:
            #Other tasks
            pass
    else:
        print("task_starttime variable NOT FOUND!!! Could not write the task in Execution table!!!")
    return