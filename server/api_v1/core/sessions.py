import logging

from django.contrib import auth
from django.http.request import HttpRequest
from django.http.response import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt

from internal import load_json_request, HTTPException
from api_v1.scraping.models import Project

logger = logging.getLogger(__name__)


@csrf_exempt
def login(request: HttpRequest):
    try:
        credentials = load_json_request(request)

        user = auth.authenticate(request, **credentials)
        if user:
            auth.login(request, user)
            request.session['isClientTool'] = 1
            request.session['login'] = True
            result = True
        else:
            result = False

        return JsonResponse(result, safe=False)
    except HTTPException as ex:
        return ex.as_response()
    except Exception:
        logger.exception('Internal server error')
        return HttpResponse(status=500)


@csrf_exempt
def logout(request: HttpRequest):
    try:
        auth.logout(request)
        return JsonResponse(True, safe=False)
    except Exception:
        logger.exception('Internal server error')
        return HttpResponse(status=500)
