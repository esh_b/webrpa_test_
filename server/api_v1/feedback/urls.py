from django.urls.conf import path
from . import views


urlpatterns = [
    path('post_feedback', views.post_feedback, name='feedback'),
]
