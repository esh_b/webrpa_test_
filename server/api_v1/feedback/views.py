from django.db.transaction import atomic
from django.http.request import HttpRequest
from django.http.response import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from internal import load_json_request, HTTPException
from .models import Feedback
from datetime import datetime, timedelta


@csrf_exempt
@atomic
def post_feedback(request: HttpRequest):
    print('\n==========feedback=====\n')
    params = load_json_request(request)
    try:
        if 'feedback' not in params:
            # Bad Request
            raise HTTPException(400)

        feedback_data = params['feedback']
        print(feedback_data)
        try:
            new_feedback = Feedback(user=request.user, category=feedback_data.get('category'),
                                    comment=feedback_data.get('comment'), timestamp=datetime.now())
            new_feedback.save()
            status_code = 200
        except:
            status_code = 500

        return JsonResponse(status_code, safe=False)
    except HTTPException as ex:
        return ex.as_response()
    except Exception:
        logger.exception('Internal server error')
        return HttpResponse(status=500)
