# Generated by Django 2.0.1 on 2019-03-19 02:25

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='BehindLogin',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('salt', models.CharField(blank=True, max_length=256)),
            ],
        ),
        migrations.CreateModel(
            name='Execution',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_executed', models.DateTimeField()),
                ('end_time', models.DateTimeField()),
                ('exec_time', models.DurationField()),
                ('status', models.BooleanField()),
            ],
        ),
        migrations.CreateModel(
            name='Project',
            fields=[
                ('id', models.UUIDField(default=uuid.uuid4, primary_key=True, serialize=False)),
                ('name', models.CharField(max_length=256)),
                ('date_created', models.DateTimeField()),
                ('last_executed', models.DateTimeField(null=True)),
                ('status', models.IntegerField(null=True)),
                ('category', models.CharField(default='No Category', max_length=256, null=True)),
                ('start_url', models.CharField(default=None, max_length=1024, null=True)),
                ('project_size', models.FloatField(blank=True, default=None, null=True)),
                ('keyword', models.CharField(default=None, max_length=1024, null=True)),
                ('mail_alert', models.BooleanField(default=False)),
                ('data_alert', models.BooleanField(default=False)),
                ('keyword_alert', models.BooleanField(default=False)),
                ('periodic_task', models.ForeignKey(null=True, on_delete=django.db.models.deletion.PROTECT, to='django_celery_beat.PeriodicTask')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='CeleryTask',
            fields=[
            ],
            options={
                'proxy': True,
                'indexes': [],
            },
            bases=('django_celery_results.taskresult',),
        ),
        migrations.AddField(
            model_name='execution',
            name='project',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='scraping.Project'),
        ),
        migrations.AddField(
            model_name='execution',
            name='task',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='django_celery_results.TaskResult', to_field='task_id'),
        ),
        migrations.AddField(
            model_name='behindlogin',
            name='project',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to='scraping.Project'),
        ),
    ]
