from django.contrib import admin
from django_celery_results.models import TaskResult 
from .models import Project, CeleryTask
class ProjectAdmin(admin.ModelAdmin):
    list_display = ('user_id','full_name','user_name','name', 'date_created', 'last_executed', 'status','get_url')
    list_filter = ('last_executed', 'date_created')
    ordering = ('user_id',)
    actions = ["export_as_csv"]

    def export_as_csv(self, request, queryset):
        import csv
        from django.http import HttpResponse
        from io import StringIO

        f = StringIO()
        writer = csv.writer(f)
        writer.writerow(['full_name','user_name','name', 'date_created', 'last_executed', 'status','get_url'])

        for s in queryset:
            writer.writerow([s.full_name(), s.user_name(), s.name, s.date_created, s.last_executed, s.status, s.get_url()])

        f.seek(0)
        response = HttpResponse(f, content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename=project-info.csv'
        return response  


    export_as_csv.short_description = "Export data"
admin.site.register(Project, ProjectAdmin)

class CeleryTaskAdmin(admin.ModelAdmin):
    """Admin-interface for results of tasks."""

    model = CeleryTask
    list_display = ('task_id','get_user_name', 'get_user_email', 'get_project_name','date_done', 'status','traceback','get_start_url')
    readonly_fields = ('date_done', 'result', 'hidden', 'meta')
    fieldsets = (
        (None, {
            'fields': (
                'task_id',
                'status',
                'content_type',
                'content_encoding',
            ),
            'classes': ('extrapretty', 'wide')
        }),
        ('Result', {
            'fields': (
                'result',
                'date_done',
                'traceback',
                'hidden',
                'meta',
            ),
            'classes': ('extrapretty', 'wide')
        }),
    )

admin.site.register(CeleryTask, CeleryTaskAdmin)
