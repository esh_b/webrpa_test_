from django.urls.conf import path
from django.conf.urls import url
from . import views


urlpatterns = [
    path('save_project', views.save_project),
    path('get_project', views.get_project),
    path('save_result', views.save_result),
    path('save_salt', views.save_salt),
    path('get_public_key', views.get_public_key),
]
