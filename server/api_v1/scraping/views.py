import uuid
from os import path
from .tasks import *
from .models import Project, BehindLogin
from api_v1.scraping.models import Execution
from django.db.transaction import atomic
from django.http.request import HttpRequest
from django.http.response import HttpResponse, JsonResponse
from django.views.decorators.csrf import csrf_exempt
from django.utils.crypto import get_random_string
from datetime import datetime, timedelta
from django.db.models import Sum
from accounts.models import Profile
from django.utils import timezone

from schedule.views import update_or_create_schedule

logger = logging.getLogger(__name__)

salt = get_random_string(length=32)

@csrf_exempt
@atomic
def save_salt(request: HttpRequest):
	try:
		params = load_json_request(request)
		print('inside behind login')
		if 'project_id' not in params:
			# Bad Request
			raise HTTPException(400)

		try:
			#: :type project: Project
			project = Project.objects.get(
				id=params['project_id'], user=request.user)
		except Project.DoesNotExist:
			raise HTTPException(404)

		salt = params['salt']
		behind = BehindLogin()
		behind.project = project
		behind.salt = salt
		behind.save()

		return JsonResponse(behind.salt, safe=False)
	except HTTPException as ex:
		return ex.as_response()
	except Exception:
		logger.exception('Internal server error')
		return HttpResponse(status=500)

@csrf_exempt
def get_public_key(request: HttpRequest):
    try:
        if not request.user.is_authenticated:
            # Forbidden
            raise HTTPException(403)

        try:
            #: :type project: Project
            profile = Profile.objects.get(
                user_id=request.user.id)
        except Profile.DoesNotExist:
            print('====Profile not Found====\n')
            raise HTTPException(404)
        return JsonResponse(profile.public_key, safe=False)
    except HTTPException as ex:
        return ex.as_response()
    except Exception:
        logger.exception('Internal server error')
        return HttpResponse(status=500)


@csrf_exempt
@atomic
def save_project(request: HttpRequest):
    try:
        if not request.user.is_authenticated:
            # Forbidden
            raise HTTPException(403)

        params = load_json_request(request)
        if 'project' not in params:
            # Bad Request
            raise HTTPException(400)

        project_data = params['project']
        alert_type = params['alert_type']

        #: :type project: Project
        project = Project()
        project.id = str(uuid.uuid4())
        project.user = request.user
        project.name = project_data.get('project_name', project.id)
        project.date_created = datetime.now()
        project.start_url = project_data.get('start_url', project.id)
        project.mail_alert = alert_type[0]
        project.data_alert = alert_type[1]
        project.keyword_alert = alert_type[2]
        project.keyword = params['keyword']

        # create project directory
        project_path = path.join(settings.SCRAPING_PROJECT_DIR,
                                 str(request.user.id))
        print(project_path)
        os.makedirs(project_path, exist_ok=True)

        # create project file
        project_path = path.join(project_path, project.id + '.json')
        with open(project_path, 'w', encoding='utf-8') as fp:
            json.dump(project_data, fp, ensure_ascii=False)

        # create data directory
        data_path = path.join(settings.SCRAPING_DATA_DIR,
                              str(request.user.id), project.id)
        os.makedirs(data_path, exist_ok=True)

        print("--------\nproject id before save:" + project.id + '\n')

        project.save()

        
        """
        # save project scheduling setting
        sched_type = params['sched_type']
        sched_val = int(params['sched_val'])
        now = datetime.now()
        if sched_type == "1":
            ndate = now + timedelta(days=+sched_val)
        elif sched_type== "2":
            week_diff = sched_val - now.weekday() #0(Mon):new - 1(Tue):now => -1
            if week_diff >= 0: #4(Fri):new - 2(Wed):now => 2
                ndate = now + timedelta(days=+week_diff)
                sched_val = week_diff
            else: #2(Wed):now - 4(Fri):new => -2
                ndate = now + timedelta(days=+(week_diff+7))
                sched_val = week_diff + 7
        elif sched_type== "3":
            if sched_val >= now.day:  # ex) 3th(now.day), 10th(custom)
                ndate = now + now.replace(month=now.month, day=sched_val).date()
            elif now.month == 12:
                ndate = now + now.replace(month=1, day=sched_val).date()
            else:  # ex) 3th(now.day), 1th(custom)
                ndate = now + now.replace(month=now.month + 1, day=sched_val).date()
        else:
            ndate = None

        scheduled = Schedule.objects.filter(project=Project.objects.get(id=project.id)).update(schedule_type=sched_type,
                                                                                        last_date=now, next_date=ndate,
                                                                                        custom=sched_val)
        if not scheduled:
            schedule = Schedule(schedule_type=sched_type, project=Project.objects.get(id=project.id), last_date=now,
                                next_date=ndate, custom=sched_val)
            schedule.save()

        # start project execution
        execution = Execution()
        execution.id = str(uuid.uuid4())
        execution.project = project
        execution.date_executed = datetime.now()
        execution.status = 0
        execution.error = 0
        print("--------\nproject id after save and before starting task scrape: " + project.id + '\n')
        res = task_scrape.delay(request.user.id, project.id, execution.id)
        execution.celery_id = res.task_id
        execution.save()
        task_update_status.delay(res.task_id, execution.id)
        return JsonResponse(project.id, safe=False)
        """
    except HTTPException as ex:
        return ex.as_response()
    except Exception:
        logger.exception('Internal server error')
        return HttpResponse(status=500)

@csrf_exempt
@atomic
def saas_save_project(**kwargs: dict):
    """Summary
    
    Args:
        **kwargs: Description
    
    Returns:
        TYPE: Description
    """

    #Validate the required params for the project
    try:
        project_user = kwargs['usr']
        project_start_url = kwargs['procedure_file']['start_url']
        project_name = kwargs['procedure_file']['project_name']
        project_keyword = kwargs['keyword']
        project_mail_alert, project_data_alert, project_keyword_alert = kwargs['alert_type']
        procedure_file = kwargs['procedure_file']
    except Exception as e:
        print("EXCEPTION:", e)
        return HttpResponse(status=500)

    #setup the project details in db and also create appropriate project and data dirs
    try:
        project = Project(
            user=project_user,
            name=project_name,
            date_created=timezone.now(),
            start_url=project_start_url,
            mail_alert=project_mail_alert,
            data_alert=project_data_alert,
            keyword_alert=project_keyword_alert,
            keyword=project_keyword
        )

        project_path = path.join(settings.SCRAPING_PROJECT_DIR, str(project_user.id))
        os.makedirs(project_path, exist_ok=True)

        project_path = path.join(project_path, str(project.id) + '.json')
        with open(project_path, 'w', encoding='utf-8') as fp:
            json.dump(procedure_file, fp, ensure_ascii=False)

        data_path = path.join(settings.SCRAPING_DATA_DIR, str(project_user.id), str(project.id))
        os.makedirs(data_path, exist_ok=True)

        project.save()
    except Exception as e:
        print("EXCEPTION:", e)
        return HttpResponse(status=500)

    #Create the scheduling task for the project (if any)
    try:
        """
        time_to_run = timezone.now() + timedelta(minutes=2)
        update_or_create_schedule(usr.id, project.id, kwargs['sched_type'], kwargs['sched_val'], time_to_run, hour, minute)
        """
        pass
    except Exception as e:
        print("EXCEPTION:", e)
        return HttpResponse(status=500)

    #Run the project
    res = task_scrape.delay(project_user.id, str(project.id))
    return JsonResponse(project.id, safe=False)

@csrf_exempt
def get_project(request: HttpRequest):
	try:

		if not request.user.is_authenticated:
			# Forbidden
			raise HTTPException(403)

		params = load_json_request(request)
		if 'project_id' not in params:
			# Bad Request
			raise HTTPException(400)

		try:
			#: :type project: Project
			project = Project.objects.get(
				id=params['project_id'])
		except Project.DoesNotExist:
			print('====Project not Found====\n')
			raise HTTPException(404)

		# load project file
		project_path = path.join(settings.SCRAPING_PROJECT_DIR,
								 str(project.user.id),
								 project.id + '.json')
		with open(project_path, 'r', encoding='utf-8') as fp:
			project = json.load(fp)

		return JsonResponse(project)
	except HTTPException as ex:
		return ex.as_response()
	except Exception:
		logger.exception('Internal server error')
		return HttpResponse(status=500)


@csrf_exempt
@atomic
def save_result(request: HttpRequest):
	try:

		params = load_json_request(request)

		project = Project.objects.get(id=params['project_id'])

		execution = Execution()
		execution.id = str(uuid.uuid4())
		execution.project = project
		execution.date_executed = datetime.now()

		# TODO: Define status constants
		if 'error' in params and params['error']:
			execution.status = 2
			execution.error = params['error']
		else:
			execution.status = 1

		execution.save()

		data_path = path.join(
			settings.SCRAPING_DATA_DIR, str(project.user.id), project.id)
		os.makedirs(data_path, exist_ok=True)

		if 'output' in params and params['output']:
			data_path = path.join(data_path, execution.id + '.json')
			with open(data_path, 'w') as fp:
				json.dump(params['output'], fp)

		return JsonResponse(execution.id, safe=False)
	except HTTPException as ex:
		return ex.as_response()
	except Exception:
		logger.exception('Internal server error')
		return HttpResponse(status=500)


@csrf_exempt
def user_type(request: HttpRequest):
    try:
        #user_subscription = UserSubscription.objects.get(user=request.user)
        user_subscription = '1'
        return JsonResponse(user_subscription, safe=False)
        #return JsonResponse(user_subscription.plan_id, safe=False)
    except Exception:
        logger.exception('Internal server error')
        return HttpResponse(status=500)


@csrf_exempt
def get_version(request: HttpRequest):
    try:
        version = '1.0.3'
        return JsonResponse(version, safe=False)
    except Exception:
        logger.exception('Internal server error')
        return HttpResponse(status=500)


@csrf_exempt
def project_created(request: HttpRequest):
    try:
        total_projects = Project.objects.filter(user=request.user).count()
        return JsonResponse(total_projects, safe=False)
    except Exception:
        logger.exception('Internal server error')
        return HttpResponse(status=500)


@csrf_exempt
def user_storage(request: HttpRequest):
    try:
        storage = Project.objects.filter(user=request.user).aggregate(Sum('project_size'))['project_size__sum']
        if storage is None:
            return JsonResponse('0', safe=False)
        else:
            return JsonResponse(storage, safe=False)
    except Exception:
        logger.exception('Internal server error')
        return HttpResponse(status=500)
