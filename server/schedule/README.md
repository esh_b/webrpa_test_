# Schedule module
- The `schedule` module takes care of scheduling the tasks which have to be run at specified period of time (periodic tasks).
- Once scheduled, these scheduled tasks are then triggered by the `celery beat` program at their respective times and then sent to the celery workers for execution.

### Types of task scheduling
- **TYPE 1:** Schedule a task to **run at specified time every `n` days**.
	- E.g. *Run a task every 2 days at 10:00 AM localtime*: This will run the task every 2 days at the specified time (user localtime) from the scheduled day.
- **TYPE 2:** Schedule a task to **run once at a specified day and time in a week**.
	- E.g. *Run a task on Sunday at 10:00 AM localtime*: The task will be run every Sunday at 10:00 AM (user localtime).
- **TYPE 3:** Schedule a task to **run once at a specified date and time in a month**.
	- E.g. *Run a task on the 5th day of every month at 10:00 AM localtime*: The task will be run on the 5th of every month at 10:00 AM (user localtime).

### Background and assumptions
- The schedule module assumes that the `django_celery_beat` module is installed and `celery beat` uses the `DatabaseScheduler` scheduler.
- The reason for `celery beat` to use `DatabaseScheduler` is because the schedule module adds the schedule for various tasks **in the database** (in the `django_celery_beat` tables).
- **Schedule classes in django_celery_beat:** `django_celery_beat` module has `CrontabSchedule` and `IntervalSchedule` classes of schedules. The former class can schedule any cron type of schedule while the latter can schedule tasks which has to run at fixed intervals (like every 2 days). 
- **Issue with IntervalSchedule class:** While the time to run (hour and minute) can be specified in `CrontabSchedule` class, the `IntervalSchedule` has a limitation in that the date or time to run the task cannot be specified (by default, the task runs *every n (secs, mins, days etc.)* from the scheduler's start time or the scheduled time if the scheduler is already running when the user is scheduling the task).

### Managing the periodic tasks for the `task_scrape` task
- The `update_or_create_schedule` function in the schedule module creates a new schedule (or) updates the existing schedule for a given user project.

#### Scheduling - Approach 1
- For type 1 schedules, the `IntervalSchedule` class is created (which runs the task every 2 days) and the `last_run_at` field of the class is set to a datetime object (say x) which is basically `now` datetime but with its hour and minute fields replaced by the scheduled time's hour and minute. This makes sure that the next_run will be exactly n days after the `last_run_at` datetime.
- For type 2 and type 3 schedules, first, the scheduled time (in user localtime) is converted to UTC time before the cron pattern is generated (because `CELERY_TIMEZONE` is set to `UTC`). This means we have to convert the user localtime schedule to UTC time and then generate the CRON pattern for that UTC time. This is done by finding the matching `last_run_at` time for the given type (in user localtime) and then converting that `last_run_at` datetime in UTC and then generating the cron pattern from that UTC format. Even here, the `last_run_at` field of the `CrontabSchedule` class is updated.