"""Summary
"""
import pytz

from django.test import TestCase
from django.utils import timezone

from datetime import datetime, timedelta
from celery.utils.time import ffwd

from model_mommy import mommy

from api_v1.scraping.models import Project

from .views import get_next_run_cron
from .views import get_curdate_given_time
from .views import update_or_create_schedule

class TestSchedule(TestCase):

    """Class for testing `schedule` module
    """
    
    @classmethod
    def setUpClass(cls):
        """Summary
        """
        super(TestSchedule, cls).setUpClass()

        cls.project = mommy.make(Project)
        assert type(cls.project) == Project

        cls.eastern_tz = pytz.timezone("US/Eastern")
        cls.hongkong_tz = pytz.timezone("Asia/Hong_Kong")
        cls.tokyo_tz = pytz.timezone("Asia/Tokyo")
        assert str(cls.eastern_tz) == "US/Eastern"
        assert str(cls.hongkong_tz) == "Asia/Hong_Kong"
        assert str(cls.tokyo_tz) == "Asia/Tokyo"

    def test_get_curdate_given_time(self):
        """`get_curdate_given_time` function test (Used in Interval schedule)
        """
        hour, minute = 10, 30
        today_run = get_curdate_given_time(hour=hour, minute=minute)
        self.assertIsInstance(today_run, datetime)
        self.assertEqual(today_run.date(), timezone.localtime().date())
        self.assertEqual(today_run.hour, hour)
        self.assertEqual(today_run.minute, minute)

    def test_update_or_create_schedule_generic_cases(self):
        """Test generic cases in `update_or_create_schedule` function
        """
        self.assertTrue(update_or_create_schedule(self.project.user.id, str(self.project.id), '2', '1', "10:30"))
        self.assertFalse(update_or_create_schedule(self.project.user.id, str(self.project.id), '4', '1', "10:30"))
        """
        self.assertRaises(ValueError, 
            update_or_create_schedule(self.project.user.id, str(self.project.id), '2', 'abc', "10:30"))
        #self.assertRaises(ValueError, update_or_create_schedule(self.project.user.id, str(self.project.id), '2', '1', "1030"))
        """

    def test_no_schedule(self):
        """No schedule test
        """
        self.assertTrue(update_or_create_schedule(self.project.user.id, str(self.project.id), '0', None, None))
        self.project.refresh_from_db()
        self.assertIs(self.project.periodic_task, None)

    def test_interval_schedule(self):
        """Interval schedule test
        """
        # Check return value is True (implies scheduled successfully)
        self.assertTrue(update_or_create_schedule(self.project.user.id, str(self.project.id), '1', '2', "10:30"))
        self.project.refresh_from_db()

        # Check the periodic task is related to the project
        time_now = timezone.localtime()
        periodic = self.project.periodic_task
        self.assertIsNot(periodic, None)
        self.assertIsNot(periodic.interval, None)
        self.assertIsNot(periodic.crontab, None)
        
        # Check next scheduled time (by celery beat) is correct
        scheduled_next_run = time_now + periodic.interval.schedule.remaining_estimate(periodic.last_run_at)
        actual_next_run = time_now.replace(hour=10, minute=30, second=0, microsecond=0) + timedelta(days=2)
        self.assertGreaterEqual(scheduled_next_run, time_now)
        self.assertGreaterEqual(actual_next_run, time_now)
        self.assertAlmostEqual(scheduled_next_run, actual_next_run, delta=timedelta(seconds=1))

    def test_week_schedule(self):
        """Week schedule test
        """
        self.assertTrue(update_or_create_schedule(self.project.user.id, str(self.project.id), '2', '2', "10:30"))
        self.project.refresh_from_db()

        time_now = timezone.localtime()
        periodic = self.project.periodic_task
        self.assertIsNot(periodic, None)
        self.assertIsNot(periodic.crontab, None)

        scheduled_next_run = time_now + periodic.crontab.schedule.remaining_estimate(time_now)
        actual_next_run = get_next_run_cron(periodic.crontab)
        self.assertGreaterEqual(scheduled_next_run, time_now)
        self.assertGreaterEqual(actual_next_run, time_now)
        self.assertAlmostEqual(scheduled_next_run, actual_next_run, delta=timedelta(seconds=1))

    def test_month_schedule(self):
        """Month schedule test
        """
        self.assertTrue(update_or_create_schedule(self.project.user.id, str(self.project.id), '3', '2', "10:30"))
        self.project.refresh_from_db()

        time_now = timezone.localtime()
        periodic = self.project.periodic_task
        self.assertIsNot(periodic, None)
        self.assertIsNot(periodic.crontab, None)

        scheduled_next_run = time_now + periodic.crontab.schedule.remaining_estimate(time_now)
        actual_next_run = get_next_run_cron(periodic.crontab)
        self.assertGreaterEqual(scheduled_next_run, time_now)
        self.assertGreaterEqual(actual_next_run, time_now)
        self.assertAlmostEqual(scheduled_next_run, actual_next_run, delta=timedelta(seconds=1))