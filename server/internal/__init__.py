import json

from django.http.request import HttpRequest
from django.http.response import HttpResponse


class HTTPException(Exception):
    def __init__(self, status):
        self.status = status

    def as_response(self):
        return HttpResponse(status=self.status)


def load_json_request(request: HttpRequest):
    if request.method != 'POST':
        # Method Not Allowed
        raise HTTPException(405)

    if request.content_type != 'application/json':
        # Unsupported Media Type
        raise HTTPException(415)

    try:
        return json.loads(request.body.decode())
    except ValueError:
        raise HTTPException(400)
    except Exception:
        raise HTTPException(500)
