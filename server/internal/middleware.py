from django.conf import settings
from django.utils.deprecation import MiddlewareMixin


class FakeSessionMiddleware(MiddlewareMixin):
    def process_request(self, request):
        if settings.SESSION_COOKIE_NAME not in request.COOKIES \
                and settings.SESSION_COOKIE_NAME in request.GET:
            request.COOKIES[settings.SESSION_COOKIE_NAME] = \
                request.GET[settings.SESSION_COOKIE_NAME]

    def process_response(self, request, response):
        if settings.SESSION_COOKIE_NAME in request.GET:
            response.set_cookie(settings.SESSION_COOKIE_NAME,
                                request.GET[settings.SESSION_COOKIE_NAME])

        return response
