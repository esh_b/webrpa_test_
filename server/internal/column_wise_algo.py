import re
import requests
import xlwt
from bs4 import BeautifulSoup as B
from .table_list_func import child_tags, immediate_tags, get_tag, row_function, description_list_data, unordered_list_data

def extract(wiki, dompath, col_list):
    r = requests.get(wiki, headers = {'user-agent': 'Mozila/5.0'})
    url = r.text
    
    soup = B(url, 'html.parser')

    words = dompath.split()
    tags = []

    for i in range(len(words)):
        temp = re.split(r'[^\w]', words[i])
        tags.append(temp[0])

    # html_tag = soup.html
    # wb = xlwt.Workbook()
    # ws = wb.add_sheet('sheet_name')
    pagination = 0
    page_code = []
    anchor_tags = soup.findAll('a')
    for i in range(0, len(anchor_tags)):
        page_var = anchor_tags[i].string
        if page_var is not None:
            if 'next' in page_var:
                pagination = 1
                print("Pagination")
                page_code = anchor_tags[i]
                break

    table_index = tags.index('table')
    print('table_index', table_index)

    temp = words[table_index]
    c = 0
    tag_class = ''
    for i in range(0, len(temp)):
        if temp[i] == '.':
            c += 1
        elif c == 1:
            tag_class += temp[i]
        elif c == 2:
            break
    
    print('tag_name', tag_class)
    
    req_tag_name = tags[table_index]
    req_tag = soup.findAll(req_tag_name, {"class": tag_class})
    print(req_tag)
#    req_tag = [soup.body]

    tag = []
    c = 0
    specific_type = [0, 0, 0, 0]
    # creating a variable that has the list of list html tag code
    list_bs = []
    # creating a variable that has the list of table html tag code
    table_bs = []

    if len(req_tag) != 0:
        if get_tag(req_tag[0]) == 'table':
            specific_type[2] = 1
            specific_type[3] += 1
            table_bs.append(req_tag[0])
        elif get_tag(req_tag[0]) == 'ul' or get_tag(req_tag[0]) == 'ol' or get_tag(req_tag[0]) == 'dl':
            specific_type[0] = 0
            specific_type[1] += 1
            list_bs.append(req_tag[0])
        else:
            for child in req_tag[0].descendants:
                b = str(child)
                if '<' == b[0] and '!' != b[1]:
                    tag.append(child)
                    tag_name = get_tag(tag[c])
                    if tag_name == 'ul' or tag_name == 'ol' or tag_name == 'dl':
                        specific_type[0] = 0
                        specific_type[1] += 1
                        list_bs.append(child)
                    elif tag_name == 'table':
                        specific_type[2] = 1
                        specific_type[3] += 1
                        table_bs.append(child)
                    c = c + 1

    # wb = xlwt.Workbook()
    # ws = wb.add_sheet(sheet_name)
    # file_name = "random.xls"
    # file_name = sheet_name + '.xls'

    style_string = "font: bold on; borders: bottom dashed"
    style = xlwt.easyxf(style_string)

    # def sheet_save(name,data):
    #   for i in range(0,len(name)):
    #       # ws.write(i, 0, name[i])
    #       # ws.write(i, 1, data[i])
    #   wb.save(file_name)

    # def sheet_status(file_name):
    #   book = xlrd.open_workbook(file_name)
    #   sheet = book.sheet_by_index(0)
    #   count = 0
    #   for row in range(sheet.nrows):
    #       for column in sheet.row_values(row):
    #           if column.strip() != '':
    #               count += 1
    #               break;
    #   return count

    col = 0
    if specific_type[1] > 0:
        for i in range(0, specific_type[1]):
            list_code = list_bs[i]
            if get_tag(list_code) == 'dl':
                [name, data] = description_list_data(list_code)
                # sheet_save(name,data)
                col = col + len(name) + 1
            elif get_tag(list_code) == 'ul':
                data = unordered_list_data(list_code)
                # for j in range(0,len(data)):
                # ws.write(j+col, 0, data[j])
#                wb.save(file_name)
                col = col + len(data) + 1
            elif get_tag(list_code) == 'ol':
                data = unordered_list_data(list_code)
                # for j in range(0,len(data)):
                #   ws.write(j+col, 0, data[j])
#                wb.save(file_name)
                col = col + len(data) + 1

    def conventional_table_data(table_tag, col_list):
        table_contents = immediate_tags(table_tag.contents)
        table_data = []
        ind_list = []
        for i in range(0, len(table_contents)):
            temp = child_tags(table_contents[i].contents)
            temp1 = immediate_tags(table_contents[i].contents)
            try:
                temp.index('tr')
            except ValueError:
                print("no data")
            else:
                index = [
                    index for index,
                    value in enumerate(temp) if value == 'tr']
                for i in range(0, len(index)):
                    [data, ind] = row_function(temp1[index[i]])
                    data = [x[1] for x in enumerate(data) if x[0] in col_list]
                    ind = [x[1] for x in enumerate(ind) if x[0] in col_list]
                    print('reqda', data)
                    table_data.append(data)
                    ind_list.append(ind)
#                    if ind ==0:
#                        for j in range(0,len(data)):
#                            ws.write(ind, j, data[j])
#                    else:
#                        for j in range(0,len(data)):
#                            ws.write(i+1, j, data[j])
        return table_data, ind_list

    def unconventional_table_data(table_tag, col):
        table_data = []
        table_contents = child_tags(table_tag.contents)
        index = [index for index, value in enumerate(
            table_contents) if value == 'tr']
        temp1 = immediate_tags(table_tag.contents)
        print(col)
        for i in range(0, len(index)):
            [data, ind] = row_function(temp1[index[i]])
            data = [x[1] for x in enumerate(data) if x[0] in col_list]
            print('reqdaunc', data)
            table_data.append(data)
        col = col + len(index) + 1

        return table_data

    data = []
    ind_data = []
    if specific_type[3] > 0:
        for i in range(0, specific_type[3]):
            table_tag = table_bs[i]
            table_contents = child_tags(table_tag.contents)

            if 'tr' in table_contents:
                print("later")
                data = unconventional_table_data(table_tag, col)
#                wb.save(file_name)

            else:
                [data, ind_data] = conventional_table_data(table_tag, col_list)
                # if pagination == 1:
                #   page_url = page_code.get('href')
                #   parsed_uri = urlparse(wiki)
                #   domain = '{uri.scheme}://{uri.netloc}/'.format(uri=parsed_uri)
                #   domain = domain[:-1]
                #   Href = domain
                #   Href += page_url
                #   print(Href)
                #   [data1,ind_data1,wb] = extract(Href,sheet_name,path_data,wb)
                #   for k in range(0,len(data1)):
                #       if k>0:
                #           data.append(data1[k])
                #           ind_data.append(ind_data1[k])
                ind = 0
                # for a in data:
                #   for b in range(0,len(a)):
                #       if ind_data[ind][b]==0:
                #           ws.write(col+ind, b, a[b], style=style)
                #       else:
                #           ws.write(col+ind, b, a[b])
                #   ind+=1
                col = col + ind + 1
#                wb.save(file_name)
    return data
