class PigDataException(Exception):
    def __init__(self, message: str, **kwargs):
        """
        Constructs a :class:`PigDataException <PigDataException>` with a massage
        and some extra information.

        :param message: The message.
        """

        self.message = message
        self.extra = kwargs

