from urllib.parse import urljoin

import requests


class Session:
    """
    """

    def __init__(self, base_url):
        """
        Constructs a :class:`Session <Session>`.

        :param base_url: URL of the server.
        """

        self.__base_url = urljoin(base_url, '/api/v1/')
        self.__session = requests.Session()

    def __del__(self):
        try:
            self.logout()
        except Exception:
            # ignore exception
            pass

    def _invoke_method(self, name, **kwargs):
        """
        Constructs and sends POST request to the server.
        This method is not intended for uses from outside the library.

        :param name: Name of the method.
        :return: The object the server returned.
        """

        url = urljoin(self.__base_url, name)
        response = self.__session.post(url, json=kwargs)
        response.raise_for_status()

        return response.json()

    def login(self, username, password):
        """
        Logs in to the server with specified user name and password.

        :param username: The user name.
        :param password: The password.
        :return: True if succeeded, False otherwise.
        :rtype: bool
        """

        return self._invoke_method(
            'core/login', username=username, password=password)

    def logout(self):
        """
        Logs out from the server.

        :return: True if succeeded, False otherwise.
        :rtype: bool
        """

        return self._invoke_method('core/logout')

    @property
    def session_id(self):
        """
        Gets the current session id.

        :return: Session id string if any or null.
        :rtype: str
        """

        return self.__session.cookies.get('sessionid')

    @property
    def get_version(self):
        """
        Gets the pigdata released version.
        :return: Version number
        :rtype: str
        """
        return self._invoke_method('core/get_version')

    @property
    def user_type(self):
        """
        Gets the user plan type.
        :return: user subscription plan
        :rtype: int
        """
        return self._invoke_method('core/user_type')

    def project_created(self):
        """
        Number of project created by a user.

        :return: number of created project.
        :rtype: int
        """
        return self._invoke_method('core/project_created')
