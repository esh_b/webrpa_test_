from datetime import date, datetime

from scripts.rpalib.core import Session


def get_upcoming_projects(session: Session, target: date=None):
    """
    Gets the list of project ids that are scheduled on the specified date.
    Only superusers can get list of projects.

    :param session: The session.
    :param target: (Optional) The scheduled date if specified. Defaults to None meaning `today`.
    :return: List of project ids.
    :rtype: list
    """

    params = {}

    if target:
        if not isinstance(target, (date, datetime)):
            raise ValueError()

        if isinstance(target, datetime):
            target = target.date()

        params['date'] = target.isoformat()

    return session._invoke_method('scheduling/get_upcoming_projects', **params)
