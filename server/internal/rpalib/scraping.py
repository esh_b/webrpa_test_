from . import internal
from .core import Session


class Project():
    def __init__(self, start_url, project_name):
        self.start_url = start_url
        self.project_name = project_name
        self.project_type = -1  # 1:single 2:table 3:multilevel 4:urllist 5:productlist
        self.product_url = {}
        self.page_list = []
        self.login_details = []

    @staticmethod
    def from_dict(data):
        result = Project(None, None)
        result.__dict__ = data

        page_list = []
        for page in data['page_list']:
            page_list.append(PageInfo.from_dict(page))
        result.page_list = page_list

        login_details = []
        if 'login_details' in data:
            for login in data['login_details']:
                login_details.append(LoginInfo.from_dict(login))
            result.login_details = login_details
        else:
            result.login_details = []
        return result


class LoginInfo():
    def __init__(self):
        self.login_url = ""
        self.action_list = []

    @staticmethod
    def from_dict(data):
        result = LoginInfo()
        result.__dict__ = data

        action_list = []
        for action in data['action_list']:
            action_list.append(ActionInfo.from_dict(action))
        result.action_list = action_list

        return result


class PageInfo():
    def __init__(self):
        self.current_page_url = ""
        self.action_list = []
        self.pagination = ""
        self.num_pages = 2

    @staticmethod
    def from_dict(data):
        result = PageInfo()
        result.__dict__ = data

        action_list = []
        for action in data['action_list']:
            action_list.append(ActionInfo.from_dict(action))
        result.action_list = action_list

        return result


class ActionInfo():
    def __init__(self):
        self.action_type = ""
        self.path = []
        self.similar_path = []
        self.column_name = ""
        self.fill_value = ""
        self.row_column = []
        self.column_list = []

    @staticmethod
    def from_dict(data):
        result = ActionInfo()
        result.__dict__ = data
        return result


class ActionType():
    TEXT = 1
    URL = 2
    IMAGE_URL = 3

    SIMILAR_TEXT = 4
    SIMILAR_URL = 5
    SIMILAR_IMG = 6
    
    FULL_TABLE = 7
    COLUMN_TABLE = 8

    PAGINATION = 9
    MULTI_LEVEL = 10

    DOWNLOAD = 11
    SIMILAR_DOWNLOAD = 12

    CLICK = 13
    FILL = 14
    BACK_TO_ACTION = 15   


def save_project(session: Session, project: Project, sched_type: str, sched_val: str, alert_type: int, keyword: str):
    """
    Saves project definition on the server.
    :param session: The session.
    :param project: The project definition.
    :param sched_type: The scheduling type.
    :param sched_val: The value corrosponding sched_type.
    :return: The project id.
    :rtype: str
    """
    params = {}
    params['sched_type'] = sched_type
    params['sched_val'] = sched_val
    params['alert_type'] = alert_type
    params['keyword'] = keyword
    project = internal.as_jsonable(project)
    return session._invoke_method('scraping/save_project', project=project, **params)


def get_project(session: Session, project_id: str):
    """
    Gets the project definition from the server.

    :param session: The session.
    :param project_id: The project id to get.
    :return: The project definition.
    :rtype: dict
    """

    data = session._invoke_method('scraping/get_project', project_id=project_id)
    return Project.from_dict(data)


def get_project(session: Session):
    return  session._invoke_method('scraping/get_public_key')


def save_result(session: Session, project_id: str, output: dict=None, error: str=None):
    """
    Saves the execution result of a project.

    :param session: The session.
    :param project_id: The project id.
    :param output: (Optional) The result of the execution.
    :param error: (Optional) The error message to be shown to the user.
    :result: The newly assigned execution id.
    :rtype: str
    """

    return session._invoke_method('scraping/save_result', project_id=project_id, output=output, error=error)


def save_salt(session: Session, salt: str, project_id: str):
    """
    Saves the behind login details of a project.

    :param session: The session.
    :param salt: a cipher key for encryption and decryption.
    :param project_id: The project id.
    """
    return session._invoke_method('scraping/save_salt',
                                  salt = salt, project_id = project_id)

