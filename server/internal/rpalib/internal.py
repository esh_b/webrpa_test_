def as_jsonable(value: object):
    """
    Converts specified object into JSON-compatible form.
    Instances of classes are converted to dict.

    :param value: The object to be converted.
    :return: The converted data.
    """

    if hasattr(value, '__dict__'):
        result = {}
        for (name, member) in value.__dict__.items():
            result[name] = as_jsonable(member)
        return result
    elif isinstance(value, dict):
        result = {}
        for (name, member) in value.items():
            result[name] = as_jsonable(member)
        return result
    elif isinstance(value, list):
        result = []
        for member in value:
            result.append(as_jsonable(member))
        return result
    else:
        return value
