import traceback
from . import table_algo
import os
import requests
from mimetypes import MimeTypes
from urllib.parse import urljoin
from bs4 import Tag, BeautifulSoup as B
from cryptography.fernet import Fernet

from .column_wise_algo import extract
from .row2column import convert_row2column
from .rpalib.scraping import Project, PageInfo, ActionInfo
from api_v1.scraping.models import BehindLogin

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from selenium.common.exceptions import TimeoutException
from selenium.common.exceptions import NoSuchElementException


class Extraction():
    def __init__(self):
        # # Driver initiation
        # options = webdriver.ChromeOptions()
        # options.add_argument("headless")  # Remove this line if you want to see the browser popup
        # options.add_argument("--window-size=1920x1080")
        # prefs = {'profile.managed_default_content_settings.images':2, 'disk-cache-size': 4096}
        # options.add_experimental_option('prefs', prefs)
        # self.driver = webdriver.Chrome("/home/srirambsk/Documents/Fill&Click/chromedriver", chrome_options = options)
        # self.delay = 4	# seconds

        # Another way of Implementation
        options = Options()
        options.add_argument("--headless")  # Runs Chrome in headless mode.
        options.add_argument('--no-sandbox')  # # Bypass OS security model
        # options.add_argument('--disable-gpu')  # applicable to windows os only
        options.add_argument('start-maximized')
        options.add_argument('disable-infobars')
        options.add_argument("--disable-extensions")
        options.add_argument("--window-size=1920x1080")

        prefs = {'profile.managed_default_content_settings.images': 2, 'disk-cache-size': 4096}
        options.add_experimental_option('prefs', prefs)
        chrome_driver = 'internal/chromedriver'

        self.driver = webdriver.Chrome(chrome_options=options, executable_path=chrome_driver)
        self.delay = 4  # seconds

        self.domain_url = ''
        self.domain_url2 = ''
        self.words = []
        self.soup = B('', 'html.parser')
        self.table_flag = 0
        self.multi_table = 0
        self.output_list = []
        self.num = 0
        self.result = []

        self.sim_depth = -1
        self.sim_words = []
        self.download_flag = 0
        self.mimeval = ''
        self.goback_active = 0
        self.goback_url = ''
    def find_ele(self, xpath, flag):
        try:
            # element = WebDriverWait(driver, delay).until(EC.presence_of_element_located((By.XPATH, xpath)))
            # element = WebDriverWait(driver, delay).until(EC.visibility_of_element_located((By.XPATH, xpath)))
            if flag == 0:
                element = WebDriverWait(self.driver, self.delay).until(
                    EC.visibility_of_element_located((By.XPATH, xpath)))
            else:
                # element = WebDriverWait(driver, delay).until(EC.element_to_be_clickable((By.XPATH, xpath)))
                element = WebDriverWait(self.driver, self.delay).until(
                    EC.presence_of_element_located((By.XPATH, xpath)))

        except NoSuchElementException as e:
            print(traceback.print_exc())
            return False

        except TimeoutException as ex:
            print(traceback.print_exc())
            return False

        return element

    def fill_action(self, xpath, text):
        element = self.find_ele(xpath, 0)
        if element:
            element.send_keys(text)

    def click_action(self, xpath):
        # element = driver.find_element(By.XPATH, xpath)
        element = self.find_ele(xpath, 1)
        # print("Element found but...")
        if element:
            try:
                # print("Normal click!", xpath)
                element.click()
            except:
                try:
                    # print("Click using Javascript")
                    self.driver.execute_script("arguments[0].click();", element)
                except:
                    try:
                        # print("Keyboard Enter")
                        element.send_keys(Keys.RETURN)
                    except:
                        print(traceback.print_exc())

    def filter_text(self, text: str):
        text = text.replace(u'\\u3000', u' ')
        text = text.replace(u'\\xa0', u' ')
        text = text.replace(u'\\n', u' ')
        text = ' '.join(text.split())

        return text

    # def domain_name(self, link: str):
    # 	res = ''
    # 	count = 0

    # 	while link[0:3] == '../':
    # 		link = link[3:]
    # 		count+=1

    # 	if link[0:4] != 'http' and link != '':
    # 		if link[0] == '/':
    # 			link = link[1:]
    # 		res = self.domain_url + link
    # 		with requests.head(res) as r:
    # 			if r.status_code < 200 or r.status_code > 299:
    # 				if count != 0:
    # 					res = "/".join(self.domain_url2.split("/")[:-count]) + '/' + link
    # 				else:
    # 					res = self.domain_url2 + '/' + link
    # 	return res

    def domain_name(self, link: str):
        if link[0:4] != 'http' and link[0] == '/' and link != '':
            link = self.domain_url + link[1:]
        elif link[0:4] != 'http' and link[0] != '/' and link != '':
            link = self.domain_url + link
        return link

    def mime_type(self, url):
        return MimeTypes().guess_type(url)[0]

    def full_tag(self, node: Tag):
        """
        Function to convert a given node into it's tag_name, id_name and class_name.

        :param node: Input node to convert(Eg: <div class= "qwe cvb asd" id = "uiop">...</div>).
        :return: div#uiop.qwe.cvb.asd
        :rtype: string
        """
        tag_name = node.name.lower()
        class_name = ''
        if node.get('id'):
            id_name = '#' + node.get('id')
        else:
            id_name = ''
        xx = node.get('class')
        if xx:
            for x in xx:
                if x != '':
                    class_name = class_name + '.' + x
        else:
            class_name = ''
        return tag_name + id_name + class_name

    def get_tag_attr(self, str1: str):
        """
        Function on self.full_tag string input gives all the node attributes such as tag_name, id_name and class_name
        :param str1: A string as an input (Eg: div#uiop.qwe.cvb.asd).
        :return: A list of tag_name, id_name and class_name => ["div", "uiop", "qwe cvb asd"].
        :rtype: list
        """
        arr = str1.split("#")
        xarr = []
        if len(arr) < 2:
            id_name = ''
            arr2 = arr[0].split(".")
            tag_name = arr2[0]
            xarr = arr2
        else:
            tag_name = arr[0]
            arr2 = arr[1].split(".")
            id_name = arr2[0]
            xarr = arr2
        class_name = ''
        if len(xarr) < 2:
            class_name = ' '
        elif len(xarr) < 3:
            class_name = xarr[1] + ' '
        else:
            for k in range(1, len(xarr)):
                class_name = class_name + xarr[k] + ' '
        class_name = class_name[:-1]
        return [tag_name, id_name, class_name]

    def path_ofnode(self, e: Tag):
        """
        Function to get the complete path of the given node

        :param e: Input Node
        :return: Appended string of node's path from head to current element instance
        :rtype: string
        """
        x = []
        while e.name != "[document]":
            x.append(self.full_tag(e))
            e = e.parent
        result = ""
        for string in x[::-1]:
            result = result + string + " "
        return result[:-1]

    def simple_text(self, e: Tag):
        sim_text = ""
        for string in e.stripped_strings:
            sim_text = sim_text + repr(string)[1:-1] + " "
        sim_text = sim_text[:-1]
        if sim_text != "":  # Check if there's a text string inside
            self.output_list.append(self.filter_text(sim_text))
        else:  # Else, check if there's an <img> tag
            if e.name == 'img' and e['src'] != "":
                self.output_list.append(self.domain_name(e['src']))
            else:
                img = e.find('img', src=True)
                if img != None:
                    self.output_list.append(self.domain_name(img['src']))
                else:
                    self.output_list.append("")

    def simple_url(self, e: Tag):
        url_val = ''
        if e.name == 'a' and e.has_attr('href'):  # Check if it is an <a> tag
            url_val = self.domain_name(e['href'])
        else:
            a = e.find('a', href=True)  # Check if it contains an <a> tag inside
            if a != None:
                url_val = self.domain_name(a['href'])
            else:
                nodex = e.parent
                while nodex.name.lower() != "html":
                    if nodex.name.lower() == 'a' and nodex.has_attr('href'):
                        url_val = self.domain_name(nodex['href'])
                        break
                    else:
                        nodex = nodex.parent
        return url_val

    def simple_img(self, e: Tag):
        img_val = ''
        if e.name == 'img' and e['src'] != "":  # Check if it is an <img> tag
            img_val = self.domain_name(e['src'])
        else:
            img = e.find('img', src=True)  # Check if it conatins an <img> tag inside
            if img != None:
                img_val = self.domain_name(img['src'])
        return img_val

    def depth_count(self, depth):
        val1 = self.get_tag_attr(self.words[depth])
        val2 = self.get_tag_attr(self.sim_words[depth])

        if val1[0].lower() == val2[0].lower():
            if (val1[1] == val2[1] and val1[2] == val2[2]) or val1[0].lower() == 'html' or val1[0].lower() == 'body':
                if depth != len(self.words) - 1:
                    return self.depth_count(depth + 1)
            else:
                return depth - 1
        else:
            return -1

        return depth

    def path_table(self, e: Tag, depth: int, flag: int, row: int, col: int):
        if self.full_tag(e) == self.words[depth] or e.name.lower() == "html" or e.name.lower() == "body":
            if e.name.lower() == "table":
                # print(e)
                if self.multi_table == 0:
                    self.multi_table = 1
                    rows = e.select('tr')
                    try:
                        cells = rows[row]
                        # print(cells)
                        head = cells.select('th')
                        # print(head)
                        if (col < len(head)):
                            if flag == 1:
                                self.simple_text(head[col])
                            elif flag == 2:
                                # self.simple_url(head[col])
                                self.output_list.append(self.simple_url(head[col]))
                        else:
                            cell = cells.select('td')
                            column = col - len(head)
                            if flag == 1:
                                self.simple_text(cell[column])
                            elif flag == 2:
                                # self.simple_url(cell[column])
                                self.output_list.append(self.simple_url(cell[column]))
                    except Exception as e:
                        print(e)

            else:
                for child in e.contents:
                    if isinstance(child, Tag):
                        self.path_table(child, depth + 1, flag, row, col)

    def path_table_main(self, e: Tag, depth: int, flag: int, row: int, col: int):
        nodex = e
        if self.full_tag(nodex) == self.words[depth] or nodex.name.lower() == "html" or nodex.name.lower() == "body":
            while nodex.name.lower() != "html":
                if nodex.name.lower() == 'table':
                    rows = nodex.select('tr')
                    try:
                        cells = rows[row]
                        head = cells.select('th')
                        if (col < len(head)):
                            if flag == 1:
                                self.simple_text(head[col])
                            elif flag == 2:
                                # self.simple_url(head[col])
                                self.output_list.append(self.simple_url(head[col]))
                        else:
                            cell = cells.select('td')
                            column = col - len(head)
                            if flag == 1:
                                self.simple_text(cell[column])
                            elif flag == 2:
                                # self.simple_url(cell[column])
                                self.output_list.append(self.simple_url(cell[column]))
                    except Exception as e:
                        print(e)

                    self.table_flag = 1
                    break
                else:
                    nodex = nodex.parent

    def preorder_traversal(self, e: Tag, depth: int, flag: int):
        """
        Function to traverse the DOM tree recurrsively to find similar items.

        :param e: Current node being traversed
        :param depth: Depth to track the inputDOM path
        :param flag: flag=4 means find similar text; flag=5 means find similar paths only
        :return: List of similar item paths or similar item text
        :rtype: list
        """
        val = self.get_tag_attr(self.words[depth])
        if e.name == val[0]:
            # print(self.full_tag(e))
            if depth == (len(self.words) - 1):
                if flag == 4:
                    self.simple_text(e)
                elif flag == 5:
                    # self.simple_url(e)
                    self.output_list.append(self.simple_url(e))
                elif flag == 12:
                    url_x = self.simple_img(e)
                    if url_x == '':
                        url_x = self.simple_url(e)
                    if self.num == 0:
                        self.mimeval = self.mime_type(url_x)
                        self.num += 1
                    if self.mime_type(url_x) == self.mimeval:
                        self.output_list.append(url_x)
            else:
                for child in e.contents:
                    if isinstance(child, Tag):
                        self.preorder_traversal(child, depth + 1, flag)

    def similar_traversal(self, e: Tag, depth: int, flag: int):
        if self.sim_depth >= depth:
            if self.full_tag(e) == self.words[depth] or e.name.lower() == "html" or e.name.lower() == "body":
                if depth == (len(self.words) - 1):
                    if flag == 4:
                        self.simple_text(e)
                    elif flag == 5 or flag == 10:
                        # self.simple_url(e)
                        self.output_list.append(self.simple_url(e))
                else:
                    for child in e.contents:
                        if isinstance(child, Tag):
                            self.similar_traversal(child, depth + 1, flag)
        else:
            val = self.get_tag_attr(self.words[depth])
            if e.name.lower() == val[0].lower():
                if depth == (len(self.words) - 1):
                    if flag == 4:
                        self.simple_text(e)
                    elif flag == 5 or flag == 10:
                        # self.simple_url(e)
                        self.output_list.append(self.simple_url(e))
                else:
                    for child in e.contents:
                        if isinstance(child, Tag):
                            self.similar_traversal(child, depth + 1, flag)

    def path_traversal(self, e: Tag, depth: int, flag: int):
        """
        Function which extracts simpleText, url extraction, img extraction.

        :param e: Current node being traversed
        :param depth: Depth to track the inputDOM path
        :param flag: flag= 1 means Simple Text extraction,
                    flag= 2 means URL extraction,
                    flag= 3 means Image extraction
        :return: Outputs required extracted data based on the type
        """

        if self.full_tag(e) == self.words[depth] or e.name == "html" or e.name == "body":
            if depth == (len(self.words) - 1):
                if flag == 1:
                    self.simple_text(e)
                elif flag == 2:
                    # self.simple_url(e)
                    self.output_list.append(self.simple_url(e))
                elif flag == 3:
                    self.output_list.append(self.simple_img(e))
                elif flag == 11:
                    url_x = self.simple_img(e)
                    if url_x == '':
                        url_x = self.simple_url(e)
                    self.output_list.append(url_x)
            else:
                for child in e.contents:
                    if isinstance(child, Tag):
                        self.path_traversal(child, depth + 1, flag)

    def pagination_path(self, e: Tag, depth: int):
        if self.full_tag(e) == self.words[depth] or e.name == "html" or e.name == "body":
            if depth == (len(self.words) - 1):
                # self.simple_url(e)
                self.output_list.append(self.simple_url(e))
            else:
                for child in e.contents:
                    if isinstance(child, Tag):
                        self.pagination_path(child, depth + 1)

    def find_path(self, node_list: list, depth: int, row_col: list, flag: int):
        if len(node_list) == 0:
            # Exception: Path not found
            print("Path Not Found")
            if flag == 9:
                self.preorder_traversal(self.soup.html, 0, 5)
        elif depth == 0:
            print("Multiple similar paths exists")
            if flag == 9:
                self.pagination_path(node_list[-1], depth)
            elif flag < 4 or flag == 11:
                if len(row_col) != 0:
                    self.path_table_main(node_list[0], depth, flag, row_col[0], row_col[1])
                    if self.table_flag == 0:
                        self.path_table(node_list[0], depth, flag, row_col[0], row_col[1])
                else:
                    self.path_traversal(node_list[0], depth, flag)
            elif flag == 12:
                self.preorder_traversal(self.soup.html, 0, 12)
            elif flag != 109:
                self.sim_depth = self.depth_count(0)
                self.similar_traversal(self.soup.html, 0, flag)
        elif len(node_list) == 1:
            # Found the correct branch
            if flag == 9:
                self.pagination_path(node_list[-1], depth)
            elif flag < 4 or flag == 11:
                if len(row_col) != 0:
                    self.path_table_main(node_list[0], depth, flag, row_col[0], row_col[1])
                    if self.table_flag == 0:
                        self.path_table(node_list[0], depth, flag, row_col[0], row_col[1])
                else:
                    self.path_traversal(node_list[0], depth, flag)
            elif flag == 12:
                self.preorder_traversal(self.soup.html, 0, 12)
            else:
                path_node = self.path_ofnode(node_list[0])

                for x in range(depth + 1, len(self.words)):
                    path_node = path_node + " " + self.words[x]
                if flag == 109:
                    self.sim_words = path_node.split()
                else:
                    self.words = path_node.split()
                    self.sim_depth = self.depth_count(0)
                    self.similar_traversal(self.soup.html, 0, flag)
                # print(self.output_list)
        else:
            # Traverse upwards
            el_list = []
            for el in node_list:
                el = el.parent
                if self.full_tag(el) == self.words[depth - 1] or (
                        el.name == "html" and ("html" in self.words[depth - 1])) or (
                        el.name == "body" and ("body" in self.words[depth - 1])):
                    el_list.append(el)
                elif "--hover" in self.words[depth - 1]:
                    # self.words[depth-1] = self.words[depth-1].replace('--hover', '')
                    hover = self.words[depth - 1].split(".")
                    class_text = ''
                    for i in range(0, len(hover)):
                        if "--hover" not in hover[i]:
                            class_text = class_text + hover[i] + '.'
                    class_text = class_text[:-1]
                    self.words[depth - 1] = class_text
                    if self.full_tag(el) == self.words[depth - 1]:
                        el_list.append(el)
            if (len(el_list) == 0) and ((len(self.words) - 1) != depth):
                path_node = self.path_ofnode(node_list[0])
                count = len(path_node.split())
                for x in range(depth + 1, len(self.words)):
                    path_node = path_node + " " + self.words[x]
                self.words = path_node.split()
                self.find_path(node_list, count - 1, row_col, flag)
            else:
                self.find_path(el_list, depth - 1, row_col, flag)

    def classify_action(self, url, act):

        self.output_list = []

        print(act.action_type)
        if act.action_type == 13:
            self.goback_active = 1
            self.click_action(act.path[1])
            self.soup = B(self.driver.page_source, 'html.parser')
        elif act.action_type == 14:
            self.fill_action(act.path[1], act.fill_value)
        elif act.action_type == 15:
            self.page_source(self.goback_url)
        elif act.action_type == 7:
            res = table_algo.extract(url, act.path[0])
            return convert_row2column(res, act.column_list)
        elif act.action_type == 8:
            res = extract(url, act.path[0], act.column_list)
            return convert_row2column(res, [])
        else:
            if act.action_type == 11 or act.action_type == 12:
                self.download_flag = 1

            if act.action_type == 4 or act.action_type == 5 or act.action_type == 10:
                self.sim_words = act.similar_path[0].split()
                self.words = self.sim_words
                val = self.get_tag_attr(self.sim_words[-1])
                node = self.soup.findAll(val[0], {"class": val[2]}, {"id": val[1]})
                self.find_path(node, len(self.sim_words) - 1, [], 109)
            self.words = act.path[0].split()
            val = self.get_tag_attr(self.words[-1])
            node = self.soup.findAll(val[0], {"class": val[2]}, {"id": val[1]})
            self.find_path(node, len(self.words) - 1, act.row_column, act.action_type)

            if (act.action_type < 4 or act.action_type == 11) and len(self.output_list) > 1:
                return [self.output_list[0]]
            elif self.output_list == None:
                return []
            else:
                return self.output_list

    def page_source(self, url: str):

        self.driver.get(url)
        self.soup = B(self.driver.page_source, 'html.parser')
        if self.goback_active == 0:
            self.goback_url = url

    # file = open("websites_test/test123.html","w")
    # file.write(str(self.soup))
    # file.close()

    def main_function(self, url: str, page: PageInfo):

        self.domain_url = urljoin(url, '/')
        path1, filename1 = os.path.split(url)
        self.domain_url2 = path1

        self.page_source(url)

        self.result = []
        if len(page.pagination) == 0:
            for act in page.action_list:
                if act.action_type == 13 or act.action_type == 14:
                    self.classify_action(url, act)
                else:
                    self.result.append({act.column_name: self.classify_action(url, act)})
        else:
            out = []
            c = 0
            while c < page.num_pages:
                resultx = []
                if url != '':
                    for act in page.action_list:
                        if c == 0 and (act.action_type == 13 or act.action_type == 14 or act.action_type == 15):
                            self.classify_action(url, act)
                            resultx.append("")
                        elif act.action_type != 13 and act.action_type != 14 and act.action_type != 15:
                            resultx.append(self.classify_action(url, act))
                        else:
                            resultx.append("")

                    act1 = ActionInfo()
                    act1.action_type = 9
                    act1.row_column = []
                    act1.path = page.pagination

                    temp_url = self.classify_action(url, act1)
                    if temp_url != []:
                        url = temp_url[-1]
                        if url != "" and url != "#":
                            if url[0:4] != 'http' and url[0] == '/':
                                url = domain_url + url[1:]
                            elif url[0:4] != 'http' and url[0] != '/':
                                url = domain_url + url
                        self.page_source(url)
                    else:
                        c = page.num_pages
                c += 1
                if len(out) == 0:
                    out = resultx
                else:
                    for i in range(len(resultx)):
                        out[i] += resultx[i]
            for i in range(len(page.action_list)):

                if page.action_list[i].action_type != 13 and page.action_list[i].action_type != 14 and page.action_list[
                    i].action_type != 15:
                    self.result.append({page.action_list[i].column_name: out[i]})

        return self.result

    def recur_page(self, pages: list, depth):
        output1 = []
        current = pages[depth]
        self.goback_active = 0
        out1 = self.main_function(current.current_page_url, current)
        output1.append(out1)
        print(out1)
        if len(pages) != (depth + 1):
            count = 0
            domain_url = urljoin(current.current_page_url, '/')
            for i in range(0, len(current.action_list)):
                if current.action_list[i].action_type == 10:
                    print(i - count)
                    for key, value in out1[i - count].items():
                        similar_list = value
                    for i in range(0, len(similar_list)):
                        if similar_list[i][0:4] != 'http' and similar_list[i][0:4] != 'file':
                            similar_list[i] = domain_url + similar_list[i]

                    final = []
                    for similar_page in similar_list:
                        pages[depth + 1].current_page_url = similar_page
                        final.append(self.recur_page(pages, depth + 1))
                    output1.append(final)
                elif current.action_list[i].action_type == 13 or current.action_list[i].action_type == 14 or \
                        current.action_list[i].action_type == 15:
                    count += 1

        return output1

    def login_process(self, login_info: dict, key):
        self.driver.get(login_info.login_url)
        for act in login_info.action_list:
            if act.action_type == 13:
                self.click_action(act.path[1])
            elif act.action_type == 14:

                self.fill_action(act.path[1], Fernet(key.encode('utf-8')).decrypt(bytes(act.fill_value, 'utf-8')).decode("utf-8"))


    def process(self, project: Project, pid, **key):

        if project.login_details and project.project_type != 5:
            self.login_process(project.login_details[0], key)

        # self.driver.get(project.start_url)
        output = []
        if project.project_type == 1 or project.project_type == 2:
            for page in project.page_list:
                output.append(self.main_function(page.current_page_url, page))

        elif project.project_type == 3:
            output = self.recur_page(project.page_list, 0)

        elif project.project_type == 4:
            for key, value in project.product_url.items():
                output.append([{"URL_List": value}])
                final = []
                for page in value:
                    final.append([self.main_function(page, project.page_list[0])])
                output.append(final)

        elif project.project_type == 5:
            final = []
            for key in project.product_url:
                final.append(key)
            output.append([{"Product": final}])

            final = []
            for key, value in project.product_url.items():
                xlist = []
                for i in range(0, len(value)):
                    xlist.append([self.main_function(value[i], project.page_list[i])])
                final.append([[{"URL_List": value}], xlist])
            output.append(final)

        self.driver.quit()

        return [project.project_type, output, self.download_flag]