#import socket module
from socket import *

serverPort = 9988 
serverSocket = socket(AF_INET, SOCK_STREAM)
#Prepare a sever socket
#Fill in start
serverSocket.bind(('',serverPort))
serverSocket.listen(1)
print('The web server is up on port:',serverPort)
#Fill in end
while True:
    #Establish the connection
    print('Ready to serve...')
    (connectionSocket, addr) = serverSocket.accept()

    try:

        message = connectionSocket.recv(1024)
        print(message,'::',message.split()[0],':',message.split()[1])
        filename = message.split()[1]
        print(filename,'||',filename[1:])
        f = open(filename[1:])
        outputdata = f.read()
        print(outputdata)
        #Send one HTTP header line into socket
        #Fill in start
        connectionSocket.send(b'\nHTTP/1.1 200 OK\n\n')
        connectionSocket.send(outputdata.encode('utf-8'))
        #Fill in end
        #Send the content of the requested file to the client
    
        # for i in range(0, len(outputdata)):
        #   x = outputdata[i]
        #   connectionSocket.send(x.encode('utf-8'))
        #   connectionSocket.close()
    
    except IOError:
        #Send response message for file not found
        #Fill in start
        connectionSocket.send(b'\nHTTP/1.1 404 Not Found\n\n')

connectionSocket.send(b'\nHTTP/1.1 404 Not Found\n\n')