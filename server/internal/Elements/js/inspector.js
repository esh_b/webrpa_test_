(function(document) {
	var last;

	function cssPath(el) {
		var fullPath    = 0,  // Set to 1 to build ultra-specific full CSS-path, or 0 for optimised selector
		    useNthChild = 0,  // Set to 1 to use ":nth-child()" pseudo-selectors to match the given element
		    cssPathStr = '',
		    testPath = '',
		    parents = [],
		    parentSelectors = [],
		    tagName,
		    cssId,
		    cssClass,
		    tagSelector,
		    vagueMatch,
		    nth,
		    i,
		    c;
		
		// Go up the list of parent nodes and build unique identifier for each:
		while ( el ) {
			vagueMatch = 0;

			// Get the node's HTML tag name in lowercase:
			tagName = el.nodeName.toLowerCase();
			
			// Get node's ID attribute, adding a '#':
			cssId = ( el.id ) ? ( '#' + el.id ) : false;
			
			// Get node's CSS classes, replacing spaces with '.':
			cssClass = ( el.className ) ? ( '.' + el.className.replace(/\s+/g,".") ) : '';

			// Build a unique identifier for this parent node:
			if ( cssId ) {
				// Matched by ID:
				tagSelector = tagName + cssId + cssClass;
			} else if ( cssClass ) {
				// Matched by class (will be checked for multiples afterwards):
				tagSelector = tagName + cssClass;
			} else {
				// Couldn't match by ID or class, so use ":nth-child()" instead:
				vagueMatch = 1;
				tagSelector = tagName;
			}
			
			// Add this full tag selector to the parentSelectors array:
			parentSelectors.unshift( tagSelector )

			// If doing short/optimised CSS paths and this element has an ID, stop here:
			// if ( cssId && !fullPath )
			// 	break;
			
			// Go up to the next parent node:
			el = el.parentNode !== document ? el.parentNode : false;
			
		} // endwhile
		
		
		// Build the CSS path string from the parent tag selectors:
		for ( i = 0; i < parentSelectors.length; i++ ) {
			cssPathStr += ' ' + parentSelectors[i];// + ' ' + cssPathStr;
			
			// If using ":nth-child()" selectors and this selector has no ID / isn't the html or body tag:
			if ( useNthChild && !parentSelectors[i].match(/#/) && !parentSelectors[i].match(/^(html|body)$/) ) {
				
				// If there's no CSS class, or if the semi-complete CSS selector path matches multiple elements:
				if ( !parentSelectors[i].match(/\./) || $( cssPathStr ).length > 1 ) {
					
					// Count element's previous siblings for ":nth-child" pseudo-selector:
					for ( nth = 1, c = el; c.previousElementSibling; c = c.previousElementSibling, nth++ );
					
					// Append ":nth-child()" to CSS path:
					cssPathStr += ":nth-child(" + nth + ")";
				}
			}
			
		}
		
		// Return trimmed full CSS path:
		return cssPathStr.replace(/^[ \t]+|[ \t]+$/, '');
	}

	function simplify(str1){
		var arr = str1.split("#");
		if(arr.length < 2) {
			var arr2 = arr[0].split(".");
			return arr2[0];
		}
		else { 
			return arr[0]; 
		}
	}

	function pathNode(el, strPath, depth) {
		// var xpath = strPath.split(" ");
		var val = simplify(strPath[depth]);
		if(el.nodeName.toLowerCase() == val.toLowerCase())
		{
			console.log(val, depth);
			if(depth == (strPath.length - 1) )
			{
				el.style.outline = '2px solid #090';
				console.log(el);
			}
			else 
			{
				depth = depth + 1;
				var children = el.childNodes;
				for (var i = 0; i < children.length; i++) {
					if(children[i].nodeType == 1){
						pathNode(children[i], strPath, depth);
					}
				}
			}
		}
	}

	/**
	 * MouseOver action for all elements on the page:
	 */
	function inspectorMouseOver(e) {
		// NB: this doesn't work in IE (needs fix):
		var element = e.target;
		// var tagSelector;
		// Set outline:
		
		// If it is not green then change it to red
		if (e.target.style.outline != '2px solid rgb(0, 153, 0)')
		{
			element.style.outline = '2px solid #f00';
		}
		// If it is green then change it to yellow
		// else {
		// 	element.style.outline = '2px solid #ffff00';
		// }
		// Set last selected element so it can be 'deselected' on cancel.
		// last = element;
		
		// tagSelector.tagName.showModalDialog()
	}
	
	
	/**
	 * MouseOut event action for all elements
	 */
	function inspectorMouseOut(e) {
		// Remove outline from element:

		if (e.target.style.outline != '2px solid rgb(0, 153, 0)')
		{
			e.target.style.outline = '';
		}
	}
	
	
	/**
	 * Click action for hovered element
	 */
	function inspectorOnClick(e) {
		e.preventDefault();
		// If it is either yellow or green then do deselect by changing the color to ''.
		if( e.target.style.outline == '2px solid rgb(0, 153, 0)') 
		{
			e.target.style.outline = '';
		}
		else {	// Color change to green
			e.target.style.outline = '2px solid #090';
			var cars = cssPath(e.target);
			console.log(cars);
			

			var htmlTarget = e.target;
			while(htmlTarget.nodeName.toLowerCase() !== 'html'){
				htmlTarget = htmlTarget.parentNode;
			}
			xc = cars.split(" ");
			pathNode(htmlTarget, xc, 0);
		}

		// last = e.target;
		// console.log(e);
		// console.log(e.target)
		// console.log(last);
		// console.log(last.style.outline);
		// These are the default actions (the XPath code might be a bit janky)
		// Really, these could do anything:
		
		// var buses = [
		// 	'html body.landing section#two.wrapper.style2 div.container div.row.uniform div.4u.6u(2).12u$(3)',
		// 	'html.global.xlarge.large body.landing div#skel-layers-wrapper section#two.wrapper.style2 div.container div.row.uniform div.4u.6u$(2).12u$(3)'
		// 	'html body.landing section#two.wrapper.style2 div.container div.row.uniform div.4u.6u$(2).12u$(3)',
		// 	'html body.landing section#two.wrapper.style2 div.container div.row.uniform div.4u$.6u(2).12u$(3)',
		// 	'html body.landing section#two.wrapper.style2 div.container div.row.uniform div.4u$.6u$(2).12u$(3)',
		// 	'html body.landing section#two.wrapper.style2 div.container div.row.uniform div.4u.6u(2).12u$(3)',
		// 	'html body.landing section#two.wrapper.style2 div.container div.row.uniform div.4u.6u$(2).12u$(3)'
		// ];
		// var cars = cssPath(e.target);
		// console.log(cars);
		

		// var htmlTarget = e.target;
		// while(htmlTarget.nodeName.toLowerCase() !== 'html'){
		// 	htmlTarget = htmlTarget.parentNode;
		// }
		// console.log( getXPath(e.target).join('/') ); 
		// xc = buses[0].split(" ");
		// for(var k =0; k<xc.length; k++)
		// {
		// 	console.log(simplify(xc[k]));
		// }
		// for (i = 0; i < buses.length; i++) {
		// 	arrBuses = buses[i].split(" ");
		// 	pathNode(htmlTarget, arrBuses, 0);
		// }
		// xc = cars.split(" ");
		// pathNode(htmlTarget, xc, 0);

		return false;
	}


	/**
	 * Function to cancel inspector:
	 */
	// function inspectorCancel(e) {
	// 	// Unbind inspector mouse and click events:
	// 	if (e === null && event.keyCode === 27) { // IE (won't work yet):
	// 		document.detachEvent("mouseover", inspectorMouseOver);
	// 		document.detachEvent("mouseout", inspectorMouseOut);
	// 		document.detachEvent("click", inspectorOnClick);
	// 		document.detachEvent("keydown", inspectorCancel);
	// 		last.style.outlineStyle = 'none';
	// 	} else if(e.which === 27) { // Better browsers:
	// 		document.removeEventListener("mouseover", inspectorMouseOver, true);
	// 		document.removeEventListener("mouseout", inspectorMouseOut, true);
	// 		document.removeEventListener("click", inspectorOnClick, true);
	// 		document.removeEventListener("keydown", inspectorCancel, true);
			
	// 		// Remove outline on last-selected element:
	// 		last.style.outline = 'none';
	// 	}
	// }
	

	/**
	 * Add event listeners for DOM-inspectorey actions
	 */
	if ( document.addEventListener ) {
		document.addEventListener("mouseover", inspectorMouseOver, true);
		document.addEventListener("mouseout", inspectorMouseOut, true);
		document.addEventListener("click", inspectorOnClick, true);
		// document.addEventListener("keydown", inspectorCancel, true);
	} else if ( document.attachEvent ) {
		document.attachEvent("mouseover", inspectorMouseOver);
		document.attachEvent("mouseout", inspectorMouseOut);
		document.attachEvent("click", inspectorOnClick);
		// document.attachEvent("keydown", inspectorCancel);
	}
	
})(document);