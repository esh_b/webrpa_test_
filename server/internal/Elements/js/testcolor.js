(function(document) {
	var last;
	var xcount = 0;
	function cssPath(el) {
		var fullPath    = 0,  // Set to 1 to build ultra-specific full CSS-path, or 0 for optimised selector
		    useNthChild = 0,  // Set to 1 to use ":nth-child()" pseudo-selectors to match the given element
		    cssPathStr = '',
		    testPath = '',
		    parents = [],
		    parentSelectors = [],
		    tagName,
		    cssId,
		    cssClass,
		    tagSelector,
		    vagueMatch,
		    nth,
		    i,
		    c;
		
		// Go up the list of parent nodes and build unique identifier for each:
		while ( el ) {
			vagueMatch = 0;

			// Get the node's HTML tag name in lowercase:
			tagName = el.nodeName.toLowerCase();
			
			// Get node's ID attribute, adding a '#':
			cssId = ( el.id ) ? ( '#' + el.id ) : false;
			
			// Get node's CSS classes, replacing spaces with '.':
			cssClass = ( el.className ) ? ( '.' + el.className.replace(/\s+/g,".") ) : '';

			// Build a unique identifier for this parent node:
			if ( cssId ) {
				// Matched by ID:
				tagSelector = tagName + cssId + cssClass;
			} else if ( cssClass ) {
				// Matched by class (will be checked for multiples afterwards):
				tagSelector = tagName + cssClass;
			} else {
				// Couldn't match by ID or class, so use ":nth-child()" instead:
				vagueMatch = 1;
				tagSelector = tagName;
			}
			
			// Add this full tag selector to the parentSelectors array:
			parentSelectors.unshift( tagSelector )

			// If doing short/optimised CSS paths and this element has an ID, stop here:
			// if ( cssId && !fullPath )
			// 	break;
			
			// Go up to the next parent node:
			el = el.parentNode !== document ? el.parentNode : false;
			
		} // endwhile
		
		
		// Build the CSS path string from the parent tag selectors:
		for ( i = 0; i < parentSelectors.length; i++ ) {
			cssPathStr += ' ' + parentSelectors[i];// + ' ' + cssPathStr;
			
			// If using ":nth-child()" selectors and this selector has no ID / isn't the html or body tag:
			if ( useNthChild && !parentSelectors[i].match(/#/) && !parentSelectors[i].match(/^(html|body)$/) ) {
				
				// If there's no CSS class, or if the semi-complete CSS selector path matches multiple elements:
				if ( !parentSelectors[i].match(/\./) || $( cssPathStr ).length > 1 ) {
					
					// Count element's previous siblings for ":nth-child" pseudo-selector:
					for ( nth = 1, c = el; c.previousElementSibling; c = c.previousElementSibling, nth++ );
					
					// Append ":nth-child()" to CSS path:
					cssPathStr += ":nth-child(" + nth + ")";
				}
			}
			
		}
		
		// Return trimmed full CSS path:
		return cssPathStr.replace(/^[ \t]+|[ \t]+$/, '');
	}


	function simplify(str1) {
		var arr = str1.split("#");
		if(arr.length < 2) {
			var arr2 = arr[0].split(".");
			return arr2[0];
		}
		else { 
			return arr[0]; 
		}
	}

	function pathNode(el, strPath, depth) {
		// var xpath = strPath.split(" ");
		var val = simplify(strPath[depth]);
		if(el.nodeName.toLowerCase() == val.toLowerCase())
		{
			if(depth == (strPath.length - 1) )
			{
				el.style.outline = '2px solid #090';

			}
			else 
			{
				depth = depth + 1;
				var children = el.childNodes;
				for (var i = 0; i < children.length; i++) {
					if(children[i].nodeType == 1){
						pathNode(children[i], strPath, depth);
					}
				}
			}
		}
	}

	/**
	 * MouseOver action for all elements on the page:
	 */
	function inspectorMouseOver(e) {
		// NB: this doesn't work in IE (needs fix):
		//var element = e.target;
		// var tagSelector;
		// Set outline:
		
		// If it is not green then change it to red
		if ( !((e.target.style.outline == 'rgb(0, 153, 0) solid 2px') || (e.target.style.outline == '2px solid rgb(0, 153, 0)')) )
		{
			e.target.style.outline = '2px solid #f00';
		}
		// If it is green then change it to yellow
		else {
			e.target.style.outline = '2px solid #ffff00';
		}
		// Set last selected element so it can be 'deselected' on cancel.
		// last = element;
		
		// tagSelector.tagName.showModalDialog()
	}
	
	
	/**
	 * MouseOut event action for all elements
	 */
	function inspectorMouseOut(e) {
		// Remove outline from element

		// If it is Yellow change it to green when mouse out
		if((e.target.style.outline == 'rgb(255, 255, 0) solid 2px') || (e.target.style.outline == '2px solid rgb(255, 255, 0)'))
		{
			e.target.style.outline = '2px solid #090';
		}
		// Else if it is anything except green(which is red of course) then make it none
		else if ( !((e.target.style.outline == 'rgb(0, 153, 0) solid 2px') || (e.target.style.outline == '2px solid rgb(0, 153, 0)')) )
		{
			e.target.style.outline = '';
		}
		// e.target.style.outline = '2px solid #090';
	}
	
	/**
	 * Click action for hovered element
	 */
	function inspectorOnClick(e) {
		e.preventDefault();
		if(xcount == 0){
			var htmltag = document.getElementsByTagName("html");
			var divtag = document.createElement("div");
			var cln = htmltag[0].cloneNode(true);
			divtag.appendChild(cln);
			console.log(divtag.innerHTML);
			$.ajax({
				url : "http://127.0.0.1:5000/loadhtml",
				type : "POST",
				contentType: 'text/html;charset=UTF-8',
				data : divtag.innerHTML,
				success: function(result) {
					console.log(result);
				},
				error: function(error){
					console.log(error);
				}
			});
			xcount = 1;
		}
		// If it is either yellow or green then do deselect by changing the color to ''.
		if( (e.target.style.outline == 'rgb(255, 255, 0) solid 2px') || (e.target.style.outline == 'rgb(0, 153, 0) solid 2px') || (e.target.style.outline == '2px solid rgb(0, 153, 0)') || (e.target.style.outline == '2px solid rgb(255, 255, 0)') )
		{
			e.target.style.outline = '';
		}
		else
		{
			// Color change to green			
			e.target.style.outline = '2px solid #090';
			// var path = [];
			// path = e.path;
			// for (var i = 0; i < path.length; i++) {
			// 	console.log(path[i]);
			// }
			var cars = cssPath(e.target);
			console.log(cars);

			var htmlTarget = e.target;

			while(htmlTarget.nodeName.toLowerCase() !== 'html'){
				htmlTarget = htmlTarget.parentNode;
			}
			var htmltag = document.documentElement;
			xc = cars.split(" ");
			pathNode(htmlTarget, xc, 0);
		}
		
		// These are the default actions (the XPath code might be a bit janky)
		// Really, these could do anything:

		// console.log( getXPath(e.target).join('/') ); 

		return false;
	}


	/**
	 * Function to cancel inspector:
	 */
	// function inspectorCancel(e) {
	// 	// Unbind inspector mouse and click events:
	// 	if (e === null && event.keyCode === 27) { // IE (won't work yet):
	// 		document.detachEvent("mouseover", inspectorMouseOver);
	// 		document.detachEvent("mouseout", inspectorMouseOut);
	// 		document.detachEvent("click", inspectorOnClick);
	// 		document.detachEvent("keydown", inspectorCancel);
	// 		last.style.outlineStyle = 'none';
	// 	} else if(e.which === 27) { // Better browsers:
	// 		document.removeEventListener("mouseover", inspectorMouseOver, true);
	// 		document.removeEventListener("mouseout", inspectorMouseOut, true);
	// 		document.removeEventListener("click", inspectorOnClick, true);
	// 		document.removeEventListener("keydown", inspectorCancel, true);
			
	// 		// Remove outline on last-selected element:
	// 		last.style.outline = 'none';
	// 	}
	// }
	

	/**
	 * Add event listeners for DOM-inspectorey actions
	 */
	if ( document.addEventListener ) {
		document.addEventListener("mouseover", inspectorMouseOver, true);
		document.addEventListener("mouseout", inspectorMouseOut, true);
		document.addEventListener("click", inspectorOnClick, true);
		// document.addEventListener("keydown", inspectorCancel, true);
	} else if ( document.attachEvent ) {
		document.attachEvent("mouseover", inspectorMouseOver);
		document.attachEvent("mouseout", inspectorMouseOut);
		document.attachEvent("click", inspectorOnClick);
		// document.attachEvent("keydown", inspectorCancel);
	}

})(document);

// $(document).one('ready',function(){
// 	// Perform something here...
// 	//alert("there you go");
// 	var htmltag = document.documentElement;
// 	console.log(htmltag);
// });

