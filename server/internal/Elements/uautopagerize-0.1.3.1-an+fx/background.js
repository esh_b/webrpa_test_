'use strict';

var userAgent = window.navigator.userAgent.toLowerCase();
var IS_EDGE = userAgent.includes('edge');
var IS_FIREFOX = userAgent.includes('firefox');
var IS_CHROME = userAgent.includes('chrome');
var IS_OPERA = userAgent.includes('opera');

var EXTEINSION_ID = chrome.extension.getURL('').split(/\/+/)[1];
var IS_DEVELOPER = EXTEINSION_ID !== 'kdplapeciagkkjoignnkfpbfkebcfbpb';
if (IS_FIREFOX) {
  IS_DEVELOPER = false;
}

var SITEINFO_IMPORT_URL = 'http://wedata.net/databases/AutoPagerize/items_all.json';
var default_settings = {
  CACHE_EXPIRE: 24 * 60 * 60 * 1000,
  XHR_TIMEOUT: 60 * 1000,
  DEBUG: false,
  FORCE_TARGET_WINDOW: true,
  BASE_REMAIN_HEIGHT: 400,
  FIX_LAZYLOAD: true,
  FIX_SCROLL_OVER: false,
  enable: true,
  includes: ['*'],
  excludes: [],
};

var icons = {
  enable: 'img/icon.svg',
  disable: 'img/icon.svg#disable',
  terminated: 'img/icon.svg#terminated',
  error: 'img/icon.svg#error',
  off: 'img/icon.svg#off',
  loading: 'img/loading.png',
};

function init() {
  chrome.storage.local.get(['settings', 'cacheDate'], function (items, areaName) {
    let settings = Object.assign({}, default_settings);
    if (items.settings) {
      Object.assign(settings, items.settings);
    }
    if (!IS_DEVELOPER)
      settings.DEBUG = false;
    chrome.storage.local.set({ settings });

    if (!items.cacheDate || new Date().getTime() - items.cacheDate > settings.CACHE_EXPIRE) {
      console.log('SITEINFO is old.');
      requestSITEINFO();
    }

    chrome.browserAction.setIcon({
      path: icons[settings.enable ? 'enable' : 'off']
    });
  });
}

// chrome.runtime.onStartup.addListener(function () {
//   init();
// });

chrome.runtime.onInstalled.addListener(function (details) {
  // chrome.notifications.clear('onInstalled');
  // chrome.notifications.create('onInstalled', {
  //   iconUrl: chrome.runtime.getURL('img/icon-48.png'),
  //   title: 'onInstalled',
  //   type: 'basic',
  //   message: [details.reason, details.previousVersion, details.id].join(' ')
  // });
  init();
  if (!IS_FIREFOX) {
    createContextMenus();
  }
});

// Firefox で有効→無効→有効にするとメニューが削除されるのでここで作り直せるようにする
// タイマー掛けないとメニュー作成に失敗する
if (IS_FIREFOX) {
  setTimeout(() => {
    createContextMenus();
  }, 500);
}

chrome.contextMenus.onClicked.addListener(function (info) {
  if (info.menuItemId === 'toggleEnable') {
    toggleEnable();
  }
  if (info.menuItemId === 'debugMode') {
    chrome.storage.local.get('settings', ({ settings }) => {
      settings.DEBUG = !settings.DEBUG;
      chrome.storage.local.set({ settings });
      chrome.contextMenus.update(info.menuItemId, {
        checked: settings.DEBUG
      });
      sendMessageTab({ message: 'debug_mode', settings });
    });
  }
  if (info.menuItemId === 'disableThisPage') {
    let queryInfo = {
      active: true,
      lastFocusedWindow: true
    };
    chrome.tabs.query(queryInfo, function (tabs) {
      let orig = new URL(tabs[0].url).origin + '/*';
      chrome.storage.local.get('settings', ({ settings }) => {
        if (!orig.startsWith('http') || settings.excludes.indexOf(orig) >= 0) return;
        settings.excludes.push(orig);
        chrome.storage.local.set({ settings });
        chrome.tabs.sendMessage(tabs[0].id, {
          message: 'update_icon',
          state: 'disable',
        });
      });
    });
  }
  if (info.menuItemId === 'editMySiteinfo') {
    chrome.tabs.create({
      url: chrome.extension.getURL('edit.html'),
    });
  }
  if (info.menuItemId === 'option') {
    chrome.runtime.openOptionsPage();
  }
});

chrome.browserAction.onClicked.addListener(function () {
  toggleEnable();
});

function createContextMenus() {
  chrome.storage.local.get('settings', ({ settings }) => {
    chrome.contextMenus.create({
      id: 'toggleEnable',
      title: chrome.i18n.getMessage('enable'),
      type: 'checkbox',
      checked: settings.enable,
      contexts: ['browser_action']
    });
    chrome.contextMenus.create({
      id: 'disableThisPage',
      title: chrome.i18n.getMessage('disableThisPage'),
      contexts: ['browser_action']
    });
    chrome.contextMenus.create({
      id: 'editMySiteinfo',
      title: chrome.i18n.getMessage('editMySiteinfo'),
      contexts: ['browser_action']
    });
    if (IS_FIREFOX) {
      chrome.contextMenus.create({
        id: 'option',
        title: chrome.i18n.getMessage('option'),
        contexts: ['browser_action']
      });
    }
    if (IS_DEVELOPER) {
      chrome.contextMenus.create({
        id: 'debugMode',
        title: chrome.i18n.getMessage('debugMode'),
        type: 'checkbox',
        checked: settings.DEBUG,
        contexts: ['browser_action']
      });
    }
  });
}

function toggleEnable() {
  chrome.storage.local.get('settings', ({ settings }) => {
    settings.enable = !settings.enable;
    chrome.storage.local.set({ settings });

    chrome.contextMenus.update('toggleEnable', {
      checked: settings.enable
    });
    chrome.browserAction.setIcon({
      path: icons[settings.enable ? 'enable' : 'off']
    });
    sendMessageTab({
      message: 'update_icon',
      state: settings.enable ? 'enable' : 'disable',
    });
  });
}

function sendMessageTab(message) {
  chrome.tabs.query({ active: true, lastFocusedWindow: true }, function (tabs) {
    chrome.tabs.sendMessage(tabs[0].id, message);
  });
}

function sendMessageTabAll(message) {
  chrome.tabs.query({}, function (tabs) {
    for (let tab of tabs) {
      chrome.tabs.sendMessage(tab, message);
    }
  });
}

function requestSITEINFO() {
  console.log('requestSITEINFO');
  GM_xmlhttpRequest({
    url: SITEINFO_IMPORT_URL,
    responseType: 'json',
    timeout: default_settings.XHR_TIMEOUT,
    onload(res) {
      getCacheCallback(res.target);
    },
    onerror() {
      getCacheErrorCallback();
    }
  });
}

function getCacheCallback(res) {
  console.log('getCacheCallback', 'status:' + res.status, 'url:' + res.responseURL, res);
  if (res.status != 200)
    return getCacheErrorCallback(res.responseURL);
  var temp = res.response;
  if (!temp || !temp.length)
    return getCacheErrorCallback(res.responseURL);

  let SITEINFO = parseResponseSITEINFO(temp);
  let SITEINFOs = [];
  let maxlen = Math.ceil(Math.sqrt(SITEINFO.length));
  let curlen = 30;
  while (SITEINFO.length) {
    let list = SITEINFO.splice(0, curlen);
    let url = list.map(({ url }) => url).join('|');
    SITEINFOs.push({ list, url });
    if (curlen < maxlen) {
      curlen += 5;
    }
  }
  chrome.storage.local.set({
    'SITEINFOs': SITEINFOs,
    'cacheDate': new Date().getTime()
  });
  console.log('saved siteinfo');
}

function parseResponseSITEINFO(obj) {
  if (typeof obj === 'string')
    obj = JSON.parse(obj);

  var info = [];
  obj.forEach(function (i) {
    var data = i.data;
    if (!data.url) return;
    try {
      var regexptext = new RegExp(data.url).test('http://example.com/');
      var o = {
        url: data.url,
        nextLink: data.nextLink,
        pageElement: data.pageElement,
      };
      if (data.insertBefore)
        o.insertBefore = data.insertBefore;
      // o.resource_url = i.resource_url;
      info.push(o);
    } catch (e) {
      console.error(e);
    }
  });
  info.sort((a, b) => b.url.length - a.url.length);
  return info;
}

function getCacheErrorCallback() {
  console.log('getCacheErrorCallback');
}

function GM_xmlhttpRequest(opt) {
  var req = new XMLHttpRequest();
  req.open(opt.method || 'get', opt.url, true);

  if (opt.headers && typeof opt.headers == 'object') {
    for (var i in opt.headers) {
      req.setRequestHeader(i, opt.headers[i]);
    }
  }

  for (var k of ['timeout', 'responseType', 'onload', 'onerror', 'onreadystatechange']) {
    if (opt[k])
      req[k] = opt[k];
  }

  if (opt.overrideMimeType)
    req.overrideMimeType(opt.overrideMimeType);

  req.send(opt.data || null);
  return req;
}
/*
if (IS_DEVELOPER) {
  chrome.storage.onChanged.addListener(function (changes) {
    if (changes.settings && changes.settings.newValue) {
      let settings = changes.settings.newValue;
      chrome.notifications.clear('debug_notifi', function(){});
      chrome.notifications.create('debug_notifi', {
        iconUrl: chrome.runtime.getURL('img/icon-48.png'),
        title: 'enable: ' + settings.enable,
        type: 'basic',
        message: 'excludes\n' + settings.excludes.join('\n'),
      }, function() {});
    }
  });
}
*/