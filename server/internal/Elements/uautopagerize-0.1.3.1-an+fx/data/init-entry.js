'use strict';
var settings = {
  CACHE_EXPIRE: 24 * 60 * 60 * 1000,
  XHR_TIMEOUT: 60 * 1000,
  DEBUG: false,
  FORCE_TARGET_WINDOW: true,
  BASE_REMAIN_HEIGHT: 400,
  FIX_LAZYLOAD: true,
  FIX_SCROLL_OVER: false,
  enable: true,
};

var apfilter = {
  filters: [],
  documentFilters: [], // function(htmlDoc, requestURL, info){}
  requestFilters: [], // function(opt){}
  responseFilters: [], // function(res, requestURL){}
  // uAutoPagerize original
  fragmentFilters: [], // function(fragment, htmlDoc, page){}
  nextLinkFilters: [], // function(nextLink, baseURL){ return nextLink.href }
};

var ap = null;
var storage_loaded = false;
var siteinfos = [];
var mysiteinfo = [];
var microformat = [{
  url: '^https?://.*',
  nextLink: '//a[@rel="next"] | //link[@rel="next"]',
  pageElement: '//*[contains(@class, "autopagerize_page_element")]',
  insertBefore: '//*[contains(@class, "autopagerize_insert_before")]',
}];
var eventinfo = [];
var miscellaneous = [];

var URLREGEXP = {};

function URLChecker(list, url) {
  for (let urlstr of list) {
    let exp = URLREGEXP[urlstr] || (URLREGEXP[urlstr] = wildcardToRegExp(urlstr));
    if (exp.test(url))
      return true;
  }
}

function wildcardToRegExp(urlstr) {
  if (urlstr.source) return urlstr;
  var r = /^\/(.*?)\/([gimy])?$/;
  var reg = urlstr.match(r);
  if (reg) {
    return new RegExp(reg[1], reg[2]);
  } else {
    reg = urlstr.replace(/[()\[\]{}|+.,^$?\\]/g, '\\$&').replace(/\*+/g, function (str) {
      return str === '*' ? '.*' : '[^/]*';
    });
    return new RegExp('^' + reg + '$');
  }
}


document.addEventListener('AutoPagerize_launchAutoPager', function (event) {
  if (!settings.enable) return;
  // とりあえずイベントからの起動は excludes を無視する
  // if (!URLChecker(settings.includes, location.href)) return;
  // if (URLChecker(settings.excludes, location.href)) return;
  if (!event.detail) {
    console.log('event.detail が取得できませんでした');
    return;
  }
  let list = event.detail.siteinfo || event.detail.SITEINFO;
  if (list && Array.isArray(list)) {
    list.forEach(info => info.eventinfo = true);
    Array.prototype.unshift.apply(eventinfo, list);

    // 既にSITEINFOsをチェック済みならここで実行。まだなら GM_AutoPagerizeLoaded で実行される。
    if (storage_loaded) {
      if (event.detail.rebuild && ap) {
        ap.destroy(true);
        ap = null;
        setTimeout(() => {
          launchAutoPager(list);
        }, 500);
      } else {
        launchAutoPager(list);
      }
    }
  }
}, false);


document.addEventListener('GM_AutoPagerizeLoaded', event => {
  if (/\bgoogle\.(?:com|co\.jp)$/.test(location.host)) {
    // Google Search in thumbnail
    apfilter.fragmentFilters.push(function (df) {
      Array.from(df.querySelectorAll('div.rg_meta'), elem => {
        elem.style.setProperty('display', 'none', 'important');
        let prev = elem.previousElementSibling;
        prev.style.setProperty('overflow', 'hidden', '');
        prev.style.setProperty('position', 'relative', '');
        let pare = elem.parentNode;
        pare.style.setProperty('display', 'inline-block');
      });
    });

    // Google Video
    let ids = [];
    let datas = [];
    let docFil = function (newDoc) {
      var x = getFirstElementByXPath('//script/text()[contains(self::text(), "data:image/jpeg") and contains(self::text(), "vidthumb")]', newDoc);
      if (x) {
        [].push.apply(ids, x.nodeValue.match(/vidthumb\d+/g) || []);
        [].push.apply(datas, x.nodeValue.match(/data:image\/jpeg\;base64\,[A-Za-z0-9/+]+(?:\\x3d)*/g) || []);
      }
      var x = getFirstElementByXPath('//script/text()[contains(self::text(), "data:image/jpeg") and contains(self::text(), "uid_")]', newDoc);
      if (x) {
        [].push.apply(ids, x.nodeValue.match(/uid_\d+/g) || []);
        [].push.apply(datas, x.nodeValue.match(/data:image\/jpeg\;base64\,[A-Za-z0-9/+]+(?:\\x3d)*/g) || []);
      }
    };
    let dfrFil = function (df) {
      datas.forEach(function (d, i) {
        var elem = df.getElementById(ids[i]);
        if (!elem) return;
        elem.src = d.replace(/\\x3d/g, '=');
      });
      ids = [];
      datas = [];
    };
    apfilter.documentFilters.push(docFil);
    apfilter.fragmentFilters.push(dfrFil);
  } else if (location.origin === 'http://eow.alc.co.jp') {
    // alc from oAutoPagerize
    let info = {
      url: 'http://eow\\.alc\\.co\\.jp/[^/]+',
      nextLink: '//p[@id="paging"]/span[last()]/a',
      pageElement: 'id("resultsList")/ul',
      exampleUrl: 'http://eow.alc.co.jp/search?q=%E3%81%82%E3%82%8C http://eow.alc.co.jp/are'
    };
    apfilter.nextLinkFilters.push(function (a, _url) {
      let word = _url.indexOf('search?') >= 0 ?
        _url.match(/[?&]q=([^&]+)/) :
        _url.match(/eow\.alc\.co\.jp\/([^/]+)/);
      if (!word || !word[1]) return;
      return a.href.replace(/javascript:goPage\("(\d+)"\)/, 'http://eow.alc.co.jp/search?q=' + word[1] + '&pg=$1');
    });
  } else if (location.host === 'matome.naver.jp') {
    let info = {
      url: '^https?://matome\\.naver\\.jp',
      nextLink: '//div[contains(@class, "MdPagination0")]/strong[1]/following-sibling::a[1]|//a[@class="mdMTMEnd01Pagination01Next"]',
      pageElement: '//div[contains(concat(" ", @class, " "), " MdMTMWidgetList01 ")]|//ul[@class="MdMTMTtlList02" or @class="MdTopMTMList01" or @class="MdMTMTtlList03"]|//div[contains(concat(" ", normalize-space(@class), " "), " ArColWrap ") and contains(concat(" ", normalize-space(@class), " "), " MdCF ")]',
      exampleUrl: 'https://matome.naver.jp/odai/2143478691035538101 http://matome.naver.jp/topic/1LwZ0 https://matome.naver.jp/odai/2140350782495631701/2141082535893290103'
    };
    mysiteinfo.push(info);
    apfilter.nextLinkFilters.push(function (next, baseURL) {
      if (next.className === 'mdMTMEnd01Pagination01Next') {
        return next.href;
      } else {
        let urlo = new URL(baseURL);
        urlo.searchParams.set('page', next.textContent);
        return urlo.href;
      }
    });
  } else if (location.href.startsWith('https://news.yahoo.co.jp/pickup/')) {
    let info = {
      name: 'Yahoo!ニュース',
      url: '^https://news\\.yahoo\\.co\\.jp/pickup/',
      nextLink: '//a[@class="newsLink"][text()="[続きを読む]"]',
      pageElement: '//div[@class="articleMain"]/div[@class="paragraph"]|//div[contains(concat(" ",normalize-space(@class)," "), " headline ")]',
    };
    mysiteinfo.push(info);
  } else if (location.origin === 'https://search.yahoo.co.jp') {
    apfilter.nextLinkFilters.push(function (nextLink, baseURL) {
      let { href, origin } = nextLink;
      if ((origin === 'http://search.yahoo.co.jp' || origin === 'http://wrs.search.yahoo.co.jp') && href.includes('%26b=')) {
        return decodeURIComponent(href).split('/**')[1];
      }
    });
  }

  chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
    if (request.settings) {
      Object.keys(request.settings).map(key => settings[key] = request.settings[key]);
    }
    if (request.message === 'update_icon' && ap) {
      if (request.state) {
        ap.setState(request.state);
      } else {
        ap.stateToggle();
      }
    }
  });

  chrome.storage.local.get(['settings', 'SITEINFOs', 'MICROFORMAT', 'MY_SITEINFO'], function (items, areaName) {
    storage_loaded = true;
    if (items.settings) {
      Object.keys(items.settings).map(key => settings[key] = items.settings[key]);
    }
    if (items.SITEINFOs && items.SITEINFOs.length) {
      siteinfos = items.SITEINFOs;
    }
    if (items.MY_SITEINFO && items.MY_SITEINFO.length) {
      Array.prototype.push.apply(mysiteinfo, items.MY_SITEINFO);
    }
    if (mysiteinfo.length) {
      siteinfos.unshift({
        list: mysiteinfo,
        url: mysiteinfo.map(({ url }) => url).join('|')
      });
    }
    if (items.MICROFORMAT && items.MICROFORMAT.length) {
      Array.prototype.push.apply(microformat, items.MICROFORMAT);
    }
    if (microformat.length) {
      siteinfos.push({
        list: microformat,
        url: microformat.map(({ url }) => url).join('|')
      });
    }

    if (!settings.enable) return;
    if (eventinfo.length) {
      let ret = launchAutoPager(eventinfo);
      if (ret) return;
    }
    if (!URLChecker(settings.includes, location.href)) return;
    if (URLChecker(settings.excludes, location.href)) return;

    miscellaneous.forEach(func => func(document, location.href));
    siteinfos.some(({ list, url }) => {
      if (location.href.match(url)) {
        return !!launchAutoPager(list);
      }
    });
  });

}, { once: true });
