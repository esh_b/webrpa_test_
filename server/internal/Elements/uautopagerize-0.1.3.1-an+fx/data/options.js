'use strict';

var settings = {};
chrome.storage.local.get('settings', function (changes, areaName) {
  if (changes.settings) {
    settings = changes.settings;
  }
  $(document).ready(onReady);
});

function setLocale(elem) {
  let text = chrome.i18n.getMessage(elem.dataset.i18n);
  if (text) {
    elem.innerHTML = text;
  }
}

function setLocaleTitle(elem) {
  let text = chrome.i18n.getMessage(elem.dataset.i18nTitle);
  if (text) {
    elem.title = text;
  }
}

function setLocaleAll() {
  Array.from(document.querySelectorAll('[data-i18n]'), setLocale);
  Array.from(document.querySelectorAll('[data-i18n-title]'), setLocaleTitle);
}

function onReady() {
  setLocaleAll();

  if (settings.excludes && Array.isArray(settings.excludes))
    $('#excludes').val(settings.excludes.join('\n'));
  if (settings.includes && Array.isArray(settings.includes))
    $('#includes').val(settings.includes.join('\n'));

  $('#FORCE_TARGET_WINDOW').prop('checked', settings.FORCE_TARGET_WINDOW);
  $('#BASE_REMAIN_HEIGHT').val(settings.BASE_REMAIN_HEIGHT);
  $('#FIX_SCROLL_OVER').prop('checked', settings.FIX_SCROLL_OVER);

  $('#save-excludes').on('click', function () {
    let list = $('#excludes').val().trim().split(/\s+/)
      .filter((e, i, a) => a.indexOf(e) === i);
    if (list[0] === '')
      list = [];
    settings.excludes = list;
    console.log('saved excludes pages', list);
    saveSettings();
  });

  $('#save-excludes').on('click', function () {
    let list = $('#includes').val().trim().split(/\s+/)
      .filter((e, i, a) => a.indexOf(e) === i);
    if (list[0] === '')
      list = ['*'];
    settings.includes = list;
    console.log('saved excludes pages', list);
    saveSettings();
  });
  $('#FORCE_TARGET_WINDOW').on('change', function () {
    settings.FORCE_TARGET_WINDOW = this.checked;
  });
  $('#FIX_SCROLL_OVER').on('change', function () {
    settings.FIX_SCROLL_OVER = this.checked;
  });
  $('#BASE_REMAIN_HEIGHT').on('change', function () {
    let num = this.valueAsNumber;
    if (isNaN(num))
      this.value = num = 400;
    settings.BASE_REMAIN_HEIGHT = this.valueAsNumber;
  });
  $('#save-etc').on('click', function () {
    saveSettings();
  });
}

function saveSettings() {
  chrome.storage.local.set({
    settings: settings
  });
}
