"use strict";
(function(document) {

    history.pushState(null, null, location.href);
    window.onpopstate = function () {
        history.go(1);
    };

    // let login_url = null;
    var before_login_flag = 0;
    var csrftoken = getCookie('csrftoken');
    var login_info = {
        "login_url": "",
        "action_list": []
    }

    var page_info = {
        "current_page_url": "",
        "pagination": [],
        "num_pages": 2,
        "action_list": []
    };
    var procedure = {
        "start_url": "",
        "product_url": {},
        "project_type": 1,
        "project_name": "",
        "login_details": [],
        "page_list": []
    };
    
    var pointer = 0;
    var col_flag = 0;
    var state_node = {
        "loginTab": -1,
        "currentTab": -1,
        "action_list": [] 
    }

    var state_machine = [];
    var extraction_flag = 0;
    var total_columns = 0;

    function clear_state(flag){
        // console.log("Inside clear_state", state_machine.length, pointer, flag);
        if (flag != 2){
            state_node["action_list"] = [];
        }
        if (state_machine.length > pointer){
            let loop = state_machine.length-pointer;
            for (let i = 0; i < loop; i++) {
                state_machine.pop();
                if(flag == 0){      // Login only   
                    login_info["action_list"].pop();
                }
                else if (flag == 1 || flag == 2){
                    page_info["action_list"].pop();
                }
            }
        }
    }

    function clear_action_state(step){
        // console.log("Wow clear_action_state(state, pointer, step): ", state_machine.length, pointer, step);
        state_node["action_list"] = [];
        state_node["action_list"].push(state_machine[pointer]["action_list"][0]);
        // state_node["action_list"].push({"action_type": state_machine[pointer]["action_list"][0]["action_type"], "path": state_machine[pointer]["action_list"][0]["path"], "similar_path": state_machine[pointer]["action_list"][0]["similar_path"], "column_name": state_machine[pointer]["action_list"][0]["column_name"], "fill_value": state_machine[pointer]["action_list"][0]["fill_value"], "row_column": [], "column_list": state_machine[pointer]["action_list"][0]["column_list"]});
        // state_node["action_list"].push(page_info["action_list"].pop());
        
        if (state_node["action_list"].length > 0) { 
            switch(step){
                case 0:
                    // Do nothing
                    break;
                case 1:     // Action_type
                    state_node["action_list"][0]["action_type"] = 0;
                    state_node["action_list"][0]["path"] = [];
                    state_node["action_list"][0]["similar_path"] = [];
                    // state_node["action_list"][0]["column_list"] = [];
                    state_node["action_list"][0]["column_name"] = "";
                    break;
                case 2:     // Path1
                    state_node["action_list"][0]["path"] = [];
                    state_node["action_list"][0]["similar_path"] = [];
                    // state_node["action_list"][0]["column_list"] = [];
                    state_node["action_list"][0]["column_name"] = "";
                    break;
                case 3:
                    state_node["action_list"][0]["similar_path"] = [];
                    state_node["action_list"][0]["column_list"] = [];
                    state_node["action_list"][0]["column_name"] = "";
                    break;
                case 4:
                    state_node["action_list"][0]["column_name"] = "";
                    break;  
                default:
                    console.log("Sriram-san error!!");
            }
        }
    }

    function getCookie(name) {
        let cookieValue = null;
        if (document.cookie && document.cookie != '') {
            let cookies = document.cookie.split(';');
            for (let i = 0; i < cookies.length; i++) {
                let cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

    function bindEvent(element, eventName, eventHandler) {      // addEventListener support for IE8
        if (element){
            if (element.addEventListener){
                element.addEventListener(eventName, eventHandler, false);
            } else if (element.attachEvent) {
                element.attachEvent('on' + eventName, eventHandler);
            }
        }
    }

    function startJS(){
        var login_url = document.getElementById('login_url').value;
        // var login_url = null;
        var url_val = document.getElementById('start_url').value;
        procedure["start_url"] = url_val;
        procedure["project_type"] = 1;
        page_info["current_page_url"] = url_val;
        if (login_url && login_url != '' && login_url != null){
            injectJS(login_url, 0);
            loginPanel();
        }
        else {
            injectJS(url_val, 1);
            sidePanel();
        }

        function injectJS(url, flag) {
            let iframe = document.getElementById('iframe');
            var sendMessage = function(msg) {                       // Send a message to the child iframe
                iframe.contentWindow.postMessage(msg, '*');
            };

            bindEvent(window, 'message', function (e) {
                let data = e.data;
                if (data['iframe_content2'] != undefined){      // Preview Table Input
                    let data_type = data['iframe_content2'];
                    if(data_type['loaded'] != undefined){
                        if(flag == 1){
                            let action_extract = document.getElementById("action_extract");
                            let action_fc = document.getElementById("action_fc");
                            action_extract.disabled = false;
                            action_fc.disabled = false;
                            sendMessage({'action_data': {"login_type": 0, 'csrftoken': csrftoken}});
                        } else{
                            sendMessage({'action_data': {"login_type": 1, 'csrftoken': csrftoken}});
                            sendMessage({'action_data': {"action_type": 14, 'csrftoken': csrftoken}});
                        }    
                    }
                }
            });
            
            $.ajax({
                url : "/saas/content",
                type : "POST",
                csrfmiddlewaretoken: csrftoken,
                beforeSend : function(jqXHR, settings) {
                    jqXHR.setRequestHeader("x-csrftoken", csrftoken);
                },
                contentType: 'application/json;charset=UTF-8',
                data : JSON.stringify({'url': url}),
                success: function(data) {
                    let frameDoc = iframe.contentDocument || iframe.contentWindow.document;
                    frameDoc.write(data);
                    document.getElementById("load_bar").style.display = 'none';
                    // document.getElementById("back_button").disabled = false;
                },
                error: function(error){
                    console.log(error);
                }
            });
        }

        function loginPanel() {
            let iframe = document.getElementById('iframe');
            var sendMessage = function(msg) {                       // Send a message to the child iframe
                iframe.contentWindow.postMessage(msg, '*');
            };
            var public_key = "";
            $.ajax({
                url: "/api/v1/scraping/get_public_key",
                type : "POST",
                csrfmiddlewaretoken: csrftoken,
                beforeSend : function(jqXHR, settings) {
                    jqXHR.setRequestHeader("x-csrftoken", csrftoken);
                },
                contentType: 'application/json;charset=UTF-8',
                success: function (data) {
                    public_key = public_key + data;
                },
                error: function(data) {
                    console.log("Get Public Key went wrong!!");
                }
            });
            var action_type = 14;
            var loginTab = -1;                                  // Current tab is set to be the first tab (0)
            showLoginTab(0);                                    // Display the current tab
            
            function showLoginTab(next){
                let x = document.getElementsByClassName("loginStep");
                let y = document.getElementsByClassName("loginConfig");
                
                if(loginTab >= 0){
                    x[loginTab].style.display = "none";
                    y[loginTab].style.display = "none";
                }
                if(next >= 0){
                    x[next].style.display = "block";
                    y[next].style.display = "block";
                }
                loginTab = next;
            }

            // bindEvent(action_login_confirm, 'click', function (e) {
            //     sendMessage({'action_data': {"action_login_confirm": action_type, 'csrftoken': csrftoken}});
            //     showLoginTab(1);
            // });
            bindEvent(action_login_next, 'click', function (e) {
                // Register the fill-in action, loginTab = 0
                let action_login_text = document.getElementById('action_login_text');
                sendMessage({'action_data': {"public_key": public_key, "action_login_next": action_login_text.value, 'csrftoken': csrftoken}});
                state_node["loginTab"] = loginTab;
                showLoginTab(2);

                action_login_text.value = "";
                action_login_text.disabled = true;
                action_login_next.disabled = true;
                action_type = 14;
            });
            // bindEvent(action_password_confirm, 'click', function (e) {
            //     sendMessage({'action_data': {"action_password_confirm": action_type, 'csrftoken': csrftoken}});
            //     showLoginTab(3);
            // });
            bindEvent(action_password_next, 'click', function (e) {
                // Register the fill-in action, loginTab = 2
                let action_password_text = document.getElementById('action_password_text');
                sendMessage({'action_data': {"public_key": public_key, "action_password_next": action_password_text.value, 'csrftoken': csrftoken}});
                state_node["loginTab"] = loginTab;
                showLoginTab(4);

                action_password_text.value = "";
                action_password_text.disabled = true;
                action_password_next.disabled = true;
                action_type = 13;
            });
            bindEvent(action_password_next2, 'click', function (e) {
                // Register the fill-in action
                let action_password_text2 = document.getElementById('action_password_text2');
                sendMessage({'action_data': {"public_key": public_key, "action_password_next": action_password_text2.value, 'csrftoken': csrftoken}});
                showLoginTab(4);
                action_password_text2.value = "";
                action_type = 13;
            });
            bindEvent(finish_login, 'click', function (e) {
                // Register click action
                sendMessage({'action_data': {"finish_login": action_type, 'csrftoken': csrftoken}});
                // Get the last action
                showLoginTab(-1);
                loginTab = -1;
                sidePanel();
                document.getElementById("load_bar").style.display = 'block';
            });
            
            bindEvent(action_nextpage_password, 'click', function (e) {
                action_type = 13;
                sendMessage({'action_data': {"action_type": action_type, 'csrftoken': csrftoken}});
                sendMessage({'action_data': {"action_nextpage_password": 1, 'csrftoken': csrftoken}});
                showLoginTab(5);
            });
            bindEvent(password_click_confirm, 'click', function (e) {
                sendMessage({'action_data': {"password_click_confirm": action_type, 'csrftoken': csrftoken}});
                document.getElementById("load_bar").style.display = 'block';
                showLoginTab(6);
                action_type = 14;
                // document.getElementById("load_bar").style.display = 'block';
            });
            // bindEvent(action_simple_password, 'click', function (e) {
            //     sendMessage({'action_data': {"action_simple_password": action_type, 'csrftoken': csrftoken}});
            //     showLoginTab(3);
            // });
            bindEvent(back_button, 'click', function (e) {
                if(loginTab != -1){
                    // -1 <- 0 (Big code)
                    // 0 <- 2
                    // 2 <- 4
                    // 2 <- 5
                    // 5 <- 6
                    // 6 <- 4
                    switch(loginTab) {
                        case 0:
                            showLoginTab(-1);
                            
                            document.getElementById("start_div1_login2").style.display = 'block';
                            document.getElementById("start_div2_login2").style.display = 'block';
                            document.getElementById("login_url").value = '';
                            document.getElementById("submit2_url").disabled = true;
                            before_login_flag = 2;

                            // document.getElementById("start_div1").style.display = 'block';
                            // document.getElementById("start_div2").style.display = 'block';
                            // document.getElementById("start_url").value = "";
                            // document.getElementById("login_url").value = "";
                            // document.getElementById("is_login").checked = false;
                            // $("#hidden_fields").hide();
                            // $('#login_url').prop('required', false);
                            let frameDoc = iframe.contentDocument || iframe.contentWindow.document;
                            frameDoc.body.innerHTML = '';
                            break;
                        case 2:
                            pointer = pointer -1;
                            sendMessage({'back_action_login': {'login_fill': state_machine[pointer]}});
                            action_type = 14;
                            showLoginTab(0);
                            break;
                        case 5:
                            sendMessage({'back_action_login': {'action_type': 14}});
                            action_type = 14;
                            showLoginTab(2);
                            break;
                        case 6:
                            pointer = pointer -1;
                            showLoginTab(5);
                            action_type = 13;

                            $.ajax({
                                url : "/saas/getLogin",
                                type : "POST",
                                csrfmiddlewaretoken: csrftoken,
                                beforeSend : function(jqXHR, settings) {
                                    jqXHR.setRequestHeader("x-csrftoken", csrftoken);
                                },
                                contentType: 'application/json;charset=UTF-8',
                                data : JSON.stringify({"start_url": procedure['start_url'], 'login_info': [{"login_url": login_info["login_url"], "action_list": []}], 'flag': 1}),
                                success: function(data) {
                                    sendMessage({'back_action_login': {'login_click': state_machine[pointer-1]}});
                                    iframe.contentDocument.body.innerHTML = data;
                                    clear_state(0);
                                    document.getElementById("load_bar").style.display = 'none';
                                },
                                error: function(error){
                                    console.log(error);
                                }
                            });
                            break;
                        case 4:
                            pointer = pointer -1;
                            if (pointer == 1){
                                sendMessage({'back_action_login': {'login_fill': state_machine[pointer]}});
                                clear_state(0);
                                action_type = 14;
                                showLoginTab(2);
                            }
                            else if (pointer == 2){
                                sendMessage({'back_action_login': {'login_fill': state_machine[pointer]}});
                                clear_state(0);
                                action_type = 14;
                                showLoginTab(6);
                            }
                            else{
                                console.log("Error in pointer!!");
                            }
                            break;
                        default:
                            console.log("Ishikawa-san error!!")
                    }
                }
            });
            // bindEvent(forward_button, 'click', function (e) {
            //  pointer = pointer + 1;
            //  if(loginTab != -1){

            //  }
            // });

            bindEvent(window, 'message', function (e) {
                let data = e.data;
                if (data['iframe_content2'] != undefined){      // Preview Table Input
                    let data_type = data['iframe_content2'];
                    if(data_type['4'] != undefined){        // Login Fill-Click
                        if(action_type == 14){
                            if(loginTab == 0){
                                // let action_login_confirm = document.getElementById("action_login_confirm");
                                // action_login_confirm.disabled = false;
                                let action_login_text = document.getElementById("action_login_text");
                                action_login_text.disabled = false;
                            }
                            else if(loginTab == 2){
                                // let action_password_confirm = document.getElementById("action_password_confirm");
                                // action_password_confirm.disabled = false;
                                let action_password_text = document.getElementById("action_password_text");
                                action_password_text.disabled = false;
                            }
                            else if(loginTab == 6){
                                // let action_simple_password = document.getElementById("action_simple_password");
                                // action_simple_password.disabled = false;
                                let action_password_text2 = document.getElementById("action_password_text2");
                                action_password_text2.disabled = false;
                            }
                        }
                        else if(action_type == 13){
                            if(loginTab == 5){
                                let password_click_confirm = document.getElementById("password_click_confirm");
                                password_click_confirm.disabled = false;
                            }
                            else if(loginTab == 4){
                                let finish_login = document.getElementById("finish_login");
                                finish_login.disabled = false;
                            }
                        }
                    }
                    else if(data_type['5'] != undefined){     // Push Action
                        let data_store = data_type['5'];
                        if (data_store["login_action"] != undefined){
                            clear_state(0);
                            login_info["action_list"].push(data_store["login_action"]);
                            state_node["action_list"].push(data_store["login_action"]);
                            state_machine.push({"loginTab": state_node["loginTab"], "currentTab": state_node["currentTab"], "action_list": state_node["action_list"]});
                            pointer += 1;
                        }
                        else if(data_store["login_finish"] != undefined){
                            let action_extract = document.getElementById("action_extract");
                            let action_fc = document.getElementById("action_fc");
                            
                            clear_state(0);
                            state_node["action_list"].push(data_store["login_finish"]);
                            state_machine.push({"loginTab": state_node["loginTab"], "currentTab": state_node["currentTab"], "action_list": state_node["action_list"]});
                            pointer += 1;
                            
                            login_info["action_list"].push(data_store["login_finish"]);
                            login_info["login_url"] = login_url;
                            procedure["login_details"].push(login_info);
                            // Login_info is a dictionary. Convert into JSON and send it to the server.
                            let frame = document.getElementById('iframe');
                            $.ajax({
                                url : "/saas/getLogin",
                                type : "POST",
                                csrfmiddlewaretoken: csrftoken,
                                beforeSend : function(jqXHR, settings) {
                                    jqXHR.setRequestHeader("x-csrftoken", csrftoken);
                                },
                                contentType: 'application/json;charset=UTF-8',
                                data : JSON.stringify({"start_url": procedure['start_url'], 'login_info': procedure["login_details"], 'flag': 2}),
                                success: function(data) {
                                    frame.contentDocument.body.innerHTML = data;
                                    action_extract.disabled = false;
                                    action_fc.disabled = false;
                                    document.getElementById("load_bar").style.display = 'none';
                                    // document.getElementById("back_button").disabled = true;
                                    // frame.contentDocument.write(data);
                                },
                                error: function(error){
                                    console.log(error);
                                }
                            });
                        }
                        else if(data_store["login_second_click"] != undefined){
                            clear_state(0);
                            login_info["action_list"].push(data_store["login_second_click"]);
                            login_info["login_url"] = login_url;
                            state_node["action_list"].push(data_store["login_second_click"]);
                            state_machine.push({"loginTab": state_node["loginTab"], "currentTab": state_node["currentTab"], "action_list": state_node["action_list"]});
                            pointer += 1;
                            procedure["login_details"].push(login_info);
                            // Login_info is a dictionary. Convert into JSON and send it to the server.
                            let frame = document.getElementById('iframe');

                            $.ajax({
                                url : "/saas/getLogin",
                                type : "POST",
                                csrfmiddlewaretoken: csrftoken,
                                beforeSend : function(jqXHR, settings) {
                                    jqXHR.setRequestHeader("x-csrftoken", csrftoken);
                                },
                                contentType: 'application/json;charset=UTF-8',
                                data : JSON.stringify({"start_url": procedure['start_url'], 'login_info': procedure["login_details"], 'flag': 1}),
                                success: function(data) {
                                    procedure["login_details"].pop();
                                    frame.contentDocument.body.innerHTML = data;
                                    document.getElementById("load_bar").style.display = 'none';
                                },
                                error: function(error){
                                    procedure["login_details"].pop();
                                    console.log(error);
                                }
                            });
                        }
                    }
                }
            });
        }

        function sidePanel() {
            let iframe = document.getElementById('iframe');
            
            var sendMessage = function(msg) {                       // Send a message to the child iframe
                iframe.contentWindow.postMessage(msg, '*');
            };
            var action_type = 0;

            var currentTab = -1;                                    // Current tab is set to be the first tab (0)
            var tabledata = [];
            var maxrow = 0;
            var list_column = [];

            showTab(0);                                             // Display the current tab
            
            function showTab(next){
                let x = document.getElementsByClassName("headStep");
                let y = document.getElementsByClassName("projectStep");
                if(currentTab >= 0){
                    x[currentTab].style.display = "none";
                    y[currentTab].style.display = "none";
                }
                if(next >= 0){
                    x[next].style.display = "block";
                    y[next].style.display = "block";
                }
                currentTab = next;
            }
            
            var currentStep = 0
            function showStep(next){
                let element = document.getElementsByClassName("statusStep");
                if(currentStep >= 0){
                    element[currentStep].classList.remove('active');
                }
                if(next >= 0){
                    element[next].classList.add('active');
                    currentStep = next;
                }
            }

            // function refAction(ref_node) {
            //     return {"action_type": ref_node["action_type"], "path": ref_node["path"], "similar_path": ref_node["similar_path"], "column_name": ref_node["column_name"], "fill_value": ref_node["fill_value"], "row_column": ref_node["row_column"], "column_list": ref_node["column_list"]};
            // }

            sendMessage({'action_data': {"login_type": 0, 'csrftoken': csrftoken}});
            bindEvent(action_fc, 'click', function (e) {
                state_node["currentTab"] = currentTab; 
                showTab(12);
                document.getElementById("back_button").disabled = false;
                action_type = 14;
                extraction_flag = 0;
                sendMessage({'action_data': {"action_type": action_type, 'csrftoken': csrftoken}});
            });
            bindEvent(action_fill, 'click', function (e) {
                action_type = 14;
                sendMessage({'action_data': {"action_type": action_type, 'csrftoken': csrftoken}});
                showTab(3);
            });
            bindEvent(fill_confirm, 'click', function (e) {
                sendMessage({'action_data': {"fill_confirm": action_type, 'csrftoken': csrftoken}});
                showTab(12);
            });
            bindEvent(action_fill_next, 'click', function (e) { 
                let action_fill_text = document.getElementById('action_fill_text');
                state_node["currentTab"] = currentTab; 
                sendMessage({'action_data': {"action_fill_next": action_fill_text.value, 'csrftoken': csrftoken}});
                showTab(4);
                action_fill_text.value = "";
                action_fill_text.disabled = true;
                action_fill_next.disabled = true;
                extraction_flag = 0;
            });
            bindEvent(action_click, 'click', function (e) {
                action_type = 13;
                sendMessage({'action_data': {"action_type": action_type, 'csrftoken': csrftoken}});
                showTab(4);
            }); 
            bindEvent(click_confirm, 'click', function (e) {
                state_node["currentTab"] = currentTab; 
                sendMessage({'action_data': {"click_confirm": action_type, 'csrftoken': csrftoken}});
                showTab(2);
                extraction_flag = 0;
                document.getElementById("load_bar").style.display = 'block';
            });

            // Data Extraction
            bindEvent(action_extract, 'click', function (e) {
                showTab(2);
                showStep(1);
                document.getElementById("back_button").disabled = false;
                extraction_flag = 1;
                state_node["action_list"] = [];
                state_node["action_list"].push({"action_type": 0, "path": [], "similar_path": [], "column_name": "", "fill_value": "", "row_column": [], "column_list": []});
            });

            // Text
            bindEvent(action_text, 'click', function (e) {
                action_type = 1;
                sendMessage({'action_data': {"action_type": action_type, 'csrftoken': csrftoken}});
                showTab(5);
                if (state_machine.length > pointer){
                    clear_action_state(1);
                }
                state_node["action_list"][0]["action_type"] = action_type;
            });
            // URL
            bindEvent(action_url, 'click', function (e) {
                action_type = 2;
                if (state_machine.length > pointer){
                    clear_action_state(1);
                }
                state_node["action_list"][0]["action_type"] = action_type;
                sendMessage({'action_data': {"action_type": action_type, 'csrftoken': csrftoken}});
                showTab(5);
            });
            // IMG
            // bindEvent(action_img_url, 'click', function (e) {
            //  action_type = 3;
            //  sendMessage({'action_data': {"action_type": action_type, 'csrftoken': csrftoken}});
            //  showTab(5);
            // });
            
            bindEvent(simple_confirm, 'click', function (e) {
                sendMessage({'action_data': {"simple_confirm": action_type, 'csrftoken': csrftoken}});
                showTab(6);
                simple_confirm.disabled = true;
            });
            bindEvent(similar_yes, 'click', function (e) {
                showTab(7);
                if (state_machine.length > pointer){
                    clear_action_state(0);
                }
                if(action_type > 0 && action_type < 4){
                    action_type = action_type + 3;
                }
                state_node["action_list"][0]["action_type"] = action_type;
                sendMessage({'action_data': {"similar": 1, 'csrftoken': csrftoken}});
                similar_yes.disabled = true;
            });
            bindEvent(similar_confirm, 'click', function (e) {
                sendMessage({'action_data': {"similar_confirm": action_type, 'csrftoken': csrftoken}});
                showTab(8);
                similar_confirm.disabled = true;
            });
            bindEvent(similar_no, 'click', function (e) {
                showTab(8);
                if (state_machine.length > pointer){
                    clear_action_state(3);
                }
                if(action_type > 3 && action_type < 7){
                    action_type = action_type - 3;
                }
                state_node["action_list"][0]["action_type"] = action_type;
                sendMessage({'action_data': {"similar": 0, 'csrftoken': csrftoken}});
                similar_yes.disabled = true;
            });

            // Table
            bindEvent(action_table, 'click', function (e) {
                action_type = 7;
                sendMessage({'action_data': {"action_type": action_type, 'csrftoken': csrftoken}});
                showTab(13);
                if (state_machine.length > pointer){
                    clear_action_state(1);
                }
                state_node["action_list"][0]["action_type"] = action_type;
            });
            bindEvent(action_table_confirm, 'click', function (e) {
                action_type = 7;
                if (state_machine.length > pointer){
                    clear_action_state(2);
                }
                state_node["action_list"][0]["action_type"] = action_type;
                sendMessage({'action_data': {"action_table_confirm": 1, 'csrftoken': csrftoken}});
                showTab(14);
                action_table_confirm.disabled = true;
            });
            bindEvent(action_full_confirm, 'click', function (e) {
                action_type = 7;
                sendMessage({'action_data': {"action_type": action_type, 'csrftoken': csrftoken}});
                if (state_machine.length > pointer){
                    clear_action_state(3);
                }
                state_node["action_list"][0]["action_type"] = action_type;
                sendMessage({'action_data': {"action_full_confirm": 1, 'csrftoken': csrftoken}});
                showTab(9);
            });
            bindEvent(action_col_confirm, 'click', function (e) {
                action_type = 8;
                sendMessage({'action_data': {"action_type": action_type, 'csrftoken': csrftoken}});
                if (state_machine.length > pointer){
                    clear_action_state(3);
                }
                state_node["action_list"][0]["action_type"] = action_type;
                sendMessage({'action_data': {"action_col_confirm": 2, 'csrftoken': csrftoken}});
                document.getElementById("action_add_col").disabled = true;
                showTab(15);
            });
            bindEvent(action_add_col, 'click', function (e) {
                set_color(tabledata, 5);    // Removes next col.(Makes it white)            
                sendMessage({'action_data': {"action_add_col": 1, 'csrftoken': csrftoken}});
                // showTab(9);
                action_add_col.disabled = true;
                document.getElementById("action_col_next").disabled = false;
            });
            bindEvent(action_col_next, 'click', function (e) {
                sendMessage({'action_data': {"action_col_next": 1, 'csrftoken': csrftoken}});
                showTab(9);
                action_col_next.disabled = true;
                list_column = [];
            });

            // File Download
            // bindEvent(action_img, 'click', function (e) {
            //  action_type = 11;
            //  sendMessage({'action_data': {"action_type": action_type, 'csrftoken': csrftoken}});
            //  showTab(16);
            // });
            // bindEvent(action_file, 'click', function (e) {
            //  action_type = 11;
            //  sendMessage({'action_data': {"action_type": action_type, 'csrftoken': csrftoken}});
            //  showTab(16);
            // });
            // bindEvent(action_file_confirm, 'click', function (e) {
            //  sendMessage({'action_data': {"action_file_confirm": 1, 'csrftoken': csrftoken}});
            //  showTab(17);
            //  action_file_confirm.disabled = true;
            // });
            // bindEvent(file_yes, 'click', function (e) {
            //  action_type = 12;
            //  sendMessage({'action_data': {"file_yes": 1, 'csrftoken': csrftoken}});
            //  showTab(18);
            // });
            // bindEvent(file_no, 'click', function (e) {
            //  sendMessage({'action_data': {"file_no": 1, 'csrftoken': csrftoken}});
            //  showTab(9);
            // });
            // bindEvent(file_next, 'click', function (e) {
            //  sendMessage({'action_data': {"file_next": 1, 'csrftoken': csrftoken}});
            //  showTab(9);
            // });
            // bindEvent(file_cancel, 'click', function (e) {
            //  sendMessage({'action_data': {"file_cancel": 1, 'csrftoken': csrftoken}});
            //  showTab(9);
            // });

            // From column name
            bindEvent(column_name, 'click', function (e) {
                // $("#collapseCardExample").collapse('show');
                let column_text = document.getElementById('column_text');
                sendMessage({'action_data': {"column_name": column_text.value, 'csrftoken': csrftoken}});
                //document.getElementById("tab2").click();
                showTab(9);
                if (state_machine.length > pointer){
                    clear_action_state(4);
                }
                state_node["action_list"][0]["column_name"] = column_text.value;
                column_text.value = "";
                column_name.disabled = true;
            }); 
            bindEvent(continue_action, 'click', function (e) {
                $("#collapseCardExample").collapse('hide');
                showTab(2);
                set_color(tabledata, 5);
            }); 

            bindEvent(next_to_pagination, 'click', function (e) {
                $("#collapseCardExample").collapse('hide');
                showTab(10);
                showStep(2);
                action_type = 9;
                sendMessage({'action_data': {"action_type": action_type, 'csrftoken': csrftoken}});
                set_color(tabledata, 5);
            }); 
            bindEvent(pagination_confirm, 'click', function (e) {
                sendMessage({'action_data': {"pagination_confirm": 1, 'csrftoken': csrftoken}});
                showTab(19);
                showStep(3);
                pagination_confirm.disabled = true;
            });
            bindEvent(pagination_next, 'click', function (e) {
                let pag_num = document.getElementById("pag_num");
                page_info["num_pages"] = pag_num.value;
                sendMessage({'action_data': {"pagination_next": 1, 'csrftoken': csrftoken}});
                showTab(11);
                pag_num.value = "2";
            });
            
            bindEvent(pagination_skip, 'click', function (e) {
                //document.getElementById("tab2").click();
                sendMessage({'action_data': {"pagination_skip": 1, 'csrftoken': csrftoken}});
                showTab(11);
                pagination_confirm.disabled = true;
            });
            bindEvent(finish, 'click', function (e) {
                let project_name = document.getElementById("project_name");
                procedure["project_name"] = project_name.value;
                procedure["page_list"].push(page_info);
                project_name.value = "";
                finish.disabled = true;
                document.getElementById("load_bar").style.display = 'block';
                console.log(procedure);
                // Send to server
                $.ajax({
                    url : "/saas/procedure",
                    type : "POST",
                    contentType: 'application/json;charset=UTF-8',
                    data : JSON.stringify({'procedure': procedure, 'csrftoken': csrftoken}),
                    success: function(data) {
                        document.getElementById("load_bar").style.display = 'block';
                        window.setTimeout(function() {
                             window.parent.window.location = "http://192.168.20.152:8000/global/projects/";
                        }, 3000);
                    },
                    error: function(error){
                        console.log(error);
                    }
                });
            });
            bindEvent(back_button, 'click', function (e) {
                if(currentTab != -1){
                    // Initialization
                    // -1 <- 0 (Big code)
                    
                    // Fill& Click
                    // 0 <- 12
                    // 12 <- 4
                    // 4 <- 2 (From, Extract Text: To, Click)

                    // Extraction
                    // 0 <- 2 (From, Extract Text)
                    // 2 <- 5 (From, Simple confirm)
                
                    // 5 <- 6 (From, Similar yes/ No)
                    // 6 <- 8 (From, Column name)
                    // 6 <- 7 (From Similar Yes)
                    // 7 <- 8 (Col name in similar yes) ????????

                    // 8 <- 9 (From, Set additional data/ Next)
                    // 9 <- 2 (New action page)         ????????
                    // 9 <- 10 (Pagination next/ Skip)
                    // 10 <- 11 (Pagination Skip ,Project Name)
                    // 10 <- 19 (Num_pages)
                    // 19 <- 11 (Pagination next, Project Name) ??????? 
                    
                    // 2 <- 13 (From, Table confirm)
                    // 13 <- 14 (From, Full/ Col table decision)
                    // Full table
                    // 14 <- 9 (From, Set additiona data/ Next, Go back to table)
                    // Col Table
                    // 14 <- 15 (From: Add column, To: Table decision)
                    // 15 <- 15 (From: Add column, To: Add column )
                    // 15 <- 9 (From, Set additiona data/ Next, Go back to Add column)

                    console.log("CurrentTab, len.of state, pointer is: ", currentTab, state_machine.length, pointer);
                    // console.log("State is: ", state_machine);
                    switch(currentTab) {
                        case 0:
                            if (pointer == 0 || (login_info["login_url"] != '' && pointer == 3 && state_machine.length == 3)){      // Redirect to start page
                                showTab(-1);
                                if(login_info["login_url"] == ''){
                                    document.getElementById("start_div1_login").style.display = 'block';
                                    document.getElementById("start_div2_login").style.display = 'block';
                                    before_login_flag = 1;
                                    let frameDoc = iframe.contentDocument || iframe.contentWindow.document;
                                    frameDoc.body.innerHTML = '';
                                }
                                else{
                                    console.log("Login URL: ", login_info["login_url"]);
                                }
                            }
                            break;
                        case 12:
                            sendMessage({'back_action_extraction': {'clear_fill': 1}});
                            let fill_input = document.getElementById("action_fill_text")
                            fill_input.value = '';
                            fill_input.disabled = true;
                            showTab(0);
                            break;
                        case 4:
                            pointer -= 1
                            sendMessage({'back_action_extraction': {'back_fill': state_machine[pointer]}});
                            showTab(12);
                            action_type = 14;
                            break;
                        case 2:
                            if (pointer == 0 || (login_info["login_url"] != '' && pointer == 3 && state_machine.length == 3)){
                                showTab(0);
                                document.getElementById("back_button").disabled = true;
                            } 
                            else{
                                if(state_machine[pointer-1]["action_list"][0]["action_type"] == 13){
                                    document.getElementById("load_bar").style.display = 'block';
                                    pointer -= 1;
                                    let frame = document.getElementById('iframe');
                                    // var fill_click_action_list = page_info["action_list"][0];
                                    $.ajax({
                                        url : "/saas/getLogin",
                                        type : "POST",
                                        // async:false,
                                        // dataType: "json",
                                        csrfmiddlewaretoken: csrftoken,
                                        beforeSend : function(jqXHR, settings) {
                                            jqXHR.setRequestHeader("x-csrftoken", csrftoken);
                                        },
                                        contentType: 'application/json;charset=UTF-8',
                                        data : JSON.stringify({"start_url": procedure['start_url'], 'login_info': procedure["login_details"], 'fill_click_list': [], 'flag': 1}),
                                        success: function(data) {
                                            frame.contentDocument.body.innerHTML = data;
                                            document.getElementById("load_bar").style.display = 'none';
                                            // frame.contentDocument.write(data);
                                            sendMessage({'back_action_extraction': {'back_click': state_machine[pointer-1]["action_list"][0]}});
                                        },
                                        error: function(error){
                                            console.log(error);
                                        }
                                    });
                                    showTab(4);
                                    document.getElementById("click_confirm").disabled = true;
                                    action_type = 13;
                                    break;
                                }
                                else{
                                    showTab(9);
                                }
                            }
                            if (pointer != state_machine.length){
                                let pop_row = maxrow;
                                let pop_col = tabledata.length + 1;
                                      
                                let temp_arr = [];
                                let loop = tabledata.length - (state_machine.length - pointer);
                                for (let i = 0; i < loop; i++) {
                                    temp_arr.push(tabledata[i]);
                                }
                                edit_preview(pop_row, pop_col, temp_arr);
                                if(pointer != 0){
                                    set_color(temp_arr, 9);
                                }    
                            }
                            break;
                        case 5:
                            if (pointer == state_machine.length){
                                sendMessage({'back_action_extraction': {'highlights': {"currentTab": 2, "extra_flag": 0, "action_node": state_node["action_list"][0]}}});
                                if (state_node["action_list"][0]["path"].length == 0){
                                    let simple_confirm = document.getElementById("simple_confirm");
                                    simple_confirm.disabled = true;
                                }
                            }
                            else{
                                sendMessage({'back_action_extraction': {'highlights': {"currentTab": 2, "extra_flag": 0, "action_node": state_machine[pointer]["action_list"][0]}}});
                            }
                            let simple_confirm1 = document.getElementById("simple_confirm");
                            simple_confirm1.disabled = true;
                            showTab(2);
                            break;
                        case 6:     // Remove disable and green flag
                            if (pointer == state_machine.length){
                                sendMessage({'back_action_extraction': {'highlights': {"currentTab": 5, "extra_flag": 0, "action_node": state_node["action_list"][0]}}});
                            }
                            else{
                                sendMessage({'back_action_extraction': {'highlights': {"currentTab": 5, "extra_flag": 0, "action_node": state_machine[pointer]["action_list"][0]}}});
                            }
                            let simple_confirm = document.getElementById("simple_confirm");
                            simple_confirm.disabled = false;
                            let similar_yes = document.getElementById("similar_yes");
                            similar_yes.disabled = true;
                            showTab(5);
                            let pop_row = maxrow;
                            let pop_col = tabledata.length + 1;
                            
                            let temp_arr = [];
                            let loop = 0;
                            if (pointer == state_machine.length){    
                                loop = tabledata.length;
                            } else{
                                loop = tabledata.length - (state_machine.length - pointer);
                            }
                            for (let i = 0; i < loop; i++) {
                                temp_arr.push(tabledata[i]);
                            }
                            edit_preview(pop_row, pop_col, temp_arr);
                            set_color(temp_arr, 6);
                            break;
                        case 7:
                            if (pointer == state_machine.length){
                                sendMessage({'back_action_extraction': {'highlights': {"currentTab": 6, "extra_flag": 0, "action_node": state_node["action_list"][0]}}});
                            }
                            else{
                                sendMessage({'back_action_extraction': {'highlights': {"currentTab": 6, "extra_flag": 0, "action_node": state_machine[pointer]["action_list"][0]}}});
                            }
                            let similar_confirm1 = document.getElementById("similar_confirm");
                            similar_confirm1.disabled = true;
                            showTab(6);
                            break;
                        case 8:
                            if (pointer == state_machine.length){
                                if (state_node["action_list"][0]["similar_path"].length == 0){
                                    sendMessage({'back_action_extraction': {'highlights': {"currentTab": 6, "extra_flag": 0, "action_node": state_node["action_list"][0]}}});
                                    showTab(6);
                                } else{
                                    sendMessage({'back_action_extraction': {'highlights': {"currentTab": 7, "extra_flag": 1, "action_node": state_node["action_list"][0]}}});
                                    showTab(7);
                                    let similar_confirm2 = document.getElementById("similar_confirm");
                                    similar_confirm2.disabled = false;
                                }
                            }
                            else{
                                if (state_machine[pointer]["action_list"][0]["similar_path"].length == 0){
                                    sendMessage({'back_action_extraction': {'highlights': {"currentTab": 6, "extra_flag": 0, "action_node": state_machine[pointer]["action_list"][0]}}});
                                    showTab(6);
                                } else{
                                    sendMessage({'back_action_extraction': {'highlights': {"currentTab": 7, "extra_flag": 1, "action_node": state_machine[pointer]["action_list"][0]}}});
                                    showTab(7);
                                    let similar_confirm = document.getElementById("similar_confirm");
                                    similar_confirm.disabled = false;
                                }
                            }
                            let column_text = document.getElementById("column_text");
                            column_text.value = "";
                            break;
                        case 9:
                            pointer -= 1;
                            if(state_machine[pointer]["action_list"][0]["action_type"] == 7){
                                sendMessage({'back_action_extraction': {'highlights': {"currentTab": 14, "extra_flag": 0, "action_node": state_machine[pointer]["action_list"][0]}}});
                                showTab(14);

                                let temp_arr2 = [];
                                let loop2 = tabledata.length - (state_machine.length - pointer);
                                for (let i = 0; i < loop2; i++) {
                                    temp_arr2.push(tabledata[i]);
                                }

                                temp_arr2.push(tabledata[loop2]);
                                sequence_arr(temp_arr2);
                                let temp_columns = total_columns;
                                temp_arr2.pop();

                                let pop_row2 = maxrow;
                                let pop_col2 = temp_columns + 1;
                                
                                edit_preview(pop_row2, pop_col2, temp_arr2);
                                set_color(temp_columns, 10);

                            } else if (state_machine[pointer]["action_list"][0]["action_type"] == 8){
                                clear_action_state(4);
                                sendMessage({'back_action_extraction': {'highlights': {"currentTab": 15, "extra_flag": 0, "action_node": state_machine[pointer]["action_list"][0]}}});
                                list_column = [];
                                for (let i = 0; i < state_machine[pointer]["action_list"][0]["column_list"].length; i++) {
                                    list_column.push(state_machine[pointer]["action_list"][0]["column_list"][i]);
                                }
                                col_flag == 1;
                                showTab(15);
                            
                            } else{
                                if (state_machine[pointer]["action_list"][0]["similar_path"].length == 0){      
                                    sendMessage({'back_action_extraction': {'highlights': {"currentTab": 8, "extra_flag": 0, "action_node": state_machine[pointer]["action_list"][0]}}});
                                } else {
                                    sendMessage({'back_action_extraction': {'highlights': {"currentTab": 8, "extra_flag": 1, "action_node": state_machine[pointer]["action_list"][0]}}});
                                }
                                showTab(8);
                                let pop_row1 = maxrow;
                                let pop_col1 = tabledata.length + 1;

                                let temp_arr1 = [];
                                let loop1 = tabledata.length - (state_machine.length - pointer);
                                for (let i = 0; i < loop1; i++) {
                                    temp_arr1.push(tabledata[i]);
                                }
                                temp_arr1.push(tabledata[loop1]);
                                temp_arr1[loop1][0] = '#Column name';
                                edit_preview(pop_row1, pop_col1, temp_arr1);
                                set_color(temp_arr1, 8);
                            }
                            break;
                        case 10:
                            sendMessage({'back_action_extraction': {'highlights': {"currentTab": 9}}});
                            page_info["pagination"] = [];
                            showTab(9);
                            set_color(tabledata, 9);
                            break;
                        case 11:
                            if (page_info["pagination"].length == 0){
                                sendMessage({'back_action_extraction': {'highlights': {"currentTab": 10, "extra_flag": 0, "pagination": []}}});
                                showTab(10);
                            } else{
                                sendMessage({'back_action_extraction': {'highlights': {"currentTab": 19, "extra_flag": 1, "pagination": page_info["pagination"]}}});
                                showTab(19);
                            }
                            break;
                        case 19:
                            sendMessage({'back_action_extraction': {'highlights': {"currentTab": 10, "extra_flag": 1, "pagination": page_info["pagination"]}}});
                            showTab(10);
                            let pagination_confirm = document.getElementById("pagination_confirm");
                            pagination_confirm.disabled = false;
                            break;

                        case 13:    // Table
                            sendMessage({'back_action_extraction': {'highlights': {"currentTab": 2, "extra_flag": 1, "pagination": page_info["pagination"]}}});
                            // Disable some buttons 
                            showTab(2);
                            break;
                        case 14:    // Full table
                            // Make green table as red 
                            if (pointer == state_machine.length){
                                sendMessage({'back_action_extraction': {'highlights': {"currentTab": 13, "extra_flag": 0, "action_node": state_node["action_list"][0]}}});
                            }
                            else{
                                sendMessage({'back_action_extraction': {'highlights': {"currentTab": 13, "extra_flag": 0, "action_node": state_machine[pointer]["action_list"][0]}}});
                            }
                            let action_table_confirm = document.getElementById("action_table_confirm");
                            action_table_confirm.disabled = false;
                            showTab(13);
                            break;
                        case 15:    // Column Table
                            if (pointer == state_machine.length){
                                if (col_flag == 0){
                                    col_flag == 1;
                                    list_column = [];
                                    for (let i = 0; i < state_node["action_list"][0]["column_list"].length; i++) {
                                        list_column.push(state_node["action_list"][0]["column_list"][i]);
                                    }
                                }
                                // Sending
                                if(list_column.length == 0){
                                    sendMessage({'back_action_extraction': {'highlights': {"currentTab": 14, "extra_flag": 1, "action_node": state_node["action_list"][0]}}});
                                    showTab(14);
                                    tabledata.pop();
                                    col_flag = 0;
                                } else{
                                    let action_add_col = document.getElementById("action_add_col");
                                    action_add_col.disabled = false;
                                    
                                    sendMessage({'back_action_extraction': {'highlights': {"currentTab": 15, "extra_flag": 1, "action_node": state_node["action_list"][0]}}});
                                    showTab(15);
                                    list_column.pop();
                                    state_node["action_list"][0]["column_list"].pop();

                                    let temp_arr3 = [];
                                    let loop3 = tabledata.length;
                                    for (let i = 0; i < loop3; i++) {
                                        temp_arr3.push(tabledata[i]);
                                    }

                                    sequence_arr(temp_arr3);
                                    let temp_columns3 = total_columns;
                                    let pop_row3 = maxrow;
                                    let pop_col3 = temp_columns3 + 1;
                                    
                                    let list_temp3 = temp_arr3.pop();                                    
                                    let diff = list_temp3[0].length - list_column.length;
                                    for (let i = 0; i < list_temp3.length; i++) {
                                        for (let j = 0; j < diff; j++) {
                                            list_temp3[i].pop();
                                        }
                                    }
                                    temp_arr3.push(list_temp3);
                                    edit_preview(pop_row3, pop_col3, temp_arr3);
                                    sequence_arr(temp_arr3);
                                    set_color(total_columns+1, 10);     // Remove Yellow
                                    if (total_columns > 0){
                                        set_color(temp_arr3, 9);     // Add Yellow              
                                    }   

                                }
                            }
                            else{
                                if(list_column.length == 0){
                                    sendMessage({'back_action_extraction': {'highlights': {"currentTab": 14, "extra_flag": 1, "action_node": state_machine[pointer]["action_list"][0]}}});
                                    showTab(14);
                                    col_flag = 0;
                                } else{
                                    let action_add_col = document.getElementById("action_add_col");
                                    action_add_col.disabled = false;
                                    
                                    sendMessage({'back_action_extraction': {'highlights': {"currentTab": 15, "extra_flag": 1, "action_node": state_machine[pointer]["action_list"][0]}}});
                                    showTab(15);
                                    list_column.pop();
                                    state_node["action_list"][0]["column_list"].pop();
                                
                                    let temp_arr3 = [];
                                    let loop3 = tabledata.length - (state_machine.length - pointer) + 1;
                                    for (let i = 0; i < loop3; i++) {
                                        temp_arr3.push(tabledata[i]);
                                    }

                                    sequence_arr(temp_arr3);
                                    let temp_columns3 = total_columns;
                                    let pop_row3 = maxrow;
                                    let pop_col3 = temp_columns3 + 1;
                                    
                                    let list_temp3 = temp_arr3.pop();                                    
                                    let diff = list_temp3[0].length - list_column.length;
                                    for (let i = 0; i < list_temp3.length; i++) {
                                        for (let j = 0; j < diff; j++) {
                                            list_temp3[i].pop();
                                        }
                                    }
                                    temp_arr3.push(list_temp3);
                                    edit_preview(pop_row3, pop_col3, temp_arr3);
                                    sequence_arr(temp_arr3);
                                    set_color(total_columns+1, 10);     // Remove Yellow
                                    if (total_columns > 0){
                                        set_color(temp_arr3, 9);     // Add Yellow              
                                    }    
                                }
                            }
                            break;
                        default:
                            console.log("Shamik-san error!!");
                    }
                }
            });

            // *********************** preview extracted data ************************

            bindEvent(window, 'message', function (e) {
                let data = e.data;
                if (data['iframe_content2'] != undefined){      // Preview Table Input
                    let data_type = data['iframe_content2'];
                    if(data_type['2'] != undefined){        // Confirmation Input
                        if(action_type < 4){
                            let simple_confirm = document.getElementById("simple_confirm");
                            simple_confirm.disabled = false;
                        }
                        else if(action_type > 3 && action_type < 7){
                            let similar_confirm = document.getElementById("similar_confirm");
                            similar_confirm.disabled = false;
                        }
                        else if(action_type == 7){
                            let action_table_confirm = document.getElementById("action_table_confirm");
                            action_table_confirm.disabled = false;
                        }
                        else if(action_type == 8){
                            let action_add_col = document.getElementById("action_add_col");
                            action_add_col.disabled = false;
                        }
                        else if(action_type == 9){
                            let pagination_confirm = document.getElementById("pagination_confirm");
                            pagination_confirm.disabled = false;
                        }
                        else if(action_type == 14){
                            // let fill_confirm = document.getElementById("fill_confirm");
                            // fill_confirm.disabled = false;
                            let action_fill_text = document.getElementById("action_fill_text");
                            action_fill_text.disabled = false;
                        }
                        else if(action_type == 13){
                            let click_confirm = document.getElementById("click_confirm");
                            click_confirm.disabled = false;
                        }
                        else if(action_type == 11){
                            let action_file_confirm = document.getElementById("action_file_confirm");
                            action_file_confirm.disabled = false;
                        }
                    }
                    // else if(data_type['3'] != undefined){        // Clear Input  
                    //  let simple_confirm = document.getElementById("simple_confirm");
                    //  let similar_confirm = document.getElementById("similar_confirm");
                    //  let pagination_confirm = document.getElementById("pagination_confirm");

                    //  simple_confirm.disabled = true;
                    //  similar_confirm.disabled = true;
                    //  pagination_confirm.disabled = true;
                    // }
                    else if(data_type['5'] != undefined){
                        let data_store = data_type['5'];
                        if (data_store["page_action"] != undefined){
                            if(extraction_flag == 0){       // For fill-click
                                clear_state(1);
                                let page_action = data_store["page_action"];
                                page_info["action_list"].push(page_action);
                                // state_node["action_list"].push(page_action);
                                state_node["action_list"].push({"action_type": page_action["action_type"], "path": page_action["path"], "similar_path": page_action["similar_path"], "column_name": page_action["column_name"], "fill_value": page_action["fill_value"], "row_column": [], "column_list": page_action["column_list"]});
                                state_machine.push({"loginTab": state_node["loginTab"], "currentTab": state_node["currentTab"], "action_list": [{"action_type": state_node["action_list"][0]["action_type"], "path": state_node["action_list"][0]["path"], "similar_path": state_node["action_list"][0]["similar_path"], "column_name": state_node["action_list"][0]["column_name"], "fill_value": state_node["action_list"][0]["fill_value"], "row_column": [], "column_list": state_node["action_list"][0]["column_list"]}]});
                            }
                            else{       // After extraction start
                                clear_state(2);
                                page_info["action_list"].push({"action_type": state_node["action_list"][0]["action_type"], "path": state_node["action_list"][0]["path"], "similar_path": state_node["action_list"][0]["similar_path"], "column_name": state_node["action_list"][0]["column_name"], "fill_value": state_node["action_list"][0]["fill_value"], "row_column": [], "column_list": state_node["action_list"][0]["column_list"]});
                                state_machine.push({"loginTab": state_node["loginTab"], "currentTab": state_node["currentTab"], "action_list": state_node["action_list"]});
                                state_node["action_list"] = [];
                                state_node["action_list"].push({"action_type": 0, "path": [], "similar_path": [], "column_name": "", "fill_value": "", "row_column": [], "column_list": []});
                            }
                            pointer += 1;
                            console.log("State: ", state_machine);
                            console.log("Length(state, pointer): ", state_machine.length, pointer);
                            console.log("Page_info: ", page_info["action_list"])
                            console.log("----------------------------");
                            // Stuti
                            if (action_type == 13){
                                // Send server {'start_url': procedure["start_url"], 'login_info': procedure["login_details"], 'fill_click_list': page_info["action_list"]}
                                // Fill&Click 
                                let frame = document.getElementById('iframe');
                                $.ajax({
                                    url : "/saas/getLogin",
                                    type : "POST",
                                    // async:false,
                                    // dataType: "json",
                                    csrfmiddlewaretoken: csrftoken,
                                    beforeSend : function(jqXHR, settings) {
                                        jqXHR.setRequestHeader("x-csrftoken", csrftoken);
                                    },
                                    contentType: 'application/json;charset=UTF-8',
                                    data : JSON.stringify({"start_url": procedure['start_url'], 'login_info': procedure["login_details"], 'fill_click_list': page_info["action_list"], 'flag': 1}),
                                    success: function(data) {
                                        frame.contentDocument.body.innerHTML = data;
                                        action_extract.disabled = false;
                                        action_fc.disabled = false;
                                        document.getElementById("load_bar").style.display = 'none';
                                        // frame.contentDocument.write(data);
                                    },
                                    error: function(error){
                                        console.log(error);
                                    }
                                });

                            }
                            else if(action_type == 14){
                                action_type = 13;
                            }
                        }
                        else if(data_store["pagination"] != undefined){
                            page_info["pagination"] = data_store["pagination"];
                        }
                    }
                    else if(data_type['1'] != undefined){
                        $("#collapseCardExample").collapse('show');

                        let data_col = data_type['1'];
                        if(data_col['no_col_name'] != undefined){           // Data default col_name
                            let pop_row = maxrow;
                            let pop_col = tabledata.length + 1;
                            let loop = tabledata.length - (state_machine.length - pointer); 
                            let temp_arr = [];
                            for (let i = 0; i < loop; i++) {
                                temp_arr.push(tabledata[i]);
                            }
                            temp_arr.push(data_col['no_col_name']);
                            edit_preview(pop_row, pop_col, temp_arr);
                            set_color(temp_arr, 3);
                        }
                        else if (data_col['result'] != undefined){          // Data with col_name
                            let pop_row = maxrow;
                            let pop_col = tabledata.length + 1;
                            let loop = state_machine.length - pointer;
                            for (let i = 0; i < loop; i ++){
                                tabledata.pop();
                            }
                            tabledata.push(data_col['result']);
                            edit_preview(pop_row, pop_col, tabledata);
                            set_color(tabledata, 4);
                        }
                        else if (data_col['full_table'] != undefined){      // Full table
                            let pop_row = maxrow;
                            let pop_col = tabledata.length + 1;
                            let loop = state_machine.length - pointer;
                            for (let i = 0; i < loop; i ++){
                                tabledata.pop();
                            }
                            tabledata.push(data_col['full_table']);
                            edit_preview(pop_row, pop_col, tabledata);
                            set_color(tabledata, 4);
                        }
                        else if (data_col['full_table'] != undefined){      // Full table
                            let pop_row = maxrow;
                            let pop_col = tabledata.length + 1;
                            let loop = state_machine.length - pointer;
                            for (let i = 0; i < loop; i ++){
                                tabledata.pop();
                            }
                            tabledata.push(data_col['full_table']);
                            edit_preview(pop_row, pop_col, tabledata);
                            set_color(tabledata, 4);
                        }
                        else if (data_col['col_table'] != undefined){      // Col. table
                            let pop_row = maxrow;
                            let pop_col = tabledata.length + 1;
                            let loop = 0;
                            if(list_column.length == 0){
                                loop = state_machine.length - pointer;
                            } else{
                                loop = state_machine.length - pointer - 1;
                            }
                            for (let i = 0; i < loop; i ++){
                                tabledata.pop();
                            }
                            
                            if (list_column.length == 0){
                                tabledata.push(transpose([data_col['col_table']["data"]]));
                            }
                            else{
                                let temp_col = tabledata.pop();
                                let temp1 = transpose(temp_col);
                                temp1.push(data_col['col_table']["data"]);
                                tabledata.push(transpose(temp1));
                            }
                            list_column.push(data_col['col_table']["index"]);
                            state_node["action_list"][0]["column_list"].push(data_col['col_table']["index"]);
                            edit_preview(pop_row, pop_col, tabledata);
                            set_color(tabledata, 4);
                        }
                    }
                    else if(data_type['similarcheck'] != undefined){
                        let similar_yes = document.getElementById("similar_yes");
                        similar_yes.disabled = false;
                        if (state_machine.length > pointer){
                            clear_action_state(2);
                        }
                        state_node["action_list"][0]["path"] = data_type['similarcheck'];
                        // Add to preview table: data_type['last_val']
                        let pop_row = maxrow;
                        let pop_col = tabledata.length + 1;
                              
                        let temp_arr = [];
                        let loop = 0;
                        if(state_machine.length == pointer){
                            loop = tabledata.length;
                        } else{
                            loop = tabledata.length - (state_machine.length - pointer);
                        }
                        for (let i = 0; i < loop; i++) {
                            temp_arr.push(tabledata[i]);
                        }
                        temp_arr.push(data_type['last_val']);
                        edit_preview(pop_row, pop_col, temp_arr);
                        set_color(temp_arr, 2);
                    }
                    else if(data_type['singlecheck'] != undefined){
                        if (state_machine.length > pointer){
                            clear_action_state(2);
                        }
                        state_node["action_list"][0]["path"] = data_type['singlecheck'];
                        // Add to preview table: data_type['last_val']
                        let pop_row = maxrow;
                        let pop_col = tabledata.length + 1;
                              
                        let temp_arr = [];
                        let loop = 0;
                        if(state_machine.length == pointer){
                            loop = tabledata.length;
                        } else{
                            loop = tabledata.length - state_machine.length + pointer;
                        }
                        for (let i = 0; i < loop; i++) {
                            temp_arr.push(tabledata[i]);
                        }
                        temp_arr.push(data_type['last_val'])
                        edit_preview(pop_row, pop_col, temp_arr);
                    }
                    else if(data_type['similarcheck2'] != undefined){
                        if (state_machine.length > pointer){
                            clear_action_state(3);
                        }
                        state_node["action_list"][0]["similar_path"] = data_type['similarcheck2'];
                    }
                    else if(data_type['tablecheck'] != undefined){
                        let tablecheck = data_type['tablecheck'];
                        if (tablecheck['1'] != undefined){
                            let temp_action = tablecheck['1'];
                            state_node["action_list"][0]["path"] = tablecheck['1'];
                        }
                    }
                }
            });
            
            function transpose(a) {
                return a[0].map(function (_, c) { return a.map(function (r) { return r[c]; }); });
            }

            function edit_preview(row, col, data){
                remove_preview(row, col);
                let new_arr = sequence_arr(data);
                find_maxrow(new_arr);
                add_preview(new_arr);
            }
            
            function find_maxrow(data){
                if (data.length == 0){
                    maxrow = 0;
                } else{
                    maxrow = 0;
                    for (let i = 0; i < data.length; i++){
                        if(data[i].length > maxrow){
                            maxrow = data[i].length;
                        }    
                    }
                }
            }

            function sequence_arr(data){
                let new_arr = [];
                for(let i = 0; i < data.length; i++){
                    if(data[i][0].constructor === Array){
                        let mrow = 0;
                        for (let k = 0; k < data[i].length; k++) {
                            if (data[i][k].length > mrow){
                                mrow = data[i][k].length;
                            }
                        }
                        for(let p = 0; p < mrow; p++){
                            let pq_arr = [];
                            for (let q = 0; q < data[i].length; q++){
                                pq_arr.push(data[i][q][p]);
                            }
                            new_arr.push(pq_arr);
                        }
                    }
                    else{
                        new_arr.push(data[i]);
                    }
                }
                // console.log("New arr: ", new_arr)
                total_columns = new_arr.length;
                return new_arr;
            }

            function remove_preview(row, col) {
                let table = document.getElementById("htCore");
                for (let i = 0; i < row; i++) {
                    for (let j = 0; j < col; j++) {
                        table.rows[i+1].cells[j+1].innerHTML = '';
                    }
                }
            }

            function set_color(data, flag){
                let table = document.getElementById("htCore");
                // let data = tabledata;
                // console.log("Data is:", tabledata.length, data.length, maxrow, data);
                $("#collapseCardExample").collapse('show');
                switch(flag){
                    case 1:     // Initial color to table            
                        // for (let i = 1; i < 501; i++) {
                        //     table.rows[i].cells[1].style.backgroundColor = "white";
                        // }
                        break;
                    case 2:     // Show similar color
                        for (let i = 3; i < 501; i++) {
                            table.rows[i].cells[total_columns].style.backgroundColor = "yellow";
                        }
                        // Remove red (in case of black)
                        table.rows[1].cells[total_columns].style.backgroundColor = "white";
                        break;
                    case 3:     // Show column name red
                        for (let i = 2; i < 501; i++) {         // Remove the prev. yellow cells
                            table.rows[i].cells[total_columns].style.backgroundColor = "white";
                        }
                        table.rows[1].cells[total_columns].style.color = "black"; 
                        table.rows[1].cells[total_columns].style.backgroundColor = "red";
                        break;
                    case 4:     // After `Set additional data`, make next column yellow
                        // Remove the previous red color
                        table.rows[1].cells[total_columns].style.backgroundColor = "white";
                        for (let i = 1; i < 501; i++) {
                            table.rows[i].cells[total_columns+1].style.backgroundColor = "yellow";
                        }
                        break;
                    case 5:     // Remove next column
                        for (let i = 1; i < 501; i++) {
                            table.rows[i].cells[total_columns+1].style.backgroundColor = "white";
                        }
                        break;
                    case 6:     // Remove similar color
                        for (let i = 1; i < 501; i++) {
                            table.rows[i].cells[total_columns+1].style.backgroundColor = "white";
                        }
                        break;
                    case 7:     // Remove red column color
                        table.rows[1].cells[total_columns].style.backgroundColor = "white";
                        break;
                    case 8:     // Remove similar color and make column name red
                        for (let i = 1; i < 501; i++) {
                            table.rows[i].cells[total_columns+1].style.backgroundColor = "white";
                        }
                        table.rows[1].cells[total_columns].style.backgroundColor = "red";
                        break;
                    case 9:     // Make next column yellow
                        for (let i = 1; i < 501; i++) {
                            table.rows[i].cells[total_columns+1].style.backgroundColor = "yellow";
                        }
                        break;
                    case 10:    // Remove next column in Table case
                        for (let i = 1; i < 501; i++) {
                            table.rows[i].cells[data+1].style.backgroundColor = "white";
                        }
                        break;
                    default:
                        console.log("Color switch is wrong!!");
                }
            }

            function add_preview(data){
                let table = document.getElementById("htCore");
                for (let i = 0; i < maxrow; i++) {
                    for (let j = 0; j < data.length; j++) {
                        if (data[j].length > i && data[j][i] != undefined) {
                            table.rows[i+1].cells[j+1].innerHTML = data[j][i];
                        }
                    }
                }
            }
        }
    }

    function submit_url(){
        document.getElementById('submit_url').addEventListener('click', function(){
            document.getElementById("start_div1").style.display = 'none';
            document.getElementById("start_div2").style.display = 'none';
            document.getElementById("start_div1_login").style.display = 'block';
            document.getElementById("start_div2_login").style.display = 'block';
            document.getElementById("back_button").disabled = false;
            before_login_flag = 1;
        });
        document.getElementById('login_yes').addEventListener('click', function(){
            document.getElementById("start_div1_login").style.display = 'none';
            document.getElementById("start_div2_login").style.display = 'none';
            document.getElementById("start_div1_login2").style.display = 'block';
            document.getElementById("start_div2_login2").style.display = 'block';
            before_login_flag = 2;
        });
        document.getElementById('login_no').addEventListener('click', function(){
            document.getElementById("start_div1_login").style.display = 'none';
            document.getElementById("start_div2_login").style.display = 'none';
            document.getElementById("load_bar").style.display = 'block';
            document.getElementById("back_button").disabled = true;
            $("#collapseCardExample").collapse('hide');
            before_login_flag = 3;
            startJS();
        });
        document.getElementById('submit2_url').addEventListener('click', function(){
            document.getElementById("start_div1_login2").style.display = 'none';
            document.getElementById("start_div2_login2").style.display = 'none';
            document.getElementById("load_bar").style.display = 'block';
            document.getElementById("back_button").disabled = true;
            $("#collapseCardExample").collapse('hide');
            before_login_flag = 3;
            startJS();
        });

        document.getElementById('back_button').addEventListener('click', function(){
            console.log("Before_login_flag: ", before_login_flag);
            if(before_login_flag < 3){
                switch(before_login_flag) {
                    case 1:
                        document.getElementById("start_div1_login").style.display = 'none';
                        document.getElementById("start_div2_login").style.display = 'none';
                        document.getElementById("start_div1").style.display = 'block';
                        document.getElementById("start_div2").style.display = 'block';
                        document.getElementById("back_button").disabled = true;
                        document.getElementById('start_url').value = "";
                        document.getElementById('submit_url').disabled = true;
                        before_login_flag = 0;
                        break;
                    case 2:
                        document.getElementById("start_div1_login2").style.display = 'none';
                        document.getElementById("start_div2_login2").style.display = 'none';
                        document.getElementById("start_div1_login").style.display = 'block';
                        document.getElementById("start_div2_login").style.display = 'block';
                        document.getElementById('login_url').value = "";
                        before_login_flag = 1;
                        break;
                    default:
                        console.log("Back button before login error!!");
                }
            }
        });
    }

    /**
     * Add event listeners for DOM-inspectorey actions
    */
    if (document.addEventListener) {
        window.addEventListener('load', submit_url);
    } else if (document.attachEvent) {
        window.attachEvent('onload', submit_url);
    }

})(document);
