    //Auto Refresh
    autoRefresh();
    var no_update_count = 0;
    var eids = [];
    $(".running").each(function() {
        eids.push($(this).attr("value"));
    });
    function autoRefresh() {
        timer = setTimeout(function(){
            $.ajax({
                type: "GET",
                url: "{% url 'dashboard:auto_refresh' %}",
                data: {'eids': eids},
                cache: false,
                dataType: "json",
                success: function (data) {
                    if (data['id']) {
                        for (var i=0; i<Object.keys(data['id']).length; i++) {
                            $("img[class='running'][value='"+data['id'][i]+"']").each(function() {
                                eid = $(this).attr("value");
                                pid = $(this).parent().attr("value");
                                if(data['status'][i] == 1){
                                    $(this).attr('src', "{% static 'assets/img/tick.png' %}");
                                    $(this).parent().next().css('display','none');
                                    $(this).parent().parent().append(`
                                    <td>
                                        <form name="searchForm" id="searchForm" method="post" action="{% url 'dashboard:product_type' %}">
                                            <input id="ftype" name="filetype" type="hidden">
                                            <div class="dropdown">
                                                <button class="btn btn-primary dropdown-toggle dbtn" data-toggle="dropdown" value="#{{user.id}}#`+pid+`#`+eid+`">Download
                                                    <span class="caret"></span></button>
                                                <ul class="dropdown-menu" value="0">
                                                </ul>
                                            </div>
                                        </form>
                                    </td>`);
                                }else if(data['error'][i] == 1){
                                    $(this).attr('src', "{% static 'assets/img/cross.png' %}");
                                    $(this).parent().next().text("{% trans 'Error' %}");
                                }
                                var idx = eids.indexOf(eid);
                                if(idx >= 0){
                                    eids.splice(idx, 1);
                                }
                            });
                            console.log("-- Auto Refreshed --");
                        }
                    }else{
                        if(no_update_count >= 10){
                            clearTimeout(timer);
                        }
                        no_update_count++;
                        console.log("--- Auto Refresh No update Count "+no_update_count+" ---");
                    }
                },
                error: function (){
                    console.log("--- Auto Refresh Fail ---");
                }
            });
            autoRefresh();
        }, 30*1000);
    }


    //popup date
    $(function(){
        var today = new Date();
        var mm = today.getMonth() + 1;
        var dd = today.getDate();
        var dayNames = ['月', '火', '水', '木', '金', '土', '日'];

        $Month = "<select name='month' class='month'>";
        for(var i=1; i<=31; i++){
            if(i == dd){
                $Month +="<option value=\""+ i +"\" selected >"+ i +"</option>";
            }else{
                $Month +="<option value=\""+ i +"\" >"+ i +"</option>";
            }
        }
        $Month += "</select>";
        $('.month').html($Month);

        $Day = "<select name='day' class='day'>";
        console.log(today);
        for(var i=1; i<=6; i++ ){
            if(i == 1){
                $Day +="<option value=\""+ i +"\" selected >"+ i +"</option>";
            }else{
                $Day +="<option value=\""+ i +"\" >"+ i +"</option>";
            }
        }
        $Day += "</select>";
        $('.day').html($Day);

        $dayName = "<select name='day-name' class='day-name'>";
        for(var i=0; i<=6; i++ ){
            if(i == 0){
                $dayName +="<option value=\""+ dayNames[i] +"\" selected >"+ dayNames[i] +"</option>";
            }else{
                $dayName +="<option value=\""+ dayNames[i] +"\">"+ dayNames[i] +"</option>";
            }
        }
        $dayName += "</select>";
        $('.day-name').html($dayName);
	  });

    //show and hide project
    var currentMode = $("#all_project").attr("value");
    //if(currentMode == "categoryMode"){
    //    $('.card-content table:not(:first)').hide();
    //}
    $('.modal-open + span').hide();
	  $('.card-header').click(function(){
        $($(this).parent().find('.card-content table')).toggle();
        $($(this).find('.modal-open, .modal-open + span')).toggle();
	  });

    //modal screen
    $(".modal-popup-content > .input-group").hide();
	  $(".check-click").click(function(){
        $(".input-group").toggle();
    });

    //refine search
    $(function(){
        searchWord = function(){
            var searchText = $(this).val();
            var targetText;
            $('.card-content > #project_table > tbody > tr > td:first-child').each(function() {
                targetText = $(this).text();
                if (targetText.indexOf(searchText) != -1) {
                    $(this).parent().removeClass('hidden');
                } else {
                    $(this).parent().addClass('hidden');
                }
            });
        };
        $('.search-box').on('input', searchWord);
    });

    //button popup
	  $('.modal-setting, .modal-history, .modal-project, .modal-category').click(function(){
        var button_name = $(this).attr("class");
        var index = $("."+ button_name).index(this);
        var class_name = "."+ button_name + "-content:nth("+ index +")";

        modal(class_name);
    });
    //override
	  $('.modal-setting, .modal-category').click(function(){
        var pid = $(this).attr("value");
        //setting-popup: move category
        $("input[name='projectId']").attr("value", pid);
        //setting-popup: show and hide category_type
        $('.create-category').show();
        $('.move-category').hide();
	      $("input[name='categoryType']").click(function(){
            var category_type = $(this).attr("value");
            if (category_type == "create") {
                $('.create-category').show();
                $('.move-category').hide();
            } else if (category_type == "move") {
                $('.create-category').hide();
                $('.move-category').show();
            }
        });
        //category-popup: rename category
        $("input[name='category-rename-origin']").attr("value", pid);
    });

    //display category's name when we select a location where to move category
    $(function(){
        $('.dropdown-menu .dropdown-item').click(function(){
            var visibleItem = $('.dropdown-toggle', $(this).closest('.dropdown'));
            visibleItem.text($(this).attr('value'));
        });
    });

    //test tool
    //$(function() {
    //    modal(".modal-setting-content");
    //});

    //connection with client
		function myFunction() {
	      var backend;
	      new QWebChannel(qt.webChannelTransport, function (channel) {
	          backend = channel.objects.backend;
	          column_type();
	      });
	      function column_type(){
	          backend.project_start(1);
	      }
		}

    //show Calendar
	  $(".modal-download").click(function(){
        var pid;
        var button_name = $(this).attr("class");
        var index = $("."+ button_name).index(this);
        var class_name = "."+ button_name + "-content:nth("+ index +")";
        pid = $(this).attr("value");
        //project Date Range
        var proj_date = $(this).parents("td").prevAll().find('.proj_date').text();
        var min_year = proj_date.slice(0, 4); //"2018"
        var min_month = proj_date.slice(5, 7); //"08"
        //execution Date
        var exec_date = $(this).parents("td").prevAll().find('.exec_date').text();
        $("#date_"+pid).datepicker({
            dateFormat: 'yy/mm/dd',
            minDate: new Date(parseInt(min_year), parseInt(min_month)-1, 1),
            maxDate: '+0d',
            onSelect: function(dateText, inst) {
                $("tr.date_val_"+pid).css("display", "none");
                $("tr.date_val_"+pid+">td:nth-child(1):contains('"+dateText+"')").parent().css("display", "table-row");
            },
            beforeShowDay: function(date) {
                var dateStr = createDateStr(date);
                if ($(".dateList_"+pid).val().indexOf(dateStr) != -1) {
                    return [true,"ui-datepicker-date-exist"];
                }
                else {
                    return [false];
                }
            }
        });
        $("#date_"+pid).datepicker("setDate", exec_date);
        modal(class_name);
    });
    function createDateStr(date) {
        var year = date.getYear();
        var month = date.getMonth() + 1;
        var day = date.getDate();
        if (year < 2000) { year += 1900 };
        if (month < 10)  { month = "0" + month };
        if (day < 10)    { day = "0" + day };
        var dateStr = year + "/" + month + "/" + day;
        return dateStr;
    }
	  $(".ui-datepicker-date-exist").click(function(){
        $(".ui-datepicker-date-exist").show();
    });

    //popup
    function modal(className){
        $(function(){
            //open
            $(this).blur();
           	if($("#modal-overlay")[0]) return false ;
           	$("body").append('<div id="modal-overlay"></div>');
           	$("#modal-overlay").fadeIn("slow");
           	centeringModalSyncer(className);
           	$(className).animate({
                opacity: '1',
                right: '+=' + ($(className).outerWidth() + 50) + 'px'
            }, 600);
            //close
            $("#modal-overlay, #modal-close").unbind().click(function() {
                modalClose(className);
            });
            //Esc close
            $(window).keyup(function(e){
                if(e.keyCode == 27){
                   	modalClose(className);
                }
            });
        });
    }
    function modalClose(className){
        $(className).animate({
            opacity: '0',
            right: '-=' + ($(className).outerWidth() + 50) + 'px'
        }, 600);
        $(className).fadeOut(100, function(){
            $('#modal-overlay').remove();
            $(className).css('display', 'inline-block');
        });
    }
    //modal centering function
    $(window).resize(centeringModalSyncer);
    function centeringModalSyncer(className) {
        var w = $( window ).width();
        var h = $( window ).height();

        var cw = $(className).outerWidth();
        var ch = $(className).outerHeight();

        $(className).css({
            "right": -cw + "px",
            "top": ((h - ch) / 2) + "px"
        });
   	}

    //edit project name
    $(".editprojname").click(function(){
        var el = $(this).siblings('.editme');
        var pid = el.attr("value");
        var input = $('<input class = "editme" onblur="blurFoo.call(this)" value='+pid+'>').val(el.text());
        el.replaceWith( input );
        input.focus();
    });
    function blurFoo(){
        var el = $(this);
        var text = $(this).val();
        var  pid = $(this).attr("value");

        $.ajax({
            url: "{% url 'dashboard:edit_project' %}",
            data: { 'pid': pid, 'pname': text},
            cache: false,
            dataType: "json",
            success: function (data){
                if (data.changed == 1){
                    var input = $('<span class = "editme" contenteditable="false" value='+pid+'>'+text+'</span>')
                    el.replaceWith(input);
               }
            },
            error: function(){
                console.log("blurFoo fail");
            }
        });
    }

    //edit keywords
    $(".editkwname").click(function(){
        var el = $(this).siblings('.editme1');
        var pid = el.attr("value");
        var input = $('<input class = "editme1" onblur="keywordChangeAjax.call(this)" value='+pid+'>').val(el.text());
        el.replaceWith( input );
        input.focus();
    });
    function keywordChangeAjax(){
        var el = $(this);
        var text = $(this).val();
        var  pid = $(this).attr("value");

        $.ajax({
            url: "{% url 'dashboard:edit_keyword' %}",
            data: { 'pid': pid, 'kwname': text},
            cache: false,
            dataType: "json",
            success: function (data){
                if (data.changed == 1){
                    var input = $('<span class = "editme1" contenteditable="false" value='+pid+'>'+text+'</span>')
                    el.replaceWith(input);
               }
            },
            error: function(){
                console.log("keywordChangeAjax fail");
            }
        });
    }


    $(document).on("click",".dbtn", function() {
        var res = $(this).attr("value").split("#");
        var uid = res[1];
        var pid = res[2];
        var eid = res[3];
        var ch = $(this).next().attr("value");
        var ch_obj = $(this).next();
        $.ajax({
            url: "{% url 'dashboard:managefiletype' %}",
            data: { 'uid': uid, 'pid': pid , 'eid': eid},
            cache: false,
            dataType: "json",
            success: function (data){
                //type: text
                if(data.mediatype == 0){
                    if(ch === "0"){
                        ch_obj.append("<li onclick=\"$('#ftype').val('"+uid+"#"+pid+"#"+eid+"#1'); $('#searchForm').submit()\"><a>Excel</a></li>");
                        ch_obj.append("<li onclick=\"$('#ftype').val('"+uid+"#"+pid+"#"+eid+"#2'); $('#searchForm').submit()\"><a>CSV</a></li>");
                        ch_obj.append("<li onclick=\"$('#ftype').val('"+uid+"#"+pid+"#"+eid+"#3'); $('#searchForm').submit()\"><a>TXT</a></li>");
                        ch_obj.attr("value","1");
                    }
                }
                //type: media
                else if(data.mediatype == 1){
                    if(ch === "0"){
                       ch_obj.append("<li onclick=\"$('#ftype').val('"+uid+"#"+pid+"#"+eid+"#4'); $('#searchForm').submit()\"><a>MEDIA</a></li>");
                       ch_obj.attr("value","1");
                    }
                }
                //type: text and media
                else if(data.mediatype == 2){
                    if(ch === "0"){
                        ch_obj.append("<li onclick=\"$('#ftype').val('"+uid+"#"+pid+"#"+eid+"#1'); $('#searchForm').submit()\"><a>Excel</a></li>");
                        ch_obj.append("<li onclick=\"$('#ftype').val('"+uid+"#"+pid+"#"+eid+"#2'); $('#searchForm').submit()\"><a>CSV</a></li>");
                        ch_obj.append("<li onclick=\"$('#ftype').val('"+uid+"#"+pid+"#"+eid+"#3'); $('#searchForm').submit()\"><a>TXT</a></li>");
                        ch_obj.append("<li onclick=\"$('#ftype').val('"+uid+"#"+pid+"#"+eid+"#4'); $('#searchForm').submit()\"><a>MEDIA</a></li>");
                        ch_obj.attr("value","1");
                    }
                }
                //type: other
                else if(data.mediatype == -1){
                    if(ch === "0"){
                        ch_obj.append("<li>No Data. Please run again.</li>");
                        ch_obj.attr("value","1");
                    }
                }
                //error
                else {
                    if(ch === "0"){
                        ch_obj.append("<li>Error</li>");
                        ch_obj.attr("value","1");
                    }
                }
            },
            error: function(){
                console.log("managefiletype fail");
            }
        });
    });