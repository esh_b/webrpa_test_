

$(function() {
    $.fn.autoKana('#id_last_name', '#id_last_name_kana', {
        katakana : true
    });

    $.fn.autoKana('#id_first_name', '#id_first_name_kana', {
        katakana : true
    });

    $.fn.autoKana('#id_company_name', '#id_company_name_kana', {
        katakana : true
    });

    $('#id_post_code').jpostal({
        postcode : [
            '#id_post_code'
        ],
        address : {
            '#id_address' : '%3%4%5'
        },
        url : {
            'http'  : '/json/',
            'https' : '/json/'
        }
    });
});
