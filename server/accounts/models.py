from __future__ import unicode_literals
from django.conf import settings
from datetime import datetime
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.auth.models import AbstractUser, BaseUserManager ## A new class is imported. ##
from django.db import models
from django.utils.translation import ugettext_lazy as _
import os
import sys

import pytz

data_folder = settings.SCRAPING_DATA_DIR

class UserManager(BaseUserManager):
    """Define a model manager for User model with no username field."""

    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """Create and save a User with the given email and password."""
        if not email:
             raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        """Create and save a regular User with the given email and password."""
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        """Create and save a SuperUser with the given email and password."""
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, **extra_fields)




class User(AbstractUser):
    """User model."""
    #username = None
    email = models.EmailField(_('email address'), unique=True)
    old_email = models.EmailField(null=True)
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = []

    objects = UserManager() ## This is the new line in the User model. ##

    def last_name(self):
        return self.profile.last_name

    def first_name(self):
        return self.profile.first_name        

    def project_counts(self):
        from api_v1.scraping.models import Project
        num_proj = Project.objects.filter(user_id=self.id)
        return len(num_proj)

    def project_size(self):
        from api_v1.scraping.models import Project
        total_size = 0
        prj = Project.objects.filter(user_id=self.id)
        for project in prj:
            total_size = total_size + int(project.project_size or 0)

        g = 1024 * 1024 * 1024
        m = 1024 * 1024
        k = 1024

        show_size = round(total_size / m, 1)
        return show_size


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    bio = models.TextField(max_length=500, blank=True)
    birth_date = models.DateField(null=True, blank=True)
    notification = models.BooleanField(default=True)
    first_name = models.CharField(_('first name'), max_length=30, blank=True)
    last_name = models.CharField(_('last name'), max_length=150, blank=True)
    first_name_kana = models.CharField(max_length=15, blank=True)
    last_name_kana = models.CharField(max_length=15, blank=True)

    company_name = models.CharField(max_length=30, blank=True)
    company_name_kana = models.CharField(max_length=30, blank=True)
    department = models.CharField(max_length=30, blank=True)
    job_title = models.CharField(max_length=30, blank=True)
    tel_number = models.CharField(max_length=18, blank=True)
    post_code = models.CharField(max_length=10, blank=True)
    address = models.CharField(max_length=50, blank=True)

    public_key = models.CharField(max_length=1024, blank=True)
    private_key = models.CharField(max_length=1024, blank=True)

    is_agree_user_pol = models.BooleanField(default=False)
    is_agree_privacy_pol = models.BooleanField(default=False)
    is_agree_security_pol = models.BooleanField(default=False)

    timezone = models.CharField(max_length=100, choices=tuple(zip(pytz.all_timezones, pytz.all_timezones)), default='UTC')


    def __str__(self):
        return self.first_name_kana + "   " + self.last_name_kana

    def full_name_kana(self):
        return self.last_name_kana + "   " + self.first_name_kana  
    full_name_kana.short_description = 'full name (kana)'

    def full_name(self):
        return self.last_name + "   " + self.first_name
    full_name.short_description = 'full name'

    def emailid(self):
        return self.user.email
    emailid.short_description = "email"

    def staffstat(self):
        return self.user.is_staff


class Data_Order(models.Model):
    user          = models.ForeignKey(User, on_delete=models.PROTECT, null=True)
    url_list      = models.CharField(max_length=100)
    data_details  = models.CharField(max_length=200, blank=True)
    due_date      = models.DateField()
    purpose       = models.CharField(max_length=100)
    budget        = models.CharField(max_length=30)
    company       = models.CharField(max_length=30)
    department    = models.CharField(max_length=30)
    email         = models.CharField(max_length=100)
    phone_number  = models.CharField(max_length=15)
    regular_order = models.BooleanField(default=False)
    created_at = models.DateTimeField(default=datetime.now)

