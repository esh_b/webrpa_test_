from django.contrib import admin
from .models import Profile
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as DjangoUserAdmin
from django.utils.translation import ugettext_lazy as _
from .models import User

"""Integrate with admin module."""


# @admin.register(User)
class UserAdmin(DjangoUserAdmin):
    """Define admin model for custom User model with no email field."""

    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2'),
        }),
    )
    list_display = ('email', 'last_name', 'first_name', 'is_staff', 'last_login', 'project_counts', 'project_size')
    list_filter = ('last_login', 'is_staff')
    search_fields = ('email', 'first_name', 'last_name')
    actions = ["export_as_csv"]
    ordering = ('email',)

    def export_as_csv(self, request, queryset):
        import csv
        from django.http import HttpResponse
        from io import StringIO

        f = StringIO()
        writer = csv.writer(f)
        writer.writerow(['email', 'last_name', 'first_name', 'is_staff', 'last_login', 'project_counts', 'project_size'])

        for s in queryset:
            writer.writerow([s.email, s.last_name(), s.first_name(), s.is_staff, s.last_login, s.project_counts(), s.project_size()])

        f.seek(0)
        response = HttpResponse(f, content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename=user-info.csv'
        return response

    export_as_csv.short_description = "Export data"


admin.site.register(User, UserAdmin)


class IsStaff(admin.SimpleListFilter):
    title = 'staff'
    parameter_name = 'staff'

    def lookups(self, request, model_admin):
        return (
            ('True', 'True'),
            ('False', 'False'),
        )

    def queryset(self, request, queryset):
        value = self.value()
        if value == 'True':
            return queryset.filter(user__is_staff=True)
        elif value == 'False':
            return queryset.filter(user__is_staff=False)
        return queryset


class ProfileAdmin(admin.ModelAdmin):

    list_display = ('full_name_kana','full_name' ,'emailid', 'staff','company_name', 'company_name_kana','department','job_title','address','tel_number')
    list_filter = (IsStaff,)
    actions = ["export_as_csv"]

    def export_as_csv(self, request, queryset):
        import csv
        from django.http import HttpResponse
        from io import StringIO

        f = StringIO()
        writer = csv.writer(f)
        writer.writerow(['full_name_kana','full_name' ,'emailid', 'staff','company_name', 'company_name_kana','department','job_title','address','tel_number'])

        for s in queryset:
            writer.writerow([s.full_name_kana(), s.full_name(), s.emailid(), s.staffstat(), s.company_name, s.company_name_kana, s.department, s.job_title, s.address, str(s.tel_number)])

        f.seek(0)
        response = HttpResponse(f, content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename=profile-info.csv'
        return response

    export_as_csv.short_description = "Export data"

    def staff(self, obj):
        return obj.user.is_staff
    staff.short_description = 'staff'
    staff.boolean = True


admin.site.register(Profile, ProfileAdmin)
