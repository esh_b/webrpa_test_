from .models import User
from .models import Profile
from django.shortcuts import render, redirect
from django.http.response import HttpResponse
import datetime

def save_user_profile_payplan(strategy, user, response, details, is_new=False,*args,**kwargs):
    #User profile
    user_profile, created = Profile.objects.update_or_create(user=user, defaults={'first_name':details['first_name'], 'last_name': details['last_name']})

    """
    #User payment details (set to FREE plan by default)
    payplan = SubscriptionPlan.objects.get(id=1)
    user_payment, created = Payment.objects.update_or_create(user=user, defaults={'pay_date':datetime.datetime.now()})
    user_payplan, created = UserSubscription.objects.update_or_create(user=user, defaults={'payment': user_payment, 'plan': payplan})
    """

def check_email_exists(backend, details, uid, user=None, *args, **kwargs):
    print("inside...")
    email = details.get('email', '')

    if(not email):
        return render(kwargs['request'], 'global-sign-up.html', {'social_login_error': "Could not retrieve your email from your social account. Please try again!"})

    provider = backend.name

    # check if social user exists to allow logging in (not sure if this is necessary)
    social = backend.strategy.storage.user.get_social_auth(provider, uid)
    
    # check if given email is in use
    exists = User.objects.filter(email=email).exists()

    print(user, provider, social, exists)

    # user is not logged in, social profile with given uid doesn't exist
    # and email is in use
    if not user and not social and exists:
        return render(kwargs['request'], 'global-sign-up.html', {'social_login_error': "The given email " + email + " is already registered. Please login!"})

    return None