from django.contrib.auth import login
from django.http.request import HttpRequest
from django.http.response import HttpResponse, JsonResponse, HttpResponseRedirect
import os
import requests
from .models import User
from django.contrib.sites.shortcuts import get_current_site
from django.shortcuts import render, redirect
from django.urls import reverse
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string
import mimetypes
from wsgiref.util import FileWrapper
from .forms import UserForm, ProfileForm
from email_sys.tokens import account_activation_token
from django.contrib.auth import views as auth_views
from django.contrib.auth.decorators import login_required
from django.views.decorators.cache import cache_control
from .models import Profile
from datetime import datetime
from Crypto.PublicKey import RSA
from django.contrib.auth.views import login
from django.conf import settings

from django.db import transaction

from django.contrib.auth.forms import SetPasswordForm
from django.contrib.auth import update_session_auth_hash

from django.contrib.auth.views import PasswordResetView
from django.contrib.auth.views import PasswordResetDoneView
from django.contrib.auth.forms import PasswordResetForm
from django.utils.translation import gettext_lazy as _
from django.contrib.auth import get_user_model
from django.urls import reverse_lazy

from .decorators import anonymous_required

UserModel = get_user_model()

class ResendLinkForm(PasswordResetForm):

    """Form subclassing PasswordResetForm (for Resend_Activation_Link feature)
    """
    
    def get_users(self, email):
        """Get the list of users to send the mail
        
        Args:
            email (TYPE): Description
        
        Returns:
            TYPE: Description
        """
        active_users = UserModel._default_manager.filter(**{
            '%s__iexact' % UserModel.get_email_field_name(): email,
            'is_active': False,
        })
        return (u for u in active_users)


class ResendLinkView(PasswordResetView):

    """View subclassing PasswordResetView (for Resend_Activation_Link feature)
    
    Attributes:
        email_template_name (str): Name of email template to send
        form_class (TYPE): Class name of the form (asking for email address)
        from_email (str): Email address of the sender
        subject_template_name (str): Subject for the email
        success_url (TYPE): Redirect URL after SUCCESS
        template_name (str): Template for the form
        title (TYPE): Title for the form template
        token_generator (TYPE): Token generator class
    """
    
    form_class = ResendLinkForm
    title = _("Resend Activation Link")
    token_generator = account_activation_token
    template_name = "registration/resend_activation_link_form.html"
    success_url = reverse_lazy('accounts:resend_link_done')
    email_template_name = 'registration/resend_activation_link_email.html'
    subject_template_name = 'registration/resend_activation_link_subject.txt'
    from_email = 'no-reply@sms-datatech.co.jp'


class ResendLinkDoneView(PasswordResetDoneView):

    """View subclassing PasswordResetDoneView (for Resend_Activation_Link feature)
    
    Attributes:
        template_name (str): Template to display after activation link is sent
        title (TYPE): Title for the template
    """
    
    template_name = 'registration/resend_activation_link_done.html'
    title = _('Resent activation link')

def home(request):
    # return HttpResponseRedirect('https://services.sms-datatech.co.jp/pig-data/')
    return render(request, 'home.html')
def product(request):
    return render(request, 'product.html')
def learn(request):
    return render(request, 'learn.html')
def about(request):
    return render(request, 'about.html')
def contact(request):
    return render(request, 'contact.html')
def pricing(request):
    return render(request, 'pricing.html')

# global views
@anonymous_required
def global_home(request):
    return render(request, 'global-home.html')

@anonymous_required
def global_login(request):
    return render(request, 'global-login.html')

def global_contact(request):
    return render(request, 'contacts.html')

def get_client_ip(request: HttpRequest):
    """Function to get the client IP address
    
    Args:
        request (HttpRequest): Description
    
    Returns:
        TYPE: Description
    """
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR', None)
    if(x_forwarded_for):
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip

def get_user_timezone(request: HttpRequest):
    """Function to get the user timezone from IP address
    
    Args:
        request (HttpRequest): Description
    
    Returns:
        TYPE: Description
    """
    client_ip = get_client_ip(request)
    resp = requests.get('http://freegeoip.app/json/{0}'.format(client_ip))
    resp_json = resp.json()
    return resp_json['time_zone']

@anonymous_required
def global_signup(request):
    print('>>INSIDEShamik')
    user_form    = UserForm()
    profile_form = ProfileForm()
    if request.method == 'POST':

        user_form = UserForm(request.POST)
        profile_form = ProfileForm(request.POST)
        user_timezone = None

        try:
            user_timezone = get_user_timezone(request)
        except Exception as e:
            print("SIGNUP TIMEZONE ERROR:", e)
        if user_form.is_valid() and profile_form.is_valid() and user_timezone is not None:
            try:
                # with transaction.atomic():
                user = user_form.save(commit=False)
                user.is_active = False
                user.save()
                print('user saved')
                #profile
                profile = profile_form.save(commit=False)
                profile.timezone = user_timezone
                profile.user = user
                key_pair = RSA.generate(1024)
                profile.private_key = key_pair.exportKey().decode()
                profile.public_key = key_pair.publickey().exportKey().decode()
                profile.save()

                return render(request, 'account_activation_sent.html')
            except Exception as e:
                print(e)
                return render(request, 'error.html')
        else:
            print(user_form.errors)
            print(profile_form.errors)
            print("User timezone:", user_timezone)
            return render(request, 'global-sign-up.html', {'user_form': user_form, 'profile_form': profile_form})

    return render(request, 'global-sign-up.html', {'user_form': user_form, 'profile_form': profile_form})

@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required()
def logout_view(request, *args, **kwargs):
    auth_views.logout(request)
    request.session.flush()
    domain = get_current_site(request).domain
    #production
    if domain == "pig-data.sms-datatech.co.jp":
        return HttpResponseRedirect('https://pig-data.sms-datatech.co.jp/')
    #develop
    else:
        return HttpResponseRedirect('/')

@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@anonymous_required
def login(request, *args, **kwargs):
    return auth_views.login(request, 'global-login.html')

@anonymous_required
def signup(request):
    user_form = UserForm()
    profile_form = ProfileForm()
    if request.method == 'POST' and 'signup' in request.POST:
        user_timezone = None

        try:
            user_timezone = get_user_timezone(request)
        except Exception as e:
            print("SIGNUP TIMEZONE ERROR:", e)

        user_form = UserForm(request.POST)
        if user_form.is_valid():
            try:
                #user
                user = user_form.save(commit=False)
                user.is_active = False
                user.save()
            except Exception as e:
                print(e)
                return render(request, 'error.html')

            profile_form = ProfileForm(request.POST)
            key_pair = RSA.generate(1024)
            if profile_form.is_valid():
                try:
                    #profile
                    profile = profile_form.save(commit=False)
                    profile.user = user
                    profile.timezone = user_timezone
                    profile.private_key = key_pair.exportKey().decode()
                    profile.public_key = key_pair.publickey().exportKey().decode()
                    print(key_pair.publickey().exportKey().decode())
                    print('____________________\n')
                    print(key_pair.publickey().exportKey())

                    profile.save()
                    return render(request, 'account_activation_sent.html')
                except Exception as e:
                    print(e)
                    return render(request, 'error.html')

            else:
                print(profile_form.errors)
                return render(request, 'signup.html', {'user_form': user_form, 'profile_form': profile_form})
        else:
            print(user_form.errors)
            return render(request, 'signup.html', {'user_form': user_form, 'profile_form': profile_form})
    elif request.method == 'POST' and 'edit' in request.POST:
        user_form    = UserForm(request.POST)
        profile_form = ProfileForm(request.POST)
    return render(request, 'signup.html', {'user_form': user_form, 'profile_form': profile_form})

def signupConfirm(request):
    if request.method == 'POST':
        user_form    = UserForm(request.POST)
        profile_form = ProfileForm(request.POST)
        if user_form.is_valid() and profile_form.is_valid() :
            return render(request, 'signup-confirm.html', {'user_form': user_form, 'profile_form': profile_form})
        else:
            return render(request, 'signup.html', {'user_form': user_form, 'profile_form': profile_form })

    return redirect('accounts:signup')

def account_activation_sent(request):
    return render(request, 'account_activation_sent.html')

def activate(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None

    if user is not None and account_activation_token.check_token(user, token):
        user.is_active = True
        user.save(update_fields=['is_active'])
        return redirect('accounts:account_activation')
    else:
        return render(request, 'account_activation_invalid.html')

def account_activation(request):
    return render(request, 'account_activation.html')

def error(request):
    return render(request, 'error.html')

def downloadtool(request: HttpRequest):
    BASE_DIR = os.path.dirname(os.path.dirname(__file__))
    	
    path = "tool/PigData_1.0.3.exe"
    #return HttpResponse(serve(request, os.path.basename(path), os.path.dirname(path)))

    file_path = os.path.join(os.path.dirname(BASE_DIR), 'server', 'tool', 'PigData_1.0.3.exe')
    file_wrapper = FileWrapper(open(file_path, 'rb'))
    file_mimetype = mimetypes.guess_type(file_path)
    response = HttpResponse(file_wrapper, content_type=file_mimetype)
    response['X-Sendfile'] = file_path
    response['Content-Length'] = os.stat(file_path).st_size
    response['Content-Disposition'] = 'attachment; filename="PigData.exe"'
    return response


def postal_json(request, value):
    file_name = '%i.json' % int(value)
    target_path = os.path.join("static", "assets", "json", file_name)
    return HttpResponse(open(target_path, 'r'), content_type='application/json; charset=utf8')

"""
@cache_control(no_cache=True, must_revalidate=True, no_store=True)
@login_required()
def social_signup_set_password(request: HttpRequest):
    if request.method == 'POST':
        form = SetPasswordForm(request.user, request.POST)
        if form.is_valid():
            user = form.save()

            update_session_auth_hash(request, user)
            return redirect(settings.LOGIN_REDIRECT_URL)
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        form = SetPasswordForm(request.user)
    return render(request, 'social_signup_set_password.html', {
        'form': form
    })
"""
