from django.conf import settings
from django.shortcuts import redirect

class AnonymousRequired(object):

    """Summary
    
    Attributes:
        redirect_to (TYPE): Description
        view_function (TYPE): Description
    """
    
    def __init__(self, view_function, redirect_to):
        """Summary
        
        Args:
            view_function (TYPE): Description
            redirect_to (TYPE): Description
        """
        if redirect_to is None:
            redirect_to = settings.LOGIN_REDIRECT_URL
        self.view_function = view_function
        self.redirect_to = redirect_to

    def __call__(self, request, *args, **kwargs):
        """Summary
        
        Args:
            request (TYPE): Description
            *args: Description
            **kwargs: Description
        
        Returns:
            TYPE: Description
        """
        if request.user is not None and request.user.is_authenticated:
            return redirect(self.redirect_to)
        return self.view_function(request, *args, **kwargs)

def anonymous_required(view_function, redirect_to=None):
    """Function as decorator which will redirect the authenticated user to dashboard page
    
    Args:
        view_function (TYPE): Description
        redirect_to (None, optional): Description
    
    Returns:
        TYPE: Description
    """
    return AnonymousRequired(view_function, redirect_to)