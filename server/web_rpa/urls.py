"""web_rpa URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from django.urls.conf import include
from django.conf.urls import url
from django.views.generic.base import RedirectView
from django.conf.urls.i18n import i18n_patterns

urlpatterns = [
    #path('', RedirectView.as_view(url='dashboard')),
    path('', include('accounts.urls')),
    path('api/v1/core/', include('api_v1.core.urls')),
    path('api/v1/feedback/', include('api_v1.feedback.urls')),
    #path('api/v1/scheduling/', include('api_v1.scheduling.urls')),
    path('api/v1/scraping/', include('api_v1.scraping.urls')),
    path('admin/', admin.site.urls),
    path('', include('saas.urls')),
    path('', include('dashboard.urls')),
    path('', include('global_dashboard.urls')),
    path('email/', include('email_sys.urls')),
    path('i18n/', include('django.conf.urls.i18n')),
    path('oauth/', include('social_django.urls', namespace='social')),
    path('', include('social_django.urls', namespace='social')),
]

