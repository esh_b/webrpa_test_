import json
import os
import codecs
import requests
import random
import sys
import subprocess
from .forms import StartForm
from .xpath import Extraction
from accounts.models import User, Profile
from api_v1.scraping.views import saas_save_project
from bs4 import BeautifulSoup as B
from django.conf import settings
from django.http.request import HttpRequest
from django.http.response import HttpResponse, JsonResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
# from fake_useragent import UserAgent
from internal.rpalib.scraping import Project, ActionInfo
from internal.rpalib.internal import as_jsonable
from internal.rpalib.core import Session
from lxml.html import fromstring
from urllib.parse import urlparse
from selenium import webdriver
from selenium.webdriver.chrome.options import Options as ChromeOptions
from selenium.webdriver.firefox.firefox_binary import FirefoxBinary
from selenium.webdriver.firefox.options import Options as FirefoxOptions

download_folder = settings.SCRAPING_DOWNLOAD_DIR

@csrf_exempt
def pigdata(request):
    # form = StartForm()
    # return render(request, 'start.html', {'form': form})
    return render(request, 'pigdata-saas.html')

@csrf_exempt
def start(request):
    # form = StartForm()
    # return render(request, 'start.html')
    return render(request, 'login-index.html')

@csrf_exempt
def process(request):
    if request.method == 'POST':
        # create a form instance and populate it with data from the request:
        # form = StartForm(request.POST)
        # # check whether it's valid:
        # if form.is_valid():
        start_url = request.POST.get('start_url')

        if request.POST.get('is_login'):
            print('login-flag: set')
            login_url = request.POST['login_url']
            print('start url:' + start_url + 'login url :' + login_url + ' user id: ' + str(request.user.id) + '\n')
            request.session['login_url'] = login_url
            return render(request, 'pigdata-saas.html', {'start_url': start_url, 'login_url': login_url})
        else:
            print('login-flag: unset')
            print('start url:' + start_url + ' user id: ' + str(request.user.id) + '\n')
            return render(request, 'login-index.html', {'start_url': start_url})

    return HttpResponseRedirect('/saas/start/')


def get_proxies():
    url = 'https://free-proxy-list.net/'
    response = requests.get(url)
    parser = fromstring(response.text)
    proxies = []
    for i in parser.xpath('//tbody/tr')[:10]:
        if i.xpath('.//td[7][contains(text(),"yes")]'):
            proxy = ":".join([i.xpath('.//td[1]/text()')[0], i.xpath('.//td[2]/text()')[0]])
            proxies.append(proxy)
    return proxies


def get_browser(type, hide):
    browser = ''
    if type == 'firefox':
        firefox_binary_path = os.path.normpath(os.path.expanduser("/home/nitish/tor_test/tor-browser_en-US/Browser/firefox"))
        if not os.path.exists(firefox_binary_path):
            raise ValueError("The binary path to Tor firefox does not exist.")    

        firefox_driver = 'internal/geckodriver'

        options = FirefoxOptions()
        options.add_argument("--headless") if hide is True else options.add_argument("--head")
        options.add_argument('start-maximized')
        options.add_argument('disable-infobars')
        options.add_argument("--disable-extensions")
        options.add_argument("--window-size=1920x1080")
        
        profile = webdriver.FirefoxProfile()
        profile.set_preference("network.proxy.type", 1)
        profile.set_preference("network.proxy.socks", "127.0.0.1")
        profile.set_preference("network.proxy.socks_port", 9050)
        profile.set_preference("network.proxy.socks_version", 5)
        profile.update_preferences()

        browser = webdriver.Firefox(firefox_options=options, executable_path=firefox_driver)
        # browser = webdriver.Firefox(firefox_profile=profile, options=options, firefox_binary=firefox_binary_path)  
    elif type == 'chrome':
        options = ChromeOptions()
        options.add_argument("--headless")
        # options.add_argument('--no-sandbox')  # Bypass OS security model
        # options.add_argument('--disable-gpu')  # applicable to windows os only
        options.add_argument('start-maximized')
        options.add_argument('disable-infobars')
        options.add_argument("--disable-extensions")
        options.add_argument("--window-size=1920x1080")
        # ua = UserAgent()
        # user_agent = ua.random
        # options.add_argument(f'user-agent={user_agent}')

        # proxy = random.choice(get_proxies())
        # proxy = '194.44.34.6:41833'
        # print(proxy)
        # options.add_argument("--proxy-server=" + proxy)

        # prefs = {'disk-cache-size': 4096}
        # options.add_experimental_option('prefs', prefs)
        chrome_driver = 'internal/chromedriver'
        # browser = webdriver.Firefox(firefox_binary= firefox_binary, firefox_profile=profile, firefox_options=)
        browser = webdriver.Chrome(chrome_options=options, executable_path=chrome_driver)
    return browser

@csrf_exempt
def url_content_sel(request):
    if request.is_ajax() and request.method == 'POST':
        params = json.loads(request.body.decode())
        url = params['url']
        print('Url request: ' + url)
        uid = str(request.user.id)
        domainURL = '{uri.scheme}://{uri.netloc}/'.format(uri=urlparse(url))
        browser = get_browser('chrome', True)
        browser.get(url)
        soup = B(browser.page_source, 'html.parser')
        browser.quit()
        [s.extract() for s in soup('script')]

        head = soup.find("head")
        a_url = soup.find(href=True)
        # print(a_url)
        if a_url != None:
            if a_url['href'][0:3] == '../':
                base = soup.new_tag('base', href=url)
            else:
                # print("DomainURL: ", domainURL)
                base = soup.new_tag('base', href=domainURL)
            soup.head.insert(0, base)

        # meta = soup.new_tag('meta', charset='utf-8')
        # soup.head.insert(1, meta)
        ajax = soup.new_tag('script', src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js")
        soup.head.append(ajax)
        
        forge = soup.new_tag('script', src="https://cdnjs.cloudflare.com/ajax/libs/forge/0.8.2/forge.min.js")
        soup.head.append(forge)

        inspector = soup.new_tag('script', type="text/javascript")
        with open('static/js/inspector.js', 'r') as myfile:
            inspector.string = myfile.read()
        soup.head.append(inspector)

    return HttpResponse(json.dumps(soup.prettify()), content_type="application/json")

@csrf_exempt
def get_procedure(request):
    if request.is_ajax() and request.method == 'POST':
        params = json.loads(request.body.decode())
        project = Project.from_dict(params['procedure'])
        uid = request.user.id
        user = User.objects.get(id=uid)
        params = {}
        project = as_jsonable(project)
        params['user'] = user
        params['project'] = project
        params['alert_type'] = [False, False, False]
        params['keyword'] = ''
        params['sched_type'] = '0'
        params['sched_val'] = '0'
        kwargs = {'procedure_file': params['project'], 'usr': params['user'], 'alert_type': params['alert_type'],
                  'keyword': params['keyword'], 'sched_type': params['sched_type'], 'sched_val': params['sched_val']}
        saas_save_project(**kwargs)

    return HttpResponseRedirect('/project/list/')


@csrf_exempt
def get_login(request):
    if request.is_ajax() and request.method == 'POST':
        params = json.loads(request.body.decode())
       
        json_var = {
            "product_url": {}, 
            "login_details": [], 
            "project_name": "", 
            "project_type": 1, 
            "start_url": "", 
            "page_list": []
        }
        json_var["start_url"] = params['start_url']
        json_var["login_details"] = params['login_info']
        try:    
            page = {
                "current_page_url": params['start_url'],
                "action_list": params["fill_click_list"],
                "num_pages": 2,
                "pagination": []
            }
            json_var["page_list"].append(page)
        except KeyError:
            pass

        profile = Profile.objects.get(user=request.user.id)

        url = str(params['start_url'])
        domainURL = '{uri.scheme}://{uri.netloc}/'.format(uri=urlparse(params['start_url']))
        html_data = Extraction().html_page_source(Project.from_dict(json_var), profile.private_key, 0, params['flag'])
        soup = B(html_data, 'html.parser')

        [s.extract() for s in soup('script')]

        head = soup.find("head")
        a_url = soup.find(href=True)
        if a_url['href'][0:3] == '../':
            base = soup.new_tag('base', href=url)
        else:
            base = soup.new_tag('base', href=domainURL)
        soup.head.insert(0, base)

        # meta = soup.new_tag('meta', charset='utf-8')
        # soup.head.insert(1, meta)
        ajax = soup.new_tag('script', src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js")
        soup.head.append(ajax)
        
        # forge = soup.new_tag('script', src="https://cdnjs.cloudflare.com/ajax/libs/forge/0.8.2/forge.min.js")
        # soup.head.append(forge)

        inspector = soup.new_tag('script', type="text/javascript")
        with open('static/js/inspector.js', 'r') as myfile:
            inspector.string = myfile.read()
        soup.head.append(inspector)

        return HttpResponse(json.dumps(soup.prettify()), content_type="application/json")
