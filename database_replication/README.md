### Database Replication
- Here, the pigdata database is replicated using the mysql master-slave concept.
- Through this, all the data written onto the master node is replicated onto the slave node.
- This concept can be used to distribute load across databases (like read from slaves and write to master node).
- This [link](https://www.digitalocean.com/community/tutorials/how-to-set-up-master-slave-replication-in-mysql) provides the steps to configure the master and slave nodes.
- **Note:**
	- Incase you have any issue while restarting the `mysql` service, check whether the line `[mysqld]` is written before configuring all the parameters (like `bind-address`, `server-id` etc) in the `mysql.conf` (or `my.conf`) file.