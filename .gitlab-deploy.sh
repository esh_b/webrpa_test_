#!/bin/bash
#Get servers list
set -f
string=$DEPLOY_SERVER
array=(${string//,/ })

#Iterate servers for deploy and pull last commit
for i in "${!array[@]}"
do
  echo "Deploy project on server ${array[i]}"
  ssh -vvv ubuntu@${array[i]} "source venv/bin/activate \
    && cd web-rpa \
    && git fetch origin \
    && git checkout master \
    && git checkout $CI_COMMIT_SHORT_SHA \
   	&& python server/manage.py migrate \
   	&& exit"
   	# && python server/manage.py collectstatic --noinput"
done

