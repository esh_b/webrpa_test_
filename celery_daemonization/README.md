# CELERY DAEMONIZATION

- Though celery daemonization can be configured manually, it is easier to manage daemonized tasks using the `supervisor` module.
- Please go through the *supervisor for django celery - 1* link in the `RELATED LINKS` section to setup supervisor in the server.
- **Note:** 
	- The config files for the `celery worker` and `celery beat` tasks are provided in the directory.
	- First, replace the `command`, `directory`, `user`, `stdout_logfile` and `stderr_logfile` variables in the **celery_webrpa_worker.conf** file accordingly. 
	- Similarly, replace the `command`, `directory`, `user`, `stdout_logfile` and `stderr_logfile` variables in the **celery_webrpa_beat.conf** file accordingly. 
	- Then, copy paste the conf files inside the supervisor's `conf.d` directory (See the *supervisor for django celery - 1* link for more details).
	- Also, add the 2 lines in the `supervisord.conf` file (in the current dir) to the supervisor's `supervisord.conf` file. Through this, we can see the status of all the supervisor daemon tasks and their current status in a web interface (See *supervisor web interface configuration* link).
- After completing the setup, run the supervisor's `reread` and `update` commands to add the worker and beat daemons to the supervisor.
- Finally, start the worker and beat daemons using the commands `sudo supervisorctl start webrpa_workerd` and `sudo supervisorctl start webrpa_beatd`.
> Note that the daemons will start automatically on system startup (due to the configurations in conf files). So, it is not necessary to start the daemons everytime manually.


### RELATED LINKS
- [Supervisor docs](http://supervisord.org/index.html)
- [supervisor for normal python programs](https://medium.com/@jayden.chua/use-supervisor-to-run-your-python-tests-13e91171d6d3x)
- [supervisor for django celery - 1](https://realpython.com/asynchronous-tasks-with-django-and-celery/)
- [supervisor for django celery - 2](http://emadmokhtar.com/yet-another-and-easier-way-to-daemonize-celery.html)
- [supervisor web interface configuration](https://reustle.org/managing-long-running-processes-with-supervisor.html)
