

# Setting up Gitlab's CI/CD

### How does CI help?
 - After setting up the CI for the project (in Gitlab repo and in the code), everytime something is committed to any branch, a build starts (running the unit-tests and linting tests).
 - A build is passed when the unit-tests are successful against the committed code.
 - When a merge request is triggered by a branch against the master branch, the merge-request page will show the status of the build (build status of the branch).
 - Generally, the `MERGE REQUEST` is approved when the build is passed. Once the request is approved, then another build will start checking the status of the updated code of the default branch. If the build fails then, then some unexpected error has occurred and maybe the merge request can be reverted.

### Setup in the Gitlab repository
1. Under `<PROJECT_REPO>/Settings/General` tab, in the `Badges` section, two badges need to be added.
	- **Badge 1 - Build status:**
		- This badge shows the build status when something is committed to the default branch (`master` in general) of the project.
	- **Badge 2 - Coverage:**
		- This badge shows the percent of code covered by the unit-tests.
	-Steps to add the badges:
		- Click on `Add Badge` button.
		- For *build status* badge:
			- Fill the `link` field value as `https://gitlab.com/%{project_path}/commit/%{commit_sha}/pipelines?ref=%{default_branch}`.
			- Fill the `Badge image URL` value as `https://gitlab.com/%{project_path}/badges/%{default_branch}/pipeline.svg`.
		- For *coverage* badge:
			- Fill the `link` field value as `https://gitlab.com/%{project_path}/commit/%{commit_sha}/pipelines?ref=%{default_branch}`.
			- Fill the `Badge image URL` value as `https://gitlab.com/%{project_path}/badges/%{default_branch}/coverage.svg`.
	- The build status and the coverage badges will be updated everytime something is committed in the default branch.
2. Under `<PROJECT_REPO>/Settings/CI/CD` tab, in the `General Pipelines` section, under the `Test coverage parsing` topic, add the following regex: `^TOTAL\s+\d+\s+\d+\s+(\d+\%)$`. This will help in updating the *coverage* badge.

### Configs in the project code
1. The unit-tests use the `MySQL` service container during the CI phase. And that mysql container's hostname is `mysql` by default. So, inorder for the unit-tests to run, the `HOST` field has to be set to `mysql` inside the `DATABASES` setting in `web_rpa/settings.py`.
2. On line 15 in `.gitlab-ci.yml`, change the variable - `MYSQL_DATABASE` value (which is the name of the database) to the name of the db used in `web_rpa/settings.py`.
3. **Note:** Do not change any other variable other than the name of the db in `.gitlab-ci.yml`.
4. Regarding dependencies:
	- The CI uses the `requirements_ci.txt` file in `server/` to install the dependencies before starting the build process.
	- If any other *requirements* file is used, note that celery dependency must not be there in that requirements file. This is because of the issue with the celery schedule code.
	- The build process installs the celery from source instead of using `pip`.
	- The custom *requirement* file path (relative to project dir) must be added on line 33.

### Unit-tests for the modules
1. The unit-tests for the `email_sys` and `schedule` modules are written.
2. As of now, the build will be passed if the unit-tests for the `email_sys` and `schedule` modules are passed. When the unit-tests for other modules are written, then the build results could be much more accurate.

### Gitlab CI shared runners' limitations
- By default, the build runs on `shared-runners` (Gitlab containers). If shared-runners are used for private projects, then Gitlab allows only 2000 CI minutes per month per group.

---

# Register custom runners for the project
- Registering custom runners on our server for the project might help in quicker build time (wait time is comparatively high to get access to shared runner in general) and also avoid the limitation of the shared runners for private projects.
- The custom runners can be setup to run on our servers by:
	- First, installing the `gitlab-runner` by using the Linux repositories ([link](https://docs.gitlab.com/runner/install/linux-repository.html)).
	- Then, registering the runner (as many as required) by following this [link](https://docs.gitlab.com/runner/register/#gnulinux).
		- The token can be found at `https://gitlab.com/<USER_NAME>/<REPO_NAME>/SETTINGS/<CI/CD>`.
		- The tags for a custom runner makes sure that the runner will only run a job if that job contains the tag of the runner. For our purpose, the tags for the runner can be none (Press Enter when you are asked to enter the tags for the runner).
		- Make sure you set the executor as `docker`.
	- **Note**: The above links can be used to setup the runner on a Linux system. For further info, click [here](https://docs.gitlab.com/runner/register/).
- Changes in the config file in the server:
	- By default, the `gitlab-runner` is configured to run only 1 concurrent task. But jobs at the same stage can be executed in parallel. So, in `/etc/gitlab-runner/config.toml` file, set the `concurrent` value to `2` (or more if necessary). And also, for every gitlab-runner defined below (all config under `[[runners]]` belong to a runner, add the following line `limit = 1` under `[runners.docker]`). This will make sure that any runner can only run one task at a time.
	- For example, look at the `config.toml` file in the `CI/` directory of the project.
- Configuration in the Gitlab repository:
	- Under Gitlab repo's SETTINGS -> CI/CD, disable the Shared runners for the project by clicking the `Disable shared runners` button.
- Once the runner is setup for the project, the registered runners can be found in the <PROJECT_REPO>/ SETTINGS/<CI/CD>, under the `Runners activated for this project` section.